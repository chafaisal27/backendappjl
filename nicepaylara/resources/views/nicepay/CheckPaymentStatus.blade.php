@extends('nicepay.nicepay')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Check Payment Status</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('CheckPaymentStatus') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Transaction ID</label>
                            <div class="col-md-6">
                                <input id="tXid" type="text" class="form-control" name="tXid" value="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Reference Number</label>
                            <div class="col-md-6">
                                <input id="referenceNo" type="text" class="form-control" name="referenceNo" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Amount</label>
                            <div class="col-md-6">
                                <input id="amt" type="text" class="form-control" name="amt" value="" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Check
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
