<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Nicepay') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <div class="navbar-brand">
                        <div class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Nicepay Fitur <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu">
                              <li class="dropdown-header">Nicepay Redirect</li>
                              <li><a href="{{ url('/VA-Redirect') }}">Virtual Account Redirect</a></li>
                              <li><a href="{{ url('/CVS-Redirect') }}">Convenience Store Redirect</a></li>
                              <li><a href="{{ url('/CC-Installment-Redirect') }}">Credit Card Installment Redirect</a></li>

                              <li class="divider"></li>

                              <li class="dropdown-header">Nicepay Direct</li>
                              <li><a href="{{ url('/VA-Direct') }}">Virtual Account Direct</a></li>
                              <li><a href="{{ url('/CC-Direct') }}">Credit Card Direct</a></li>
                              <li><a href="{{ url('/CVS-Direct') }}">Convenience Store Direct</a></li>

                              <li class="divider"></li>

                              <li class="dropdown-header">Other Fitur</li>
                              <li><a href="{{ url('/Check-Payment-Status') }}">Check Payment Status</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
