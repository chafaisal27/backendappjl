@extends('nicepay.nicepay')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Credit Card Installment Redirect</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('CheckoutRedirect') }}">
                        {{ csrf_field() }}
                        <input id="payMethod" type="hidden" name="payMethod" value="installment">
                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Amount</label>
                            <div class="col-md-6">
                                <input id="amt" type="text" class="form-control" name="amt" value="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Tenor</label>
                            <div class="col-md-6">
                                <select name="instmntMon" class="form-control">
                                  <option value="1">Full Payment</option>
                                  <option value="3">Tenor 3 Month</option>
                                  <option value="6">Tenor 6 Month</option>
                                  <option value="12">Tenor 12 Month</option>
                              </select>
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Checkout
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
