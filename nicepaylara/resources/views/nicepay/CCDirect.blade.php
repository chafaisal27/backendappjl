@extends('nicepay.nicepay')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Credit Card Redirect</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        <input type="hidden" name="payMethod" value="01">

                        <div class="form-group">
                            <label for="amount" class="col-md-4 control-label">Amount</label>
                            <div class="col-md-6">
                                <input id="amt" type="text" class="form-control" name="amt" value="" placeholder="Amount" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="billingNm" class="col-md-4 control-label">Cardholder's Name</label>
                            <div class="col-md-6">
                                <input id="billingNm" type="text" class="form-control" name="billingNm" value="" placeholder="Cardholder's Name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cardNo" class="col-md-4 control-label">Card Number</label>
                            <div class="col-md-6">
                                <input id="cardNo" type="text" class="form-control" name="cardNo" value="" placeholder="Card Number" maxlength="16" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cardCvv" class="col-md-4 control-label">CVV Number</label>
                            <div class="col-md-6">
                                <input id="cardCvv" type="text" class="form-control" name="cardCvv" value="" placeholder="CVV" maxlength="3" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cardExpYymm" class="col-md-4 control-label">Expiry Date</label>
                            <div class="col-md-6">
                                <input id="cardExpYymm" type="text" class="form-control" name="cardExpYymm" value="" placeholder="YYMM" maxlength="4" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Checkout
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
