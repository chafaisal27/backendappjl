<?php

namespace App\Http\Controllers\NicepayRedirect;

use App\Http\Controllers\Controller;

use App\Lib\NicepayRedirect\NicepayLib;

use Illuminate\Http\Request;

class CallPushController extends Controller
{

    public function CallBackURL(Request $request)
    {

        if ($request->has('authNo'))
        {
        		echo "<pre>";
        		echo "result code       : ".$request->input('resultCd')."\n";
        		echo "result message    : ".$request->input('resultMsg')."\n";
        		echo "auth no           : ".$request->input('authNo')."\n";
        		echo "tXid              : ".$request->input('tXid')." (Save to your database to check status) \n";
        		echo "reference no      : ".$request->input('referenceNo')."\n";
        		echo "payment date      : ".$request->input('transDt')."\n"; // YYMMDD
        		echo "payment time      : ".$request->input('transTm')."\n"; // HH24MISS
        		echo "amount            : ".$request->input('amount')."\n";
        		echo "recurring Token   : ".$request->input('recurringToken')."\n";
            echo "description       : ".$request->input('description')."\n";
        		echo "card no           : ".$request->input('cardNo')."\n";
        		echo "</pre>";
      	}
        else if ($request->has('bankVacctNo'))
        {
        		echo "<pre>";
        		echo "result code       : ".$request->input('resultCd')."\n";
        		echo "result message    : ".$request->input('resultMsg')."\n";
        		echo "vacct no          : ".$request->input('bankVacctNo')."\n";
        		echo "tXid              : ".$request->input('tXid')." (Save to your database to check status) \n";
        		echo "reference no      : ".$request->input('referenceNo')."\n";
        		echo "payment date      : ".$request->input('transDt')."\n"; // YYMMDD
        		echo "payment time      : ".$request->input('transTm')."\n"; // HH24MISS
        		echo "amount            : ".$request->input('amount')."\n";
        		echo "bank code         : ".$request->input('bankCd')."\n";
            echo "description       : ".$request->input('description')."\n";
        		echo "</pre>";
      	}
        else if ($request->has('payNo'))
        {
        		echo "<pre>";
        		echo "result code       : ".$request->input('resultCd')."\n";
        		echo "result message    : ".$request->input('resultMsg')."\n";
        		echo "pay no            : ".$request->input('payNo')."\n";
        		echo "tXid              : ".$request->input('tXid')." (Save to your database to check status) \n";
        		echo "reference no      : ".$request->input('referenceNo')."\n";
        		echo "payment date      : ".$request->input('transDt')."\n"; // YYMMDD
        		echo "payment time      : ".$request->input('transTm')."\n"; // HH24MISS
        		echo "amount            : ".$request->input('amount')."\n";
        		echo "mitra code        : ".$request->input('mitraCd')."\n";
        		echo "</pre>";
      	}
        else
        {
        		echo '<pre>Connection Timeout. Please Try again.</pre>';
      	}
    }

    public function DBProcessURL(Request $request)
    {
        if($request->isMethod('post')){
          $iMid=$request->input('iMid');
          print_r($iMid);exit;
        }
        // $data = $request->getContent();
        $tXid=$request->input('tXid');
        print_r($tXid);exit;

        $nicepay = new NicepayLib();

        //Listen for parameters passed

        $pushParameters = array(
            'tXid',
            'referenceNo',
            'merchantToken',
            'amt',
        );

        $nicepay->extractNotification($pushParameters);

        $iMid         = $nicepay->iMid;
        $tXid         = $nicepay->getNotification('tXid');
        $referenceNo  = $nicepay->getNotification('referenceNo');
        $amt          = $nicepay->getNotification('amt');
        $pushedToken  = $nicepay->getNotification('merchantToken');

        $nicepay->set('tXid',$tXid);
        $nicepay->set('amt',$amt);
        $nicepay->set('iMid',$iMid);

        if(isset($referenceNo))
        {
            $nicepay->set('referenceNo',$referenceNo);
            $transactionToken = $nicepay->transactionToken();
            $nicepay->set('merchantToken',$transactionToken);

            $paymentStatus = $nicepay->checkPaymentStatus($tXid, $referenceNo, $amt);

            //echo $pushedToken.'</br>'.$transactionToken;

            $response = array(
            		'reqTm'			=> $paymentStatus->reqTm,
            		'instmntType'	=> $paymentStatus->instmntType,
            		'resultMsg'		=> $paymentStatus->resultMsg,
            		'reqDt'			=> $paymentStatus->reqDt,
            		'instmntMon'	=> $paymentStatus->instmntMon,
            		'status'		=> $paymentStatus->status,
            		'tXid'			=> $paymentStatus->tXid
            );

            if($pushedToken == $transactionToken)
            {
                if(isset($paymentStatus->status) && $paymentStatus->status == '0')
                {
                    echo "<pre>Paid</pre>";
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '1')
                {
                    echo "<pre>Reversal</pre>";
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '3')
                {
                    echo "<pre>Cancel</pre>";
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '4')
                {
                    echo "<pre>Expired</pre>";
                }
                else
                {
                    echo "<pre>Status Unknown</pre>";
                }
            }
        }
    }
}
