<?php

namespace App\Http\Controllers\NicepayRedirect;

use App\Http\Controllers\Controller;

use App\Lib\NicepayRedirect\NicepayLib;

use Illuminate\Http\Request;

class OtherFiturController extends Controller
{
    public function CheckPaymentStatus(Request $request)
    {
        $nicepay = new NicepayLib();

        $tXid = $request->input('tXid');
        $referenceNo = $request->input('referenceNo');
        $amt = $request->input('amt');

        // Populate Mandatory parameters to send
        $nicepay->set('tXid', $tXid);
        $nicepay->set('referenceNo', $referenceNo);
        $nicepay->set('amt', $amt); // Total gross amount

        $response = $nicepay->checkPaymentStatus($tXid, $referenceNo, $amt);

        // print_r($response);exit;

        // Response from NICEPAY
        if (isset($response->resultCd) && $response->resultCd == "0000")
        {
            if (isset($response->cardNo))
            {
                echo "<pre>";
                echo "payment method    : $response->payMethod \n";
                echo "result code       : $response->resultCd \n";
                echo "result message    : $response->resultMsg \n";
                echo "tXid              : $response->tXid \n";
                echo "reference no      : $response->referenceNo \n";
                echo "description       : $response->goodsNm \n";
                echo "payment date      : $response->reqDt \n";
                echo "payment time      : $response->reqTm \n";
                echo "card no           : $response->cardNo \n";
                echo "billing name      : $response->billingNm \n";
                echo "amount            : $response->amt \n";
                echo "status            : $response->status \n";
                echo "</pre>";
            }
            else if (isset($response->vacctNo))
            {
                echo "<pre>";
                echo "payment method    : $response->payMethod \n";
                echo "result code       : $response->resultCd \n";
                echo "result message    : $response->resultMsg \n";
                echo "tXid              : $response->tXid \n";
                echo "reference no      : $response->referenceNo \n";
                echo "description       : $response->goodsNm \n";
                echo "payment date      : $response->reqDt \n";
                echo "payment time      : $response->reqTm \n";
                echo "bank code         : $response->bankCd \n";
                echo "virtual account   : $response->vacctNo \n";
                echo "billing name      : $response->billingNm \n";
                echo "amount            : $response->amt \n";
                echo "status            : $response->status \n";
                echo "</pre>";
            }
            else
            {
                echo "Transaction Data Not Found";
            }
        }
        elseif(isset($response->resultCd))
        {
            // API data not correct, you can redirect back to checkout page or echo error message.
            // In this sample, we echo error message
            echo "<pre>";
            echo "result code       :".$response->resultCd."\n";
            echo "result message    :".$response->resultMsg."\n";
            echo "</pre>";
        }
        else
        {
            // Timeout, you can redirect back to checkout page or echo error message.
            // In this sample, we echo error message
            echo "<pre>Connection Timeout. Please Try again.</pre>";
        }
    }
}
