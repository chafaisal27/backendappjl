<?php

namespace App\Http\Controllers;

use App\Http\Controllers;

use Illuminate\Http\Request;

class NicepayController extends Controller
{
    // Nicepay Redirect

    public function VARedirect()
    {
        return view('nicepay/VARedirect');
    }

    public function CVSRedirect()
    {
        return view('nicepay/CVSRedirect');
    }

    public function CCRedirect()
    {
        return view('nicepay/CCRedirect');
    }

    public function CCInstallmentRedirect()
    {
        return view('nicepay/CCInstallmentRedirect');
    }

    // Nicepay Direct

    public function VADirect()
    {
        return view('nicepay/VADirect');
    }

    public function CCDirect()
    {
        return view('nicepay/CCDirect');
    }

    public function CVSDirect()
    {
        return view('nicepay/CVSDirect');
    }

    public function CheckPaymentStatus()
    {
        return view('nicepay/CheckPaymentStatus');
    }

}
