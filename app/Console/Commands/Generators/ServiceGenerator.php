<?php

namespace App\Console\Commands\Generators;

use Illuminate\Console\Command;

class ServiceGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {class_name} {repository_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $basePath = "Services\\";

        if (PHP_OS != "Windows") {
            $basePath = "Services/";
        }

        $className = $this->argument('class_name');
        $repositoryName = $this->argument('repository_name');
        $path = $basePath.$className.".php";
        
        $this->fileValidation($className, $basePath);

        $skeleton = $this->getSkeleton();
        $template = $this->generateTemplate($className,$skeleton,$repositoryName);
        
        $this->fileWriter($path,$template);
    }

    protected function getSkeleton() {
        return file_get_contents(resource_path("stubs/Service.stub"));
    }
    
    protected function generateTemplate($classname,$skeleton,$repositoryname) {
        return str_replace(
            ['{{class_name}}','{{repository_name}}'],
            [$classname,$repositoryname],
            $skeleton
        );        
    }
    
    protected function fileValidation($classname, $basePath) {
        $path = $basePath.$classname.".php";
        if(file_exists(app_path($path))) {
            echo("Service already exists! \n");
            exit();
        }
        
        if(!file_exists(app_path("Services"))) {
            mkdir(app_path("Services"), 0777, true);
        }
    }
    
    protected function fileWriter($path,$template) {
        file_put_contents(app_path($path), $template);
        echo("Service created successfully \n");        
    }



}
