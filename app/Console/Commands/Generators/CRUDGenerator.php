<?php

namespace App\Console\Commands\Generators;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CRUDGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {modal_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CRUD Model,Repository,Service and Controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $basePath = "Http\Controllers\Api\\";
        $appPath = "Http\Controllers\Api";

        if (PHP_OS != "Windows") {
            $basePath = "Http/Controllers/Api/";
            $appPath = "Http/Controllers/Api";
        }

        $modalName = $this->argument('modal_name');
        $serviceName = $this->argument('modal_name') . "Service";
        $repositoryName = $this->argument('modal_name') . "Repository";
        $className = $this->argument('modal_name') . "Controller";

        Artisan::call("make:model /Models/$modalName");
        Artisan::call("make:repository $repositoryName $modalName");
        Artisan::call("make:service $serviceName $repositoryName");

        $path = $basePath . $className . ".php";

        $this->fileValidation($className, $basePath, $appPath);

        $skeleton = $this->getSkeleton();
        $template = $this->generateTemplate($className, $skeleton, $serviceName);

        $this->fileWriter($path, $template);
    }

    protected function getSkeleton()
    {
        return file_get_contents(resource_path("stubs/ControllerCrud.stub"));
    }

    protected function generateTemplate($classname, $skeleton, $servicename)
    {
        return str_replace(
            ['{{class_name}}', '{{service_name}}'],
            [$classname, $servicename],
            $skeleton
        );
    }

    protected function fileValidation($classname, $basePath, $appPath)
    {
        $path = $basePath . $classname . ".php";

        if (file_exists(app_path($path))) {
            echo ("Controller already exists! \n");
            exit();
        }

        if (!file_exists(app_path($appPath))) {
            mkdir(app_path($appPath), 0777, true);
        }
    }

    protected function fileWriter($path, $template)
    {
        file_put_contents(app_path($path), $template);
        echo ("Controller created successfully \n");
    }
}
