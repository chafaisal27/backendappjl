<?php

namespace App\Console\Commands\Generators;

use Illuminate\Console\Command;

class RepositoryGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {class_name} {model_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $basePath = "Repositories\\";
        if (PHP_OS != "Windows") {
            $basePath = "Repositories/";
        }

        $className = $this->argument('class_name');
        $modelName = $this->argument('model_name');
        $path = $basePath . $className . ".php";

        $this->fileValidation($className, $basePath);

        $skeleton = $this->getSkeleton();
        $template = $this->generateTemplate($className, $skeleton, $modelName);

        $this->fileWriter($path, $template);
    }

    protected function getSkeleton()
    {
        return file_get_contents(resource_path("stubs/Repository.stub"));
    }

    protected function generateTemplate($classname, $skeleton, $model)
    {
        return str_replace(
            [
                '{{class_name}}',
                '{{model}}',
            ],
            [
                $classname,
                $model,
            ],
            $skeleton
        );
    }

    protected function fileValidation($classname, $basePath)
    {
        $path = $basePath . $classname . ".php";
        if (file_exists(app_path($path))) {
            echo ("Repository already exists! \n");
            exit();
        }

        if (!file_exists(app_path("Repositories"))) {
            mkdir(app_path("Repositories"), 0777, true);
        }
    }

    protected function fileWriter($path, $template)
    {
        file_put_contents(app_path($path), $template);
        echo ("Repository created successfully \n");
    }

}
