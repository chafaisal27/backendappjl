<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\AuctionService;

class AutoCloseAuction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto-close:auction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto close auction every 5 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $AuctionService;
    public function __construct(AuctionService $AuctionService)
    {
        parent::__construct();
        $this->AuctionService = $AuctionService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->AuctionService->autoClose();
    }
}
