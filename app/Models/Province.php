<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'slug', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    public function regencies()
    {
        return $this->hasMany(Regency::class);
    }
}
