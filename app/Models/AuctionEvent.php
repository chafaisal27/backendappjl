<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuctionEvent extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'start_date', 'end_date', 'extra_point', 'active'
    ];

    protected $casts = [
        'id' => 'integer',
        'extra_point' => 'integer',
        'active' => 'integer',
    ];
}
