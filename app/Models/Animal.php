<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Animal extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'description_animal', 'description_delivery', 'description_warranty', 'description', 'slug', 'date_of_birth', 'regency_id', 'owner_user_id', 'animal_sub_category_id', 'created_at', 'updated_at', 'gender', 'video_path', 'is_sponsored'
    ];
    protected $with = ['auction', 'animal_images', 'owner', 'auction.auction_comments', 'auction.auction_comments.user', 'auction.bids', 'auction.bids.user', 'animal_sub_category', 'auction.winner_bid'];

    protected $casts = [
        'id' => 'integer',
        'regency_id' => 'integer',
        'owner_user_id' => 'integer',
        'animal_sub_category_id' => 'integer',
        'is_sponsored' => 'integer'
    ];

    public function animal_sub_category()
    {
        return $this->belongsTo(AnimalSubCategory::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_user_id');
    }

    public function auction()
    {
        return $this->hasOne(Auction::class);
    }

    public function animal_images()
    {
        return $this->hasMany(AnimalImage::class);
    }

    public function animals_comments()
    {
        return $this->hasManyThrough(AuctionComment::class, Auction::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function getVideoPathAttribute($value)
    {
        if (!empty($value)) {
            if (strpos($value, 'http') !== 0) {
                return env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . $value;
            } else {
                return $value;
            }
        }
    }
}
