<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'information', 'read', 'auction_id', 'created_at', 'updated_at', 'animal_id', 'type'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'read' => 'integer',
        'auction_id' => 'integer',
        'animal_id' => 'integer'
    ];

    protected $with = ['auction', 'animal'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }
}
