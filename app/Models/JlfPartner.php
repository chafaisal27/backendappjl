<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JlfPartner extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $casts = [
        'id' => 'integer',
        'order' => 'integer',
    ];
}
