<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regency extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'slug', 'created_at', 'updated_at'
    ];
    
    protected $casts = [
        'id' => 'integer',
        'province_id' => 'integer'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
