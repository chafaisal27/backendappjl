<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnimalImage extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'animal_id', 'image', 'thumbnail', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_id' => 'integer',
    ];

    public function animal_sub_category()
    {
        return $this->hasOne(AnimalSubCategory::class);
    }

    public function getImageAttribute($value)
    {
        if (strpos($value, 'http') !== 0) {
            return env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'images/animals/originals/' . $value; 
        } else {
            return $value;
        }
    }

    public function getThumbnailAttribute($value)
    {
        if (strpos($value, 'http') !== 0) {
          return env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'images/animals/thumbnails/' . $value;
        } else {
            return $value;
        }
    }
}
