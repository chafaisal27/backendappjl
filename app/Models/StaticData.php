<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaticData extends Model
{
    use SoftDeletes;
    
    protected $table = "statics";

    protected $dates = ['deleted_at'];
}
