<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRating extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'rating', 'comment', 'from_user_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'rating' => 'integer',
        'user_id' => 'integer',
        'from_user_id' => 'integer'
    ];


    public function receiver()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function giver()
    {
        return $this->hasOne(User::class, 'from_user_id');
    }
}
