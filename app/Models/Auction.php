<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Auction extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'animal_id', 'open_bid', 'multiply', 'buy_it_now', 'expiry_date', 'cancellation_image', 'cancellation_reason', 'cancellation_date', 'payment_image', 'owner_confirmation', 'winner_confirmation', 'winner_bid_id', 'winner_accepted_date', 'active', 'slug', 'created_at', 'updated_at', 'owner_user_id', 'inner_island_shipping', 'duration', 'admin_id', 'transaction_id', 'injury_time_counter', 'closing_type'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_id' => 'integer',
        'open_bid' => 'integer',
        'multiply' => 'integer',
        'buy_it_now' => 'integer',
        'owner_user_id' => 'integer',
        'owner_confirmation' => 'integer',
        'winner_confirmation' => 'integer',
        'winner_bid_id' => 'integer',
        'active' => 'integer',
        'inner_island_shipping' => 'integer',
        'duration' => 'integer',
        'admin_id' => 'integer',
        'transaction_id' => 'integer',
        'injury_time_counter' => 'integer'
    ];

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }

    public function winner_bid()
    {
        return $this->hasOne(Bid::class, 'id', 'winner_bid_id');
    }

    public function winner()
    {
        return $this->winner_bid->user();
        // return $this->hasOneThrough(User::class, Bid::class, 'user_id', 'winner_bid_id', 'id', 'id');
    }

    public function auction_comments()
    {
        return $this->hasMany(AuctionComment::class);
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_user_id');
    }

    public function auction_event_participant()
    {
        return $this->belongsTo(AuctionEventParticipant::class, 'id');
    }

    public function chat()
    {
        return $this->hasOne(AuctionChat::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }
}
