<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'username', 'email', 'password', 'description', 'email', 'phone_number', 'address', 'photo', 'role_id', 'created_at', 'updated_at', 'blacklisted', 'gender', 'regency_id', 'firebase_token', 'facebook_user_id', 'verification_status', 'identity_number', 'level', 'point', 'coupon'
    ];

    protected $hidden = [
        'remember_token', 'password'
    ];

    protected $with = ['role', 'regency', 'province'];

    protected $withCount = ['reports', 'bids', 'histories'];

    protected $casts = [
        'id' => 'integer',
        'role_id' => 'integer',
        'regency_id' => 'integer',
        'blacklisted' => 'integer',
        'reports_count' => 'integer',
        'bids_count' => 'integer',
        'histories_count' => 'integer',
        'point' => 'float',
        'coupon' => 'integer'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function animals()
    {
        return $this->hasMany(Animal::class, 'owner_user_id');
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function reports() 
    {
        return $this->hasMany(UserReport::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function province()
    {
        return $this->hasOneThrough(Province::class, Regency::class, 'id', 'id', 'regency_id', 'province_id');
    }

    public function histories()
    {
        return $this->hasMany(History::class)->where('read', '=', 0);
    }

    public function top_sellers()
    {
        return $this->hasMany(TopSeller::class);
    }

    public function getPhotoAttribute($value)
    {
        if (strpos($value, 'http') !== 0) {
            return env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'images/profile_pictures/' . $value;
        } else {
            return $value;
        }
    }
}
