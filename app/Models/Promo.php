<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    // protected $fillable = [
    //     'auction_id', 'ammount', 'user_id', 'created_at', 'updated_at'
    // ];

    protected $casts = [
        'id' => 'integer',
        'order' => 'integer',
        'admin_user_id' => 'integer',
    ];
}
