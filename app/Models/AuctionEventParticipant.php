<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuctionEventParticipant extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'auction_id', 'auction_event_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'auction_id' => 'integer',
        'auction_event_id' => 'integer',
    ];

    public function auction_event()
    {
        return $this->belongsTo(AuctionEvent::class);
    }
}
