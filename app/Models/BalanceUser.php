<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BalanceUser extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id', 'information', 'balance', 'animal_id', 'type', 'tXid', 'callbackUrl', 'virtualacc'
    , 'referenceno', 'paymentdate', 'paymenttime', 'paymentmethod', 'dbProcessUrl', 'merchantToken'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_id' => 'integer',
        'user_id' => 'integer',
        'balance' => 'float',
        'tXid' => 'string',
        'callbackUrl' => 'string',
        'dbProcessUrl' => 'string',
        'paymentdate' => 'integer',
        'paymenttime' => 'integer',
        'virtualacc' => 'string',
        'referenceno' => 'string',
        'paymentmethod' => 'string',
        'merchantToken' => 'string',
    ];
}
