<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnimalSubCategory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'slug', 'animal_category_id', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_category_id' => 'integer',
        'animals_count' => 'integer'
    ];

    
    protected $with = ['animal_category'];

    protected $withCount = ['animals'];

    public function animal_category()
    {
        return $this->belongsTo(AnimalCategory::class);
    }

    public function animals()
    {
        return $this->hasMany(Animal::class);
    }
}
