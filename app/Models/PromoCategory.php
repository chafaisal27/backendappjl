<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCategory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $casts = [
        'id' => 'integer',
        'order' => 'integer',
        'admin_user_id' => 'integer',
        'animal_category_id' => 'integer',
    ];

    public function animal_sub_category()
    {
        return $this->belongsTo(AnimalSubCategory::class);
    }
}
