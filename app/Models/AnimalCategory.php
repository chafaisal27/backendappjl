<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use URL;

class AnimalCategory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'slug', 'created_at', 'updated_at', 'type', 'is_video_allowed'
    ];

    protected $casts = [
        'id' => 'integer',
        'animals_count' => 'integer',
        'is_video_allowed' => 'integer',
    ];

    public function animal_sub_categories()
    {
        return $this->hasMany(AnimalSubCategory::class);
    }

    public function animals()
    {
        return $this->hasManyThrough(Animal::class, AnimalSubCategory::class);
    }

    public function getImageAttribute($value)
    {
        if (strpos($value, 'http') !== 0) {
            return URL::to("/$value");
        } else {
            return $value;
        }
    }
}
