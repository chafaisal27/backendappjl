<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'link', 'file_name', 'type', 'start_date', 'end_date', 'admin_id', 'order', 'created_at', 'updated_at'
    ];
}
