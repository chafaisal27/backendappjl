<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuctionComment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'auction_id', 'comment', 'user_id', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'auction_id' => 'integer',
        'user_id' => 'integer'
    ];


    public function auction()
    {
        return $this->hasOne(Auction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
