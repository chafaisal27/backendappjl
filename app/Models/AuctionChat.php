<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuctionChat extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $hidden = [
        'id',
    ];

    protected $fillable = [
        'auction_id', 'firebase_chat_id', 'seller_unread_count', 'seller_user_id', 'buyer_unread_count', 'buyer_user_id', 'admin_unread_count', 'admin_user_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'auction_id' => 'integer',
        'seller_unread_count' => 'integer',
        'seller_user_id' => 'integer',
        'buyer_unread_count' => 'integer',
        'buyer_user_id' => 'integer',
        'admin_unread_count' => 'integer',
        'admin_user_id' => 'integer',
    ];

    public function auction() {
        return $this->belongsTo(Auction::class);
    }
}
