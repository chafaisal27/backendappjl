<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RollingJobAdmin extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'admin_id', 'auction_id', 'batch_number',
    ];

    protected $casts = [
        'id' => 'integer',
        'admin_id' => 'integer',
        'auction_id' => 'integer',
        'batch_number' => 'integer',
    ];
}
