<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductComment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'product_id', 'comment', 'user_id', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'user_id' => 'integer'
    ];


    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
