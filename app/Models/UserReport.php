<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReport extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'reported_user_id', 'user_id', 'reason', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'reported_user_id' => 'integer',
        'user_id' => 'integer'
    ];


    public function reported()
    {
        return $this->hasOne(User::class, 'reported_user_id');
    }

    public function reporter()
    {
        return $this->hasOne(User::class);
    }
}
