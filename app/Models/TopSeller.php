<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopSeller extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'animal_sub_category_id', 'image', 'thumbnail', 'is_promoted'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_sub_category_id' => 'integer',
        'user_id' => 'integer',
        'is_promoted' => 'integer'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function animal_sub_category() {
        return $this->belongsto(AnimalSubCategory::class);
    }
}
