<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointHistory extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id', 'information', 'point', 'animal_id', 'type'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_id' => 'integer',
        'user_id' => 'integer',
        'point' => 'float',
    ];
}
