<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBlacklist extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'admin_user_id', 'user_id', 'reason', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'admin_user_id' => 'integer',
        'user_id' => 'integer',
        'blacklisted_count' => 'integer'
    ];

    public function admin()
    {
        return $this->hasOne(User::class, 'admin_user_id');
    }

    public function blacklisted()
    {
        return $this->hasOne(User::class);
    }
}
