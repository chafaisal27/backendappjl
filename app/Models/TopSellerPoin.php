<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopSellerPoin extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = "top_sellers_poin";

    protected $casts = [
        'id' => 'integer',
        'point' => 'float',
        'user_id' => 'integer',
        'animal_sub_category_id' => 'integer',
        'animal_category_id' => 'integer',

    ];

    public function user()
    {
        return $this->belongsTo(User::class, "owner_user_id");
    }
}
