<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'animal_id', 'animal_id', 'price', 'status', 'quantity', 'inner_island_shipping', 'created_at', 'updated_at', 'slug', 'owner_user_id', 'type'
    ];

    protected $casts = [
        'id' => 'integer',
        'animal_id' => 'integer',
        'price' => 'integer',
        'quantity' => 'integer',
        'inner_island_shipping' => 'integer',
        'owner_user_id' => 'integer',
    ];

    public function product_comments()
    {
        return $this->hasMany(ProductComment::class);
    }

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_user_id');
    }
}
