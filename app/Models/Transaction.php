<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = [
        'id'
    ];

    protected $with = ['animal', 'seller', 'buyer', 'admin'];

    protected $casts = [
        'id' => 'integer',
        'seller_user_id' => 'integer',
        'buyer_user_id' => 'integer',
        'admin_user_id' => 'integer',
        'animal_id' => 'integer',
        'price' => 'integer',
        'seller_bank_name' => 'string',
        'seller_bank_account_number' => 'string',
         'type' => 'string'
        //'delivery_price' => 'integer',
        //'service_price' => 'integer'
    ];

    public function animal() {
        return $this->belongsTo(Animal::class);
    }

    public function seller() {
        return $this->belongsTo(User::class, 'seller_user_id');
    }
    
    public function buyer() {
        return $this->belongsTo(User::class, 'buyer_user_id');
    }

    public function admin() {
        return $this->belongsTo(User::class, 'admin_user_id');
    }
}
