<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoginLog extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'client_ip', 'login_source'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
    ];
}
