<?php

namespace App\Services;

use App\Repositories\SendWARepository;
use GuzzleHttp\Client;

class SendWAService
{
    private $SendWARepository;
    private $AUTH_TOKEN = 'u9CRJ3ZsFfD6JVa6rSa6QWvuz3IsZfIVs3XFer4ed0vNh7kHy2PtiqlurHYGsTSA';

    public function __construct(SendWARepository $SendWARepository) {
        $this->SendWARepository = $SendWARepository;
    }
    
    public function delete($id) {
        return $this->SendWARepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->SendWARepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->SendWARepository->create($data);
    }
    
    public function get($id) {
        return $this->SendWARepository->get($id);
    }
    
    public function getAll() {
        return $this->SendWARepository->getAll();
    }

    /**
     * Sending Message via wablas
     * Reference: https://kacangan.wablas.com/
     * 
     * @params targetPhoneNumber
     * @params message
     */
    public function sendMessage($targetPhoneNumber, $message)
    {
        /**
         * Note:
         * 
         * URL Check kuota sending message via wablas
         * kacangan.wablas.com/api/device/info?token=u9CRJ3ZsFfD6JVa6rSa6QWvuz3IsZfIVs3XFer4ed0vNh7kHy2PtiqlurHYGsTSA
         */

        $client = new Client();
        $url = 'https://kacangan.wablas.com/api/send-message';

        $headers = [
            'Authorization' => $this->AUTH_TOKEN
        ];
        $params = [
            'phone' => $targetPhoneNumber,
            'message' => $message
        ];

        // Hit Url
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'form_params' => $params
        ]);

        $results = json_decode($response->getBody()->getContents());
        $status = $results->status;
        error_log($status);
        return $status ? true : false;
    }
}

