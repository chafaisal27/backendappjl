<?php

namespace App\Services;

use App\Repositories\TransactionRepository;
use App\Repositories\AuctionRepository;

class TransactionService
{
    private $TransactionRepository, $AuctionRepository;

    public function __construct(TransactionRepository $TransactionRepository, AuctionRepository $AuctionRepository) {
        $this->TransactionRepository = $TransactionRepository;
        $this->AuctionRepository = $AuctionRepository;
    }
    
    public function delete($id) {
        return $this->TransactionRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->TransactionRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->TransactionRepository->create($data);
    }
    
    public function get($id) {
        return $this->TransactionRepository->get($id);
    }
    
    public function getAll() {
        return $this->TransactionRepository->getAll();
    }

    public function getOrGenerateAuctionTransaction($auctionId) {
        $transaction = null;

        $auction = $this->AuctionRepository->get($auctionId);

        if ($auction) {
            if ($auction->winner_bid_id) {
                if (!$auction->transaction_id) {
                    // Generate Transaction
                    $winnerAcceptedDate = \Carbon\Carbon::parse($auction->winner_accepted_date)->format('dmy');
    
                    $formData = array();
                    $formData['seller_user_id'] = $auction->owner_user_id;
                    $formData['buyer_user_id'] = $auction->winner_bid->user_id;
                    $formData['admin_user_id'] = $auction->admin_id ?? 1;
                    $formData['type'] = 'auction';
                    $formData['animal_id'] = $auction->animal_id;
                    $formData['invoice_number'] = 'JLF/' . $winnerAcceptedDate . '/AUC/' . $auctionId . '/' . $auction->verification_code;
                    $formData['price'] = $auction->winner_bid->amount;
                    
                    $createTransaction = $this->TransactionRepository->create($formData);
                    
                    $transaction = $this->TransactionRepository->get($createTransaction->id);

                    $update = $this->AuctionRepository->update($auctionId, [
                        'transaction_id' => $createTransaction->id
                    ]);
                } else {
                    $transaction = $this->TransactionRepository->get($auction->transaction_id);
                }
            }
        } else {
            // $auction = 
        }

        return $transaction;
    }
}

