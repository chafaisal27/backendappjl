<?php

namespace App\Services;

use App\Repositories\BidRepository;
use App\Repositories\AuctionRepository;
use App\Repositories\HistoryRepository;
use App\Http\Controllers\Api\FirebaseController;
use Illuminate\Support\Carbon;

class BidService
{
    private $FirebaseController;
    private $BidRepository;
    private $AuctionRepository;
    private $HistoryRepository;

    public function __construct(BidRepository $BidRepository, AuctionRepository $AuctionRepository, HistoryRepository $HistoryRepository, FirebaseController $FirebaseController)
    {
        $this->BidRepository = $BidRepository;
        $this->AuctionRepository = $AuctionRepository;
        $this->HistoryRepository = $HistoryRepository;
        $this->FirebaseController = $FirebaseController;
    }

    public function delete($id)
    {
        $bid = $this->BidRepository->get($id);

        $response = false;
        if ($bid) {
            $auction = $this->AuctionRepository->get($bid->auction_id);
            if ($auction) {
                if ($auction->buy_it_now <= $bid->amount) {
                    // Can not delete BIN
                } else if ($auction->winner_bid_id != null) {
                    // Someone already win this
                } else {
                    // You can delete
                    $response = $this->BidRepository->delete($id);

                    // History & Notificatio for Bidder
                    $history = array();
                    $history['user_id'] = $bid->user_id;
                    $history['auction_id'] = $bid->auction_id;
                    $history['animal_id'] = $auction->animal_id;
                    $history['type'] = 'auction';
                    $history['information'] = "Penawaran Anda sebesar " . $this->convert_to_rupiah($bid->amount) . " pada lelang hewan '" . $auction->animal->name . "' telah dihapus oleh pemilik lelang";

                    // Firebase Notification - Notify Bidder
                    $fcmTokens = array();
                    $fcmTokens[] = $bid->user->firebase_token;

                    $fcmTitle = "Bid Anda dihapus";
                    $fcmBody = $history['information'];

                    $history['information'] = json_encode($history['information']);
                    $this->HistoryRepository->create($history);

                    $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                    // End of Firebase Notification

                    // History & Notificatio for Owner
                    $history = array();
                    $history['user_id'] = $auction->owner_user_id;
                    $history['auction_id'] = $bid->auction_id;
                    $history['animal_id'] = $auction->animal_id;
                    $history['type'] = 'auction';

                    $information = array();

                    $information[] = "Anda telah menghapus bid sebesar " . $this->convert_to_rupiah($bid->amount) . " pada lelang hewan '" . $auction->animal->name . "' oleh ";

                    $information[] = array(
                        'id' => $bid->user->id,
                        'username' => $bid->user->username
                    );

                    $history['information'] = json_encode($information);

                    // Firebase Notification - Notify Owner
                    $fcmTokens = array();
                    $fcmTokens[] = $bid->user->firebase_token;

                    $fcmTitle = "Berhasil menghapus bid";
                    $fcmBody = "Anda telah menghapus bid sebesar " . $this->convert_to_rupiah($bid->amount) . " pada lelang hewan '" . $auction->animal->name . "' oleh '" . $bid->user->username . "'";

                    $this->HistoryRepository->create($history);

                    $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                    // End of Firebase Notification                    
                }
            }
        }

        return $response;
    }

    public function update($id, $data)
    {
        return $this->BidRepository->update($id, $data);
    }

    public function convert_to_rupiah($angka)
    {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));
    }

    public function create($data)
    {
        $bid = $data["amount"];

        $auction = $this->AuctionRepository->get($data["auction_id"]);
        $bids = $auction->bids;

        $lastBid = 0;
        if (count($bids) > 0) {
            // if has bid, get the last inserted data
            $lastBid = intval($bids[count($bids) - 1]->amount);
        }

        if ($lastBid >= $bid) {
            return false;
        }

        $update = array();
        if ($auction->closing_type == "durasi") {
            // Update duration of bid if there is new bid - according to the duration - to prevent snipers
            $update['expiry_date'] = \Carbon\Carbon::now()->addHours(intval($auction->duration));
        } else {
            $injuryTimeCounter = $auction->injury_time_counter;
            $expiryDate = $auction->expiry_date;
            $expiryDate = \Carbon\Carbon::parse($expiryDate);
            $now = \Carbon\Carbon::now();
            $difference = $now->diffInMinutes($expiryDate, true);
            
            if ($difference <= 10) {
                $newExpiryDate = $expiryDate->addMinutes(10);

                // re calculate duration
                $createdDate = $auction->created_at;
                $createdDate = \Carbon\Carbon::parse($createdDate);
                $newDuration = $createdDate->diffInHours($newExpiryDate, true);
                $update['duration'] = $newDuration;

                // counter untuk menghitung berapa kali injury time bertambah 
                $injuryTimeCounter++;
                $update['expiry_date'] = $newExpiryDate;
                $update['injury_time_counter'] = $injuryTimeCounter;
            }
        }



        $this->AuctionRepository->update($auction->id, $update);

        $bidCreate = $this->BidRepository->create($data);

        $bidObject = $this->BidRepository->get($bidCreate->id);

        // Create history
        $history = array();
        $history['user_id'] = $bidObject->user_id;
        $history['auction_id'] = $auction->id;
        $history['animal_id'] = $auction->animal_id;
        $history['type'] = 'auction';

        $winningAuction = false;

        $randomVerificationCode = null;
        // Check if the value is equal to BIN or more
        if ($bid >= $auction->buy_it_now) {
            $digits = 5;
            $randomVerificationCode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

            $winningAuction = true;
            $update = array();
            $update['winner_bid_id'] = $bidObject->id;
            $update['winner_accepted_date'] = \Carbon\Carbon::now();
            $update['verification_code'] = $randomVerificationCode;
            $update['active'] = 0;

            $this->AuctionRepository->update($auction->id, $update);

            $history['information'] = "Anda memenangkan penawaran lelang dengan tawaran " . $this->convert_to_rupiah($bidObject->amount) . " pada lelang hewan '" . $auction->animal->name . "' [Kode Verifikasi: " . $randomVerificationCode . "]";
        } else {
            $history['information'] = "Anda memasang penawaran lelang sebesar " . $this->convert_to_rupiah($bidObject->amount) . " pada lelang hewan '" . $auction->animal->name . "'";
        }

        $history['information'] = json_encode($history['information']);

        // Firebase Notification - Notify Bidder
        $fcmTokens = array();
        $fcmTokens[] = $bidObject->user->firebase_token;

        $fcmTitle = "Berhasil memasang bid";
        $fcmBody = $history['information'];

        $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        // End of Firebase Notification

        $this->HistoryRepository->create($history);


        // NOTIFY THE OWNER
        // Create history
        $history = array();
        $history['user_id'] = $auction->owner_user_id;
        $history['auction_id'] = $auction->id;
        $history['animal_id'] = $auction->animal_id;
        $history['type'] = 'auction';
        $information = array();

        if ($winningAuction) {
            $information[] = array(
                'id' => $bidObject->user->id,
                'username' => $bidObject->user->username
            );

            $information[] = " memenangkan lelang Anda dengan tawaran " . $this->convert_to_rupiah($bidObject->amount) . " pada lelang hewan '" . $auction->animal->name . "' [Kode Verifikasi: " . $randomVerificationCode . "]";

            $history['information'] = "'" . $bidObject->user->username . "' memenangkan lelang Anda dengan tawaran " . $this->convert_to_rupiah($bidObject->amount) . " pada lelang hewan '" . $auction->animal->name . "' [Kode Verifikasi: " . $randomVerificationCode . "]";
        } else {
            $information[] = array(
                'id' => $bidObject->user->id,
                'username' => $bidObject->user->username
            );

            $information[] = " memasang penawaran pada lelang Anda sebesar " . $this->convert_to_rupiah($bidObject->amount) . " pada hewan '" . $auction->animal->name . "'";

            $history['information'] = "'" . $bidObject->user->username . "' memasang penawaran pada lelang Anda sebesar " . $this->convert_to_rupiah($bidObject->amount) . " pada hewan '" . $auction->animal->name . "'";
        }

        // Firebase Notification - Auction Owner
        $fcmTokens = array();
        $fcmTokens[] = $auction->owner->firebase_token;

        $fcmTitle = "Penawaran Pada Lelang Anda";
        $fcmBody = $history['information'];

        $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        // End of Firebase Notification

        $history['information'] = json_encode($information);

        $this->HistoryRepository->create($history);


        $auction = $this->AuctionRepository->get($data["auction_id"]);

        $history = array();
        $history['auction_id'] = $auction->id;
        $history['animal_id'] = $auction->animal_id;
        $history['type'] = 'auction';

        // Notify all other player that follow auction, if there's a higher bid
        if (count($bids) > 0) { // If there are previous bids
            $notifiedPlayers = array($bidObject->user->id);
            $lastBid = intval($bids[count($bids) - 1]->amount);

            // Firebase Notification - For the other bidder 
            $fcmTokens = array();

            $fcmTitle = $winningAuction ? "Lelang Dimenangkan" : "Penawaran Lelang Lebih Tinggi";

            foreach ($bids as $key => $bid) {
                $information = array();

                // Notify other user, except the current bidder
                if ($bid->user_id != $bidObject->user_id && !in_array($bid->user_id, $notifiedPlayers)) {
                    $history['user_id'] = $bid->user_id;
                    $notifiedPlayers[] = $bid->user_id;
                    if ($winningAuction) {
                        // Winning auction
                        $history['information'] = "Lelang hewan '" . $auction->animal->name . "' telah dimenangkan oleh '" . $bidObject->user->username . "' dengan tawaran " . $this->convert_to_rupiah($bidObject->amount);

                        $information[] = "Lelang hewan '" . $auction->animal->name . "' telah dimenangkan oleh ";

                        $information[] = array(
                            'id' => $bidObject->user->id,
                            'username' => $bidObject->user->username
                        );

                        $information[] = " dengan tawaran " . $this->convert_to_rupiah($bidObject->amount);
                    } else {
                        // Higher bid
                        $history['information'] = "Penawaran lebih tinggi pada lelang hewan '" . $auction->animal->name . "' senilai " . $this->convert_to_rupiah($bidObject->amount) . " oleh '" . $bidObject->user->username . "'";

                        $information[] = "Penawaran lebih tinggi pada lelang hewan '" . $auction->animal->name . "' senilai " . $this->convert_to_rupiah($bidObject->amount) . " oleh ";

                        $information[] = array(
                            'id' => $bidObject->user->id,
                            'username' => $bidObject->user->username
                        );
                    }

                    $fcmBody = $history['information'];

                    $history['information'] = json_encode($information);

                    if ($bid->user->firebase_token != null) $fcmTokens[] = $bid->user->firebase_token;

                    $this->HistoryRepository->create($history);
                }
            }

            if (count($fcmTokens) > 0) $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        }

        return $bidObject;
    }

    public function get($id)
    {
        return $this->BidRepository->get($id);
    }

    public function getAll()
    {
        return $this->BidRepository->getAll();
    }

    public function countBidByUserID($userId)
    {
        return $this->BidRepository->countBidByUserID($userId);
    }
}
