<?php

namespace App\Services;

use App\Repositories\AnimalCategoryRepository;

class AnimalCategoryService
{
    private $AnimalCategoryRepository;

    public function __construct(AnimalCategoryRepository $AnimalCategoryRepository)
    {
        $this->AnimalCategoryRepository = $AnimalCategoryRepository;
    }

    public function delete($id)
    {
        return $this->AnimalCategoryRepository->delete($id);
    }

    public function update($id, $data)
    {
        return $this->AnimalCategoryRepository->update($id, $data);
    }

    public function create($data)
    {
        return $this->AnimalCategoryRepository->create($data);
    }

    public function get($id)
    {
        return $this->AnimalCategoryRepository->get($id);
    }

    public function getAllAnimals()
    {
        return $this->AnimalCategoryRepository->getAllAnimals();
    }

    public function getAllProductAnimals()
    {
        return $this->AnimalCategoryRepository->getAllProductAnimals();
    }

    public function getAllProductAccessoryAnimals()
    {
        return $this->AnimalCategoryRepository->getAllProductAccessoryAnimals();
    }

    public function getAllProductFeedsAnimals()
    {
        return $this->AnimalCategoryRepository->getAllProductFeedsAnimals();
    }

    

    public function getAllAnimalsWithoutCount($type)
    {
        return $this->AnimalCategoryRepository->getAllAnimalsWithoutCount($type);
    }
    


    

}
