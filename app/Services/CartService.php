<?php

namespace App\Services;

use App\Repositories\CartRepository;

class CartService
{
    private $CartRepository;

    public function __construct(CartRepository $CartRepository) {
        $this->CartRepository = $CartRepository;
    }
    
    public function delete($id) {
        return $this->CartRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->CartRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->CartRepository->create($data);
    }
    
    public function get($id) {
        return $this->CartRepository->get($id);
    }
    
    public function getAll() {
        return $this->CartRepository->getAll();
    }

}

