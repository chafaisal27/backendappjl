<?php

namespace App\Services;

use App\Repositories\JlfPartnerRepository;

class JlfPartnerService
{
    private $JlfPartnerRepository;

    public function __construct(JlfPartnerRepository $JlfPartnerRepository) {
        $this->JlfPartnerRepository = $JlfPartnerRepository;
    }
    
    public function delete($id) {
        return $this->JlfPartnerRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->JlfPartnerRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->JlfPartnerRepository->create($data);
    }
    
    public function get($id) {
        return $this->JlfPartnerRepository->get($id);
    }
    
    public function getAll() {
        return $this->JlfPartnerRepository->getAll();
    }

}

