<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    private $RoleRepository;

    public function __construct(RoleRepository $RoleRepository) {
        $this->RoleRepository = $RoleRepository;
    }
    
    public function delete($id) {
        return $this->RoleRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->RoleRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->RoleRepository->create($data);
    }
    
    public function get($id) {
        return $this->RoleRepository->get($id);
    }
    
    public function getAll() {
        return $this->RoleRepository->getAll();
    }

}

