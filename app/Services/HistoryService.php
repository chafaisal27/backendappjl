<?php

namespace App\Services;

use App\Repositories\HistoryRepository;

class HistoryService
{
    private $HistoryRepository;

    public function __construct(HistoryRepository $HistoryRepository) {
        $this->HistoryRepository = $HistoryRepository;
    }
    
    public function delete($id) {
        return $this->HistoryRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->HistoryRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->HistoryRepository->create($data);
    }
    
    public function get($id) {
        return $this->HistoryRepository->get($id);
    }
    
    public function getAll() {
        return $this->HistoryRepository->getAll();
    }

    public function getAllByUserID($userId)
    {
        return $this->HistoryRepository->getAllByUserID($userId);
    }
    
    public function countHistoryByUserID($userId)
    {
        return $this->HistoryRepository->countHistoryByUserID($userId);
    }

    public function markAsRead($historyId)
    {
        $update = array();
        $update['read'] = 1;

        return $this->HistoryRepository->updateWhereIn($historyId, $update);
    }
}

