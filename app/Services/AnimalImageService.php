<?php

namespace App\Services;

use App\Repositories\AnimalImageRepository;
use App\Repositories\AnimalRepository;
use Intervention\Image\ImageManagerStatic as Image;
// use File;

class AnimalImageService
{
    private $AnimalImageRepository;
    private $AnimalRepository;

    public function __construct(AnimalImageRepository $AnimalImageRepository, AnimalRepository $AnimalRepository) {
        $this->AnimalImageRepository = $AnimalImageRepository;
        $this->AnimalRepository = $AnimalRepository;
    }
    
    public function delete($id) {
        $animalImage = $this->AnimalImageRepository->get($id);
        $animal = $this->AnimalRepository->get($animalImage->animal_id);
        
        $response = false;
        $original = $animalImage->image;
        $thumbnail = $animalImage->thumbnail;

        if (file_exists($original)) unlink($original);

        if (file_exists($thumbnail)) unlink($thumbnail);

        $response = $this->AnimalImageRepository->delete($id);
       
        return $response;
    }
    
    public function update($id,$data) {
        return $this->AnimalImageRepository->update($id,$data);
    }
    
    public function create($animalId, $data) {        
        $images = $data['images'];

        $counter = 0;
        for ($i = 1; $i <= count($images); $i++) {
            $digits = 5;
            $randomVerificationCode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            
            $rawImage = Image::make($images[$i - 1]);

            // Save as Original, Resize to 500
            $originalImage = $rawImage;
            $originalImage->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');
            $destinationPath = realpath('images/animals/originals/');

            $originalFileName = "AID-" . $animalId . "-ORI-" . $randomVerificationCode . ".jpg";

            $originalImage->save($destinationPath . "/" . $originalFileName);

            // Save as Thumbnail, Resize to 150
            $thumbnailImage = $rawImage;
            $thumbnailImage->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');
            $destinationPath = realpath('images/animals/thumbnails/');

            $thumbnailFileName = "AID-" . $animalId . "-THM-" . $randomVerificationCode . ".jpg";

            $thumbnailImage->save($destinationPath . "/" . $thumbnailFileName);

            $animalImage = $this->AnimalImageRepository->create([
                'animal_id' => $animalId,
                'image' => $originalFileName,
                'thumbnail' => $thumbnailFileName,
            ]);

            $counter += 1;
        }

        return $counter > 0 ? true : false;
    }
    
    public function get($id) {
        return $this->AnimalImageRepository->get($id);
    }
    
    public function getAll() {
        return $this->AnimalImageRepository->getAll();
    }

}

