<?php

namespace App\Services;

use App\Repositories\UserBlacklistRepository;

class UserBlacklistService
{
    private $UserBlacklistRepository;

    public function __construct(UserBlacklistRepository $UserBlacklistRepository) {
        $this->UserBlacklistRepository = $UserBlacklistRepository;
    }
    
    public function delete($id) {
        return $this->UserBlacklistRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->UserBlacklistRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->UserBlacklistRepository->create($data);
    }
    
    public function get($id) {
        return $this->UserBlacklistRepository->get($id);
    }
    
    public function getAll() {
        return $this->UserBlacklistRepository->getAll();
    }

}

