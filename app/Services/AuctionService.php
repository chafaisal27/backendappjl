<?php

namespace App\Services;

use App\Http\Controllers\Api\FirebaseController;
use App\Models\User;
use App\Models\UserBlacklist;
use App\Repositories\AnimalRepository;
use App\Repositories\AuctionRepository;
use App\Repositories\BidRepository;
use App\Repositories\HistoryRepository;
use App\Repositories\PointHistoryRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserBlacklistRepository;
use Carbon\Carbon;

class AuctionService
{
    private $AuctionRepository, $HistoryRepository, $AnimalRepository, $BidRepository, $FirebaseController, $UserRepository, $PointHistoryRepository;

    public function __construct(
        AuctionRepository $AuctionRepository,
        HistoryRepository $HistoryRepository,
        AnimalRepository $AnimalRepository,
        BidRepository $BidRepository,
        FirebaseController $FirebaseController,
        UserRepository $UserRepository,
        UserBlacklistRepository $UserBlacklistRepository,
        PointHistoryRepository $PointHistoryRepository
    ) {
        $this->AuctionRepository = $AuctionRepository;
        $this->HistoryRepository = $HistoryRepository;
        $this->AnimalRepository = $AnimalRepository;
        $this->BidRepository = $BidRepository;
        $this->FirebaseController = $FirebaseController;
        $this->UserRepository = $UserRepository;
        $this->UserBlacklistRepository = $UserBlacklistRepository;
        $this->PointHistoryRepository = $PointHistoryRepository;
    }
    
    public function getPopularAuctions()
    {
       return $this->AuctionRepository->getPopularAuctions();
    }

    public function delete($id)
    {
        // POINT SYSTEM
        // $auction = $this->AuctionRepository->get($id);
        // $animal = $this->AnimalRepository->get($auction->animal_id);

        // if ($auction && $animal) {
        //     if ($auction->winner_bid_id != null) {
        //         // If the auction has winner
        //         $bid = $this->BidRepository->get($auction->winner_bid_id);

        //         if ($bid) {
        //             // Update Winner Poin
        //             $winner = $this->UserRepository->get($bid->user_id);

        //             if ($winner) {
        //                 $pointToAdd = 1;

        //                 $this->addPoint($winner->id, $pointToAdd, $auction, $animal, 'winner');
        //             }

        //             // Update Owner's Point
        //             $owner = $this->UserRepository->get($auction->owner_user_id);

        //             if ($owner) {
        //                 $pointToAdd = 2;
        //                 $this->addPoint($owner->id, $pointToAdd, $auction, $animal, 'owner');
        //             }

        //             // Check if auction is registered in hot auction
        //             $auction_event = $auction->auction_event_participant()->auction_event;
        //             if ($auction_event) {
        //                 // Get Bonus point
        //                 $extra_point = $auction_event->extra_point;

        //                 if ($extra_point > 0) {
        //                     $this->addPoint($winner->id, $extra_point, $auction, $animal, 'hot-auctions');
        //                     $this->addPoint($owner->id, $extra_point, $auction, $animal, 'hot-auctions');
        //                 }
        //             }
        //         }
        //     }
        // }

        return $this->AuctionRepository->delete($id);
    }

    public function addPoint($id, $pointToAdd, $auction, $animal, $role)
    {
        $user = $this->UserRepository->get($id);

        if ($user) {
            // Update User's point
            $userNewPoint = $user->point + $pointToAdd;

            $formData = array();
            $formData['point'] = $userNewPoint;

            $update = $this->UserRepository->update($user->id, $formData);

            if ($update) {
                // Add to History
                $history = array();
                $history['point'] = $pointToAdd;
                $history['user_id'] = $user->id;
                $history['information'] = "Selamat! Anda mendapatkan '" . $pointToAdd . "' poin setelah menyelesaikan transaksi hewan '" . $animal->name . "' Anda telah dimenangkan. Total poin saat ini '" . $userNewPoint . "' poin";

                if ($role) {
                    switch ($role) {
                        case 'winner':
                            $history['information'] = "Selamat! Anda mendapatkan '" . $pointToAdd . "' poin setelah memenangkan lelang hewan '" . $animal->name . "'. Total poin saat ini '" . $userNewPoint . "' poin";
                            break;
                        case 'owner':
                            $history['information'] = "Selamat! Anda mendapatkan '" . $pointToAdd . "' poin setelah lelang hewan '" . $animal->name . "' Anda telah dimenangkan. Total poin saat ini '" . $userNewPoint . "' poin";
                            break;
                        case 'hot-auctions':
                            $history['information'] = "Selamat! Anda mendapatkan bonus '" . $pointToAdd . "' poin setelah menyelesaikan transaksi lelang panas hewan '" . $animal->name . "'. Total poin saat ini '" . $userNewPoint . "' poin";
                            break;
                    }
                }

                $fcmBody = $history['information'];
                $history['animal_id'] = $auction->animal_id;
                $history['type'] = 'auction';

                $this->PointHistoryRepository->create($history);

                // Firebase Notification - User Notification
                $fcmTokens = array();
                $fcmTokens[] = $user->firebase_token;

                $fcmTitle = "Selamat! Poin JLF untuk Anda!";

                $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                // End of Firebase Notification
            }
        }
    }

    public function update($id, $data)
    {
        return $this->AuctionRepository->update($id, $data);
    }

    public function create($request, $animalId)
    {
        $animal = $this->AnimalRepository->get($animalId);

        if ($animal) {
            if ($request->auction) {
                $newAuction = $request->auction;
                $newAuction['animal_id'] = $animal->id;
                if ($newAuction['closing_type'] == "durasi") {
                    $newAuction['expiry_date'] = Carbon::now()->addHours(intval($newAuction['duration']));
                }

                $this->AuctionRepository->create($newAuction);

                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function get($id)
    {
        return $this->AuctionRepository->get($id);
    }

    public function getAll()
    {
        return $this->AuctionRepository->getAll();
    }

    public function getAllByUserID($userId)
    {
        return $this->AuctionRepository->getAllByUserID($userId);
    }

    public function convert_to_rupiah($angka)
    {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));
    }

    public function closeAuction($auctionObj)
    {
        $auction = $auctionObj;
        $auctionId = $auction->id;

        if (!$auction) {
            return false;
        }

        $updateData = array();

        if ($auction->active) {
            if (count($auction->bids) > 0) {
                $lastBid = $this->BidRepository->get($auction->bids[count($auction->bids) - 1]->id);

                $digits = 5;
                $randomVerificationCode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

                $updateData['verification_code'] = $randomVerificationCode;
                $updateData['winner_bid_id'] = $lastBid->id;
                $updateData['winner_accepted_date'] = \Carbon\Carbon::now();
                $updateData['active'] = 0;

                $auctionUpdated = $this->AuctionRepository->update($auctionId, $updateData);

                $auction = $this->AuctionRepository->get($auctionId);

                $animal = $this->AnimalRepository->get($auction->animal_id);

                // Create history for auction owner
                $history = array();
                $history['user_id'] = $auction->owner_user_id;
                $history['animal_id'] = $auction->animal_id;
                $history['type'] = 'auction';
                $history['information'] = "Anda memilih '" . $lastBid->user->username . "' sebagai pemenang lelang '" . $animal->name . "' dengan tawaran " . $this->convert_to_rupiah($auction->bids[count($auction->bids) - 1]->amount);
                $information = array();
                $information[] = "Anda memilih ";
                $information[] = array(
                    'id' => $lastBid->user->id,
                    'username' => $lastBid->user->username,
                );
                $information[] = " sebagai pemenang lelang '" . $animal->name . "' dengan tawaran " . $this->convert_to_rupiah($auction->bids[count($auction->bids) - 1]->amount);
                $history['auction_id'] = $auction->id;
                $history['information'] = json_encode($information);
                $this->HistoryRepository->create($history);

                // Firebase Notification - Owner Notification
                // $fcmTokens = array();
                // $fcmTokens[] = $auction->owner->firebase_token;

                // $fcmTitle = "Perhatian";
                // $fcmBody = $history['information'];

                // $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                // End of Firebase Notification

                // Create history for auction winner
                $history = array();
                $history['user_id'] = $auction->winner_bid->user_id;
                $history['animal_id'] = $auction->animal_id;
                $history['type'] = 'auction';
                $history['information'] = "Selamat! Anda telah memenangkan lelang hewan '" . $animal->name . "' dengan tawaran " . $this->convert_to_rupiah($auction->bids[count($auction->bids) - 1]->amount) . "! Segera selesaikan transaksi dengan pemilik lelang";

                $fcmTokens = array();
                $fcmBody = $history['information'];

                $history['information'] = json_encode($history['information']);

                $history['auction_id'] = $auction->id;
                $this->HistoryRepository->create($history);

                // Firebase Notification - Winner Notification
                $fcmTokens[] = $auction->winner->firebase_token;

                $fcmTitle = "Perhatian";

                $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                // End of Firebase Notification

                $bids = $auction->bids;
                // Notify other players, except winner

                if (count($bids) > 0) {
                    $notifiedPlayers = array();
                    $lastBid = intval($bids[count($bids) - 1]->amount);
                    $lastBidder = $this->BidRepository->get($bids[count($bids) - 1]->id);

                    // Firebase Notification - Winner Notification
                    $fcmTokens = array();

                    $fcmTitle = "Lelang Telah Usai";

                    foreach ($bids as $key => $bid) {
                        $information = array();
                        $history = array();
                        $fcmTokens[] = $auction->winner->firebase_token;

                        if ($bid->user_id != $auction->winner_bid->user_id && !in_array($bid->user_id, $notifiedPlayers)) {
                            $notifiedPlayers[] = $bid->user_id;
                            $history['user_id'] = $bid->user_id;
                            $history['auction_id'] = $auction->id;
                            $history['animal_id'] = $auction->animal_id;
                            $history['type'] = 'auction';
                            $history['information'] = "Lelang hewan '" . $auction->animal->name . "' telah dimenangkan oleh '" . $lastBidder->user->username . "' dengan tawaran " . $this->convert_to_rupiah($bid->amount);

                            $information[] = "Lelang hewan '" . $auction->animal->name . "' telah dimenangkan oleh ";
                            $information[] = array(
                                'id' => $lastBidder->user->id,
                                'username' => $lastBidder->user->username,
                            );
                            $information[] = " dengan tawaran " . $this->convert_to_rupiah($bid->amount);

                            $fcmTokens[] = $bid->user->firebase_token;
                            $fcmBody = $history['information'];

                            $history['information'] = json_encode($information);

                            $this->HistoryRepository->create($history);
                        }
                    }

                    if (count($fcmTokens) > 0) {
                        $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                    }

                    // End of Firebase Notification
                }

                return $auctionUpdated;
            } else {
                $updateData = array();
                $updateData['active'] = 0;

                $auctionUpdated = $this->AuctionRepository->update($auctionId, $updateData);

                $auction = $this->AuctionRepository->get($auctionId);

                $animal = $this->AnimalRepository->get($auction->animal_id);

                // Create history for auction owner
                $history = array();
                $history['user_id'] = $auction->owner_user_id;
                $history['animal_id'] = $auction->animal_id;
                $history['type'] = 'auction';
                $history['information'] = "Lelang hewan '" . $animal->name . "' berakhir dengan tanpa pemenang (Tidak ada bid)";
                $information = array();
                $information[] = "Lelang hewan '" . $animal->name . "' berakhir dengan tanpa pemenang (Tidak ada bid)";
                $history['auction_id'] = $auction->id;
                $fcmBody = $history['information'];
                $history['information'] = json_encode($information);
                $this->HistoryRepository->create($history);

                // Firebase Notification - Owner Notification
                $fcmTokens = array();
                $fcmTokens[] = $auction->owner->firebase_token;

                $fcmTitle = "Perhatian";

                $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                // End of Firebase Notification

                return $auctionUpdated;
            }
        }
    }

    public function cancel($auctionId)
    {
        $auction = $this->AuctionRepository->get($auctionId);

        if (!$auction) {
            return false;
        }

        $animal = $this->AnimalRepository->get($auction->animal_id);

        $updateData = array();
        $updateData['cancellation_date'] = \Carbon\Carbon::now();
        $updateData['active'] = 0;

        $auctionUpdated = $this->AuctionRepository->update($auctionId, $updateData);

        // Create history for auction owner
        $history = array();
        $history['user_id'] = $auction->owner_user_id;
        $history['animal_id'] = $auction->animal_id;
        $history['type'] = 'auction';
        $history['information'] = json_encode("Anda membatalkan lelang hewan '" . $animal->name . "'");
        $history['auction_id'] = $auction->id;
        $this->HistoryRepository->create($history);

        // Firebase Notification - Owner Notification
        // $fcmTokens = array();
        // $fcmTokens[] = $auction->owner->firebase_token;

        // $fcmTitle = "Perhatian";
        // $fcmBody = $history['information'];

        // $this->FirebaseControl`le``r->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        // End of Firebase Notification

        $bids = $auction->bids;
        // Notify all players that set bid

        if (count($bids) > 0) {
            $notifiedPlayers = array();
            $lastBid = intval($bids[count($bids) - 1]->amount);
            $lastBidder = $this->BidRepository->get($bids[count($bids) - 1]->id);

            // Firebase Notification - Auction Cancellation Notification
            $fcmTokens = array();

            $fcmTitle = "Lelang Dibatalkan";

            foreach ($bids as $key => $bid) {
                $history = array();
                if (!in_array($bid->user_id, $notifiedPlayers)) {
                    $notifiedPlayers[] = $bid->user_id;
                    $history['user_id'] = $bid->user_id;
                    $history['auction_id'] = $auction->id;
                    $history['animal_id'] = $auction->animal_id;
                    $history['type'] = 'auction';
                    $history['information'] = "Lelang hewan '" . $auction->animal->name . "' dibatalkan oleh pemilik lelang";
                    $fcmBody = $history['information'];

                    $history['information'] = json_encode($history['information']);

                    $this->HistoryRepository->create($history);

                    if ($bid->user->firebase_token != null) {
                        $fcmTokens[] = $bid->user->firebase_token;
                    }
                }
            }

            if (count($fcmTokens) > 0) {
                $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
            }
        }

        return true;
    }

    public function autoClose()
    {
        // Get all auctions where is active (1) and expiry_date is less than now()
        $auctions = $this->AuctionRepository->getExpiredAuctions();

        foreach ($auctions as $key => $value) {
            // if ($value->winner_bid_id == null) {
            $this->closeAuction($value);
            // } else {
            //     $update = array();
            //     $update['active'] = 0;
            //     $this->AuctionRepository->update($value->id, $update);
            // }
        }

        return true;
    }

    // public function start($auctionId)
    // {
    //     $auction = $this->AuctionRepository->get($auctionId);

    //     if (!$auction) return false;

    //     $animal = $this->AnimalRepository->get($auction->animal_id);

    //     $updateData = array();
    //     $updateData['cancellation_date'] = null;
    //     $updateData['cancellation_reason'] = null;
    //     $updateData['cancellation_image'] = null;
    //     $updateData['active'] = 1;

    //     $auctionUpdated = $this->AuctionRepository->update($auctionId, $updateData);

    //     // Create history for auction winner
    //     $history = array();
    //     $history['user_id'] = $auction->owner_user_id;
    //     $history['information'] = "Berhasil memulai lelang pada hewan '" . $animal->name . "'.";
    //     $history['auction_id'] = $auction->id;
    //     $history = $this->HistoryRepository->create($history);

    //     return true;
    // }

    public function getAllWithActiveChat($userId, $request)
    {
        return $this->AuctionRepository->getAllWithActiveChat($userId, $request);
    }

    public function getAllWithActiveChatAdmin($request)
    {
        return $this->AuctionRepository->getAllWithActiveChatAdmin($request);
    }

    public function getAuctionWinnerNotConfirmed()
    {
        $arr = [];
        $datas = $this->AuctionRepository->getAuctionWinnerNotConfirmed();

        // $tamp = '2019-09-02 00:37:57';
        // $carbon = Carbon::parse($tamp)->addHours(1);
        // dd($carbon);
        $now = Carbon::now();
        foreach ($datas as $data) {
            $winTimeStamp = $data->winner_accepted_date;
            $winTimeStampParse = Carbon::parse($winTimeStamp);

            $limitAfterOneHour = $winTimeStampParse->addHours(1);
            $limitAfterThreeHour = $winTimeStampParse->addHours(3);
            $limitAfterSixHour = $winTimeStampParse->addHours(6);
            $limit = $winTimeStampParse->addHours(24);
            // $limit = Carbon::createFromFormat('Y-m-d H:i:s', $limit)->setTimezone('Asia/Jakarta');

            if ($now->greaterThan($limitAfterOneHour)) {
                //create notif
            }
            if ($now->greaterThan($limitAfterThreeHour)) {
                //create notif
            }
            if ($now->greaterThan($limitAfterSixHour)) {
                //create notif
            }

            // if now => winTimeStamp+24 hours
            // change status blacklisted on table user to 1 and fill table blacklist_user
            if ($now->greaterThanOrEqualTo($limit)) {

                $bid = $this->BidRepository->get($data->winner_bid_id);
                $user = $this->UserRepository->get($bid->user_id);
                User::find($user->id)->update(['blacklisted' => 1]);

                $userBlacklist = [];
                $userBlacklist['user_id'] = $user->id;
                $userBlacklist['reason'] = 'Hit and Run on Auction' + $data->id;
                $userBlacklist['admin_user_id'] = 1;

                UserBlacklist::create($userBlacklist);
            }
        }
        // return $arr;
    }

    public function getAllWithActiveChatNotPaginate($userId)
    {
        return $this->AuctionRepository->getAllWithActiveChatNotPaginate($userId);
    }

    public function getAuctionEventParticipants()
    {
        return $this->AuctionRepository->getAuctionEventParticipants();
    }
}
