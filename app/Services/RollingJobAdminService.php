<?php

namespace App\Services;

use App\Repositories\RollingJobAdminRepository;
use App\Repositories\UserRepository;

class RollingJobAdminService
{
    private $RollingJobAdminRepository;
    private $UserRepository;

    public function __construct(RollingJobAdminRepository $RollingJobAdminRepository, UserRepository $UserRepository)
    {
        $this->RollingJobAdminRepository = $RollingJobAdminRepository;
        $this->UserRepository = $UserRepository;
    }

    public function delete($id)
    {
        return $this->RollingJobAdminRepository->delete($id);
    }

    public function update($id, $data)
    {
        return $this->RollingJobAdminRepository->update($id, $data);
    }

    public function create($data)
    {
        return $this->RollingJobAdminRepository->create($data);
    }

    public function get($id)
    {
        return $this->RollingJobAdminRepository->get($id);
    }

    public function getAll()
    {
        return $this->RollingJobAdminRepository->getAll();
    }

    public function getNextAdminId($auction_id)
    {
        $lastBatch = $this->RollingJobAdminRepository->getLastBatch();

        if ($lastBatch == null) {
            $adminId = $this->UserRepository->getFirstByRoleAdmin()->id;
            $this->RollingJobAdminRepository->create(["admin_id" => $adminId, "auction_id" => $auction_id, "batch_number" => 1]);

            return $adminId;
        } else {
            $allAddedAdmin = $this->RollingJobAdminRepository->getByBatch($lastBatch->batch_number);
            $allAdmin = $this->UserRepository->getAllByRoleAdmin();

            $arrRollingAdminId = $this->extractIdRollingJob($allAddedAdmin);
            $arrUserAdminId = $this->extractIdAdmin($allAdmin);

            $result = $this->findDiff($arrUserAdminId, $arrRollingAdminId);

            if (count($result) == 0) {
                $adminId = $this->UserRepository->getFirstByRoleAdmin()->id;
                $nextNumberBatch = $lastBatch->batch_number + 1;
                $this->RollingJobAdminRepository->create(["admin_id" => $adminId, "auction_id" => $auction_id, "batch_number" => $nextNumberBatch]);

                return $adminId;
            } else {
                $adminId = $result[0];
                $nextNumberBatch = $lastBatch->batch_number;
                $this->RollingJobAdminRepository->create(["admin_id" => $adminId, "auction_id" => $auction_id, "batch_number" => $nextNumberBatch]);

                return $adminId;
            }
        }
    }

    public function extractIdRollingJob($arr)
    {
        $newArr = array();
        foreach ($arr as $item) {
            array_push($newArr, $item->admin_id);
        }
        return $newArr;
    }

    public function extractIdAdmin($arr)
    {
        $newArr = array();
        foreach ($arr as $item) {
            array_push($newArr, $item->id);
        }
        return $newArr;
    }

    public function findDiff($arr1, $arr2)
    {
        $newArr = array();
        foreach ($arr1 as $item1) {
            $notfound = true;
            foreach ($arr2 as $item2) {
                if ($item1 == $item2) {
                    $notfound = false;
                    break;
                }
            }
            if ($notfound) {
                array_push($newArr, $item1);
            }
        }
        return $newArr;
    }

}
