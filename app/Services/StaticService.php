<?php

namespace App\Services;

use App\Repositories\StaticRepository;

class StaticService
{
    private $StaticRepository;

    public function __construct(StaticRepository $StaticRepository) {
        $this->StaticRepository = $StaticRepository;
    }
    
    public function delete($id) {
        return $this->StaticRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->StaticRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->StaticRepository->create($data);
    }
    
    public function get($id) {
        return $this->StaticRepository->get($id);
    }
    
    public function getAll() {
        return $this->StaticRepository->getAll();
    }

}

