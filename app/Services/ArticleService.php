<?php

namespace App\Services;

use App\Repositories\ArticleRepository;

class ArticleService
{
    private $ArticleRepository;

    public function __construct(ArticleRepository $ArticleRepository) {
        $this->ArticleRepository = $ArticleRepository;
    }
    
    public function delete($id) {
        return $this->ArticleRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->ArticleRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->ArticleRepository->create($data);
    }
    
    public function get($id) {
        return $this->ArticleRepository->get($id);
    }
    
    public function getAll($type) {
        return $this->ArticleRepository->getAll($type);
    }

}

