<?php

namespace App\Services;

use App\Repositories\PromoRepository;

class PromoService
{
    private $PromoRepository;

    public function __construct(PromoRepository $PromoRepository) {
        $this->PromoRepository = $PromoRepository;
    }
    
    public function delete($id) {
        return $this->PromoRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->PromoRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->PromoRepository->create($data);
    }
    
    public function get($id) {
        return $this->PromoRepository->get($id);
    }
    
    public function getAll($type, $location) {
        return $this->PromoRepository->getAll($type, $location);
    }

    public function countPromos($type, $location) {
        return $this->PromoRepository->countPromos($type, $location);
    }

    

}

