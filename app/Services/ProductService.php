<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Repositories\AnimalRepository;
use Illuminate\Support\Facades\Schema;

class ProductService
{
    private $ProductRepository, $AnimalRepository;

    public function __construct(ProductRepository $ProductRepository, AnimalRepository $AnimalRepository)
    {
        $this->ProductRepository = $ProductRepository;
        $this->AnimalRepository = $AnimalRepository;
    }

    public function delete($id)
    {
        $product = $this->ProductRepository->get($id);

        if ($product) {
            $this->AnimalRepository->delete($product->animal_id);
        }

        return $this->ProductRepository->delete($id);
    }

    public function update($id, $data)
    {

        $productColumns = Schema::getColumnListing('products');
        $productArray = collect($data)->only($productColumns)->all();

        return $this->ProductRepository->update($id, $productArray);
    }

    public function create($request, $animalId)
    {
        $animal = $this->AnimalRepository->get($animalId);

        if ($animal) {
            if ($request->product) {
                $new = $request->product;
                $new['animal_id'] = $animal->id;
                $product = $this->ProductRepository->create($new);

                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function get($id)
    {
        return $this->ProductRepository->get($id);
    }

    public function getAll()
    {
        return $this->ProductRepository->getAll();
    }
}
