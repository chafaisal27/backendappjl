<?php

namespace App\Services;

use App\Repositories\UserReportRepository;

class UserReportService
{
    private $UserReportRepository;

    public function __construct(UserReportRepository $UserReportRepository) {
        $this->UserReportRepository = $UserReportRepository;
    }
    
    public function delete($id) {
        return $this->UserReportRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->UserReportRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->UserReportRepository->create($data);
    }
    
    public function get($id) {
        return $this->UserReportRepository->get($id);
    }
    
    public function getAll() {
        return $this->UserReportRepository->getAll();
    }

}

