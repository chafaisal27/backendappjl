<?php

namespace App\Services;

use App\Repositories\AuctionCommentRepository;
use App\Repositories\AuctionRepository;
use App\Repositories\HistoryRepository;
use App\Http\Controllers\Api\FirebaseController;

class AuctionCommentService
{
    private $AuctionCommentRepository;
    private $FirebaseController;
    private $AuctionRepository;
    private $HistoryRepository;

    public function __construct(AuctionCommentRepository $AuctionCommentRepository, AuctionRepository $AuctionRepository, HistoryRepository $HistoryRepository, FirebaseController $FirebaseController) {
        $this->AuctionCommentRepository = $AuctionCommentRepository;
        $this->AuctionRepository = $AuctionRepository;
        $this->HistoryRepository = $HistoryRepository;
        $this->FirebaseController = $FirebaseController;
    }
    
    public function delete($id) {
        return $this->AuctionCommentRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->AuctionCommentRepository->update($id,$data);
    }
    
    public function create($data) {
        // Get list of auction comments
        $auction = $this->AuctionRepository->get($data["auction_id"]);

        $auction_comments = $auction->auction_comments;

        // If the one's commenting is not the owner himself, notify the auction owner
        if (intval($data['user_id']) != $auction->owner_user_id) {
            // NOTIFY AUCTION OWNER
            // Create history
            $history = array();
            $history['user_id'] = $auction->owner_user_id;
            $history['auction_id'] = $auction->id;
            $history['animal_id'] = $auction->animal_id;
            $history['type'] = 'auction';
            $information = array();
            
            $information[] = "Komentar baru pada lelang hewan '" . $auction->animal->name . "' milik Anda";
    
            $history['information'] = "Komentar baru pada lelang hewan '" . $auction->animal->name . "' milik Anda";

            // Firebase Notification - Auction Owner
            $fcmTokens = array();
            $fcmTokens[] = $auction->owner->firebase_token;
            
            $fcmTitle = "Komentar Baru Pada Lelang Hewan " . $auction->animal->name;
            $fcmBody = $history['information'];

            $history['information'] = json_encode($information);
            $this->HistoryRepository->create($history);
    
            $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
            // End of Firebase Notification
        }
        
        $history = array();
        $history['auction_id'] = $auction->id;
        $history['animal_id'] = $auction->animal_id;
        $history['type'] = 'auction';

        if (count($auction_comments) > 0) {
            $notifiedPlayers = array($data['user_id']);
            // If there are previous comments, add notifications
            
            // Firebase Notification - For the other bidder 
            $fcmTokens = array();
            
            $fcmTitle = "Komentar Baru Pada Lelang Hewan " . $auction->animal->name;

            foreach ($auction_comments as $key => $comment) {
                $information = array();
                
                // Notify all user that involved in the comment, except the auction owner
                if (intval($comment->user_id) != intval($data['user_id']) && !in_array($comment->user_id, $notifiedPlayers) && $auction->owner_user_id != $comment->user_id) {
                    $history['user_id'] = $comment->user_id;
                    $notifiedPlayers[] = $comment->user_id;
                    
                    $history['information'] = "Komentar baru pada lelang hewan '" . $auction->animal->name . "'";

                    $information[] = "Komentar baru pada lelang hewan '" . $auction->animal->name . "'";

                    $fcmBody = $history['information'];
                    
                    $history['information'] = json_encode($information);

                    if ($comment->user->firebase_token != null) $fcmTokens[] = $comment->user->firebase_token;
                    
                    $this->HistoryRepository->create($history);
                }
            }
            
            if (count($fcmTokens) > 0) $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        }

        return $this->AuctionCommentRepository->create($data);
    }
    
    public function get($id) {
        return $this->AuctionCommentRepository->get($id);
    }
    
    public function getAll() {
        return $this->AuctionCommentRepository->getAll();
    }

}

