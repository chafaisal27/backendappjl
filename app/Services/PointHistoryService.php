<?php

namespace App\Services;

use App\Repositories\PointHistoryRepository;

class PointHistoryService
{
    private $PointHistoryRepository;

    public function __construct(PointHistoryRepository $PointHistoryRepository) {
        $this->PointHistoryRepository = $PointHistoryRepository;
    }
    
    public function delete($id) {
        return $this->PointHistoryRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->PointHistoryRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->PointHistoryRepository->create($data);
    }
    
    public function get($id) {
        return $this->PointHistoryRepository->get($id);
    }
    
    public function getAllByUserID($userId) {
        return $this->PointHistoryRepository->getAllByUserID($userId);
    }

}

