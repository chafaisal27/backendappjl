<?php

namespace App\Services;

use App\Repositories\AnimalSubCategoryRepository;

class AnimalSubCategoryService
{
    private $AnimalSubCategoryRepository;

    public function __construct(AnimalSubCategoryRepository $AnimalSubCategoryRepository) {
        $this->AnimalSubCategoryRepository = $AnimalSubCategoryRepository;
    }
    
    public function delete($id) {
        return $this->AnimalSubCategoryRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->AnimalSubCategoryRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->AnimalSubCategoryRepository->create($data);
    }
    
    public function get($id) {
        return $this->AnimalSubCategoryRepository->get($id);
    }
    
    public function getAll() {
        return $this->AnimalSubCategoryRepository->getAll();
    }

    public function getByAnimalCategoryId($id) {
        return $this->AnimalSubCategoryRepository->getByAnimalCategoryId($id);
    }
}

