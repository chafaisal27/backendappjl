<?php

namespace App\Services;

use App\Repositories\BalanceUserRepository;

class BalanceUserService
{
    private $PointHistoryRepository;

    public function __construct(BalanceUserRepository $PointHistoryRepository) {
        $this->BalanceUserRepository = $PointHistoryRepository;
    }
    
    
    
    public function deposit($data) {        
        return $this->BalanceUserRepository->deposit($data);
    }
    
    public function get($id) {
        return $this->BalanceUserRepository->get($id);
    }
    
    public function getAllByUserID($userId) {
        return $this->BalanceUserRepository->getAllByUserID($userId);
    }
   

}

