<?php

namespace App\Services;

use App\Repositories\AuctionChatRepository;
use App\Repositories\AuctionRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\Api\FirebaseController;

class AuctionChatService
{
    private $AuctionChatRepository, $AuctionRepository, $FirebaseController, $UserRepository;

    public function __construct(AuctionChatRepository $AuctionChatRepository, AuctionRepository $AuctionRepository, FirebaseController $FirebaseController, UserRepository $UserRepository) {
        $this->AuctionChatRepository = $AuctionChatRepository;
        $this->AuctionRepository = $AuctionRepository;
        $this->FirebaseController = $FirebaseController;
        $this->UserRepository = $UserRepository;
    }
    
    public function delete($id) {
        return $this->AuctionChatRepository->delete($id);
    }
    
    public function update($data) {
        // Lookup if there is firebase_chat_id existing
        $firebase_chats = $this->AuctionChatRepository->getByAuctionChatId($data['firebase_chat_id']);
        $userId = $data['id'];
        
        $tokens = array();

        $array = array();
        if ($firebase_chats) {
            // If exist, then update
            if ($userId == $firebase_chats->seller_user_id) {
                $array['admin_unread_count'] = $firebase_chats->admin_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->admin_user_id)->firebase_token;

                $array['buyer_unread_count'] = $firebase_chats->buyer_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->buyer_user_id)->firebase_token;
            }
            else if ($userId == $firebase_chats->buyer_user_id) {
                $array['admin_unread_count'] = $firebase_chats->admin_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->admin_user_id)->firebase_token;

                $array['seller_unread_count'] = $firebase_chats->seller_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->seller_user_id)->firebase_token;
            }
            else if ($userId == $firebase_chats->admin_user_id) {
                $array['seller_unread_count'] = $firebase_chats->seller_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->seller_user_id)->firebase_token;
                $array['buyer_unread_count'] = $firebase_chats->buyer_unread_count + 1;
                $tokens[] = $this->UserRepository->get($firebase_chats->buyer_user_id)->firebase_token;
            }

            $firebase_chats = $this->AuctionChatRepository->update($firebase_chats->id, $array);
            
            $this->FirebaseController->sendNotifications($tokens, 'Obrolan Baru JLF', $data['message']);
        } else {
            // If not exist, then create
            $auction = $this->AuctionRepository->get($data['auction_id']);

            if ($auction) {
                $array['firebase_chat_id'] = $auction->firebase_chat_id;
                $array['seller_user_id'] = $auction->owner_user_id;
                $array['buyer_user_id'] = $auction->winner_bid->user_id;
                $array['admin_user_id'] = $auction->admin_id;
                $array['auction_id'] = $auction->id;
    
                if ($userId == $auction->owner_user_id) {
                    $array['buyer_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->winner_bid->user_id)->firebase_token;
                    
                    $array['admin_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->admin_id)->firebase_token;
                } 
                else if ($userId == $auction->winner_bid->user_id) {
                    $array['seller_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->owner_user_id)->firebase_token;
                    
                    $array['admin_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->admin_id)->firebase_token;
                } 
                else if ($userId == $auction->admin_id) {
                    $array['seller_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->owner_user_id)->firebase_token;
                    
                    $array['buyer_unread_count'] = 1;
                    $tokens[] = $this->UserRepository->get($auction->winner_bid->user_id)->firebase_token;
                }
    
                $firebase_chats = $this->AuctionChatRepository->create($array);

                $this->FirebaseController->sendNotifications($tokens, 'Obrolan Baru JLF', $data['message']);
            }
        }

        return $firebase_chats;
    }

    public function reset($data) {
        // Lookup if there is firebase_chat_id existing
        $firebase_chats = $this->AuctionChatRepository->getByAuctionChatId($data['firebase_chat_id']);

        $userId = $data['id'];

        $array = array();
        if ($firebase_chats) {
            // If exist, then update
            if ($userId == $firebase_chats->seller_user_id) $array['seller_unread_count'] = 0;
            else if ($userId == $firebase_chats->buyer_user_id) $array['buyer_unread_count'] = 0;
            else if ($userId == $firebase_chats->admin_user_id) $array['admin_unread_count'] = 0;

            $firebase_chats = $this->AuctionChatRepository->update($firebase_chats->id, $array);
        } else {
            // // If not exist, then create
            // $array['firebase_chat_id'] = $data['firebase_chat_id'];
            // $array['seller_user_id'] = $data['seller_user_id'];
            // $array['buyer_user_id'] = $data['buyer_user_id'];
            // $array['admin_user_id'] = $data['admin_user_id'];

            // if ($data['role'] == 'seller') $array['seller_unread_count'] = 0;
            // else if ($data['role'] == 'buyer') $array['buyer_unread_count'] = 0;
            // else if ($data['role'] == 'admin') $array['admin_unread_count'] = 0;

            // $firebase_chats = $this->AuctionChatRepository->create($array);

            $auction = $this->AuctionRepository->get($data['auction_id']);

            if ($auction) {
                // If not exist, then create
                $array['firebase_chat_id'] = $auction->firebase_chat_id;
                $array['seller_user_id'] = $auction->owner_user_id;
                $array['buyer_user_id'] = $auction->winner_bid->user_id;
                $array['admin_user_id'] = $auction->admin_id;
                $array['auction_id'] = $auction->id;
    
                $firebase_chats = $this->AuctionChatRepository->create($array);
            }
        }

        return $firebase_chats;
    }
    
    public function create($data) {        
        return $this->AuctionChatRepository->create($data);
    }
    
    public function get($id) {
        return $this->AuctionChatRepository->get($id);
    }
    
    public function getAll() {
        return $this->AuctionChatRepository->getAll();
    }

    public function count($userId)
    {
        $data = $this->AuctionChatRepository->count($userId);
        return $data;
    }
}

