<?php

namespace App\Services;

use App\Repositories\TopSellerRepository;
use App\Repositories\AnimalSubCategoryRepository;

class TopSellerService
{
    private $TopSellerRepository, $AnimalSubCategoryRepository;

    public function __construct(TopSellerRepository $TopSellerRepository, AnimalSubCategoryRepository $AnimalSubCategoryRepository)
    {
        $this->TopSellerRepository = $TopSellerRepository;
        $this->AnimalSubCategoryRepository = $AnimalSubCategoryRepository;
    }

    public function delete($id)
    {
        return $this->TopSellerRepository->delete($id);
    }

    public function update($id, $data)
    {
        return $this->TopSellerRepository->update($id, $data);
    }

    public function create($data)
    {
        return $this->TopSellerRepository->create($data);
    }

    public function get($animalCategoryId)
    {
        return $this->TopSellerRepository->get($animalCategoryId);
    }

    public function getAll()
    {
        return $this->TopSellerRepository->getAll();
    }

    public function getAllBySubCategory($animalSubCategoryId)
    {
        $array = array($animalSubCategoryId);
        return $this->TopSellerRepository->getAllBySubCategory($array);
    }

    public function getAllByCategory($animalCategoryId)
    {
        $animalSubCategoriesId = $this->AnimalSubCategoryRepository->getAllByCategoryId($animalCategoryId);
        $data = $this->TopSellerRepository->getAllBySubCategory($animalSubCategoriesId);
        $data = $data->unique('image');
        $newData=[];
        
        foreach ($data as $key => $value) {
            array_push($newData, $value);
        }

        return $newData;
    }

    public function getPromotedTopSeller()
    {
        return $this->TopSellerRepository->getPromotedTopSeller();
    }
}
