<?php

namespace App\Services;

use App\Repositories\RegencyRepository;

class RegencyService
{
    private $RegencyRepository;

    public function __construct(RegencyRepository $RegencyRepository) {
        $this->RegencyRepository = $RegencyRepository;
    }
    
    public function delete($id) {
        return $this->RegencyRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->RegencyRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->RegencyRepository->create($data);
    }
    
    public function get($id) {
        return $this->RegencyRepository->get($id);
    }
    
    public function getAll() {
        return $this->RegencyRepository->getAll();
    }

    public function getByProvinceId($id) {
        return $this->RegencyRepository->getByProvinceId($id);
    }
}

