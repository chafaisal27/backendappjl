<?php

namespace App\Services;

use Illuminate\Support\Facades\Schema;
use App\Repositories\AnimalImageRepository;
use App\Repositories\AnimalRepository;
use App\Repositories\AuctionRepository;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use DB;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile as fileLib;
use GuzzleHttp\Client;

class AnimalService
{
    private $status = false;
    private $message = '';
    private $STATIC_TOKEN_JLF_ASSET = 'PUT_YOUR_STATIC_TOKEN_IN_HERE,_SENPAI';

    private $AnimalRepository, $AuctionRepository, $AnimalImageRepository, $ProductRepository;
    public function __construct(AnimalRepository $AnimalRepository, AuctionRepository $AuctionRepository, AnimalImageRepository $AnimalImageRepository, ProductRepository $ProductRepository)
    {
        $this->AnimalRepository = $AnimalRepository;
        $this->AuctionRepository = $AuctionRepository;
        $this->AnimalImageRepository = $AnimalImageRepository;
        $this->ProductRepository = $ProductRepository;
    }

    public function basicUpdate($id, $data)
    {
        return $this->AnimalRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->AnimalRepository->delete($id);
    }

    // update with attach file
    public function update($id, $data, UploadedFile $video = NULL)
    {
        $video = $data->file('video');
        $data = json_decode($data->input('data'), true);

        $images = null;
        if (isset($data['add_images'])) {
            $lastId = $this->AnimalImageRepository->getLastId();
            foreach ($data['add_images'] as $key => $value) {
                $order = intval($lastId) + intval($key) + 1;

                $this->saveImageToExternalAsset($value, $id, $order);
            }
            unset($data['add_images']);
        }

        if (isset($data['delete_images'])) {
            foreach ($data['delete_images'] as $key => $value) {
                $animalImage = $this->AnimalImageRepository->get($value);
                $animalImage->delete();
            }
            unset($data['delete_images']);
        }
        $animalColumns = Schema::getColumnListing('animals');
        $animalArray = collect($data)->only($animalColumns)->all();

        // process getting video from data
        $animal = $this->AnimalRepository->get($id)->only($animalColumns);
        // dd($animal);
        $urlPath = "";
        if (!empty($video)) {
            // if (!is_null($animal['video_path'])) {
            //     // delete old video
            //     File::delete($animal['video_path']);
            //     error_log($animal['video_path'] . " Deleted");
            // }

            $resp = $this->saveVideoToExternalAsset($video, $animal['owner_user_id'], $animal['video_path']);
            if ($resp) {
              $urlPath = 'videos/animals/' . $resp;
            }
            // save new video
            // $directory = public_path("videos/animals/");
            // $extension = $video->getClientOriginalExtension();

            // $ownerUserId = $animal['owner_user_id'];
            // $timestamp = now()->format('Ymd-Hi');
            // $filename = $ownerUserId . "_" . $timestamp . "." . $extension;

            // $video->move($directory, $filename);
            // // $filePath = $directory . $filename;

            // $urlPath .= "videos/animals/" . (string) $filename;
        } else {

            $$urlPath = $animal['video_path'];
        }

        //add attribute and/or set video directory 
        $animalArray['video_path'] = $urlPath;

        $return = $this->AnimalRepository->update($id, $animalArray);
        return $return;
    }

    public function updateDate($id, $data)
    {
        return $this->AnimalRepository->update($id, $data);
    }

    // create with attach file
    public function create($data, UploadedFile $video = NULL)
    {

        // // set video directory before create to database
        $dataInput = json_decode($data->input('data'));

        // process getting video from data
        $video = $data->file('video');
        if (!empty($video)) {
          // $urlPath = "";
          // $extension = $video->getClientOriginalExtension();
          // $directory = public_path("videos/animals/");
          // $ownerUserId = $dataInput->animal->owner_user_id;
          // $timestamp = now()->format('Ymd-Hi');
          // $filename = $ownerUserId . "_" . $timestamp . "." . $extension;
          
          // $video->move($directory, $filename);
          // $filePath = $directory . $filename;
          
          // // error_log($filePath);
          // $urlPath .= "videos/animals/" . (string) $filename;
          
          // //add attribute and set video directory

          $resp = $this->saveVideoToExternalAsset($data->video, $dataInput->animal->owner_user_id);
          if ($resp) {
            $dataInput->animal->video_path = 'videos/animals/' . $resp;
          } 
        }

        $return = [];
        $auction;

        $status = true;

        DB::transaction(function ($auction) use (&$return, $dataInput) {
            if (isset($dataInput->animal)) {
                $animalArr = (array) $dataInput->animal;
                $animal = $this->AnimalRepository->create($animalArr);

                if ($dataInput->images) {
                    for ($i = 1; $i <= count($dataInput->images); $i++) {
                        $image = $dataInput->images[$i - 1];
                        // $this->saveImageToInternalAsset($image, $animal->id, $i);
                        $this->saveImageToExternalAsset($image, $animal->id, $i);
                    }
                }

                if (isset($dataInput->auction)) {
                    $newAuction = $dataInput->auction;
                    $newAuction->animal_id = $animal->id;
                    if ($newAuction->closing_type == "durasi") {
                        $newAuction->duration = $dataInput->auction->duration;
                        $newAuction->expiry_date = Carbon::now()->addHours(intval($dataInput->auction->duration));
                    } else {
                        // manual count duration when closing_type is custom-time
                        $expiryDate = $newAuction->expiry_date;
                        $expiryDate = \Carbon\Carbon::parse($expiryDate);
                        $now = \Carbon\Carbon::now();
                        $difference = $now->diffInHours($expiryDate, true);
                        $newAuction->duration = $difference;
                    }
                    $newAuction = (array) $newAuction;
                    $auction = $this->AuctionRepository->create($newAuction);
                }

                if (isset($dataInput->product)) {
                    $newProduct = $dataInput->product;
                    $newProduct->animal_id = $animal->id;
                    $newProduct = (array) $newProduct;
                    $auction = $this->ProductRepository->create($newProduct);
                }
            } else {
                $status = false;
            }
        }, 5);

        return $status;
    }

    public function saveImageToInternalAsset($image, $animalId, $counter)
    {
        $rawImage = Image::make($image);

        // Save as Original, Resize to 500
        $originalImage = $rawImage;
        $originalImage->resize(1000, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');
        $destinationPath = realpath('images/animals/originals/');

        $originalFileName = "AID-" . $animalId . "-ORI-" . $counter . ".jpg";

        $originalImage->save($destinationPath . "/" . $originalFileName);

        // Save as Thumbnail, Resize to 150
        $thumbnailImage = $rawImage;
        $thumbnailImage->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        $destinationPath = realpath('images/animals/thumbnails/');

        $thumbnailFileName = "AID-" . $animalId . "-THM-" . $counter . ".jpg";

        $thumbnailImage->save($destinationPath . "/" . $thumbnailFileName);

        $animalImage = $this->AnimalImageRepository->create([
            'animal_id' => $animalId,
            'image' => $originalFileName,
            'thumbnail' => $thumbnailFileName,
        ]);
    }

    public function saveImageToExternalAsset($image, $animalId, $counter)
    {
        $rawImage = ($image);

        $client = new Client();
        $url = env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'api/animal/image/upload';
        // $url = '127.0.0.1:8000/api/animal/image/upload';

        $headers = [
            'Authorization' => $this->STATIC_TOKEN_JLF_ASSET
        ];

        $params = [
            'image' => $rawImage,
            'animal_id' => $animalId,
            'counter' => $counter
        ];

        // Hit Url
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'form_params' => $params
        ]);

        $results = json_decode($response->getBody()->getContents());
        $status = $results->status;

        if ($status == 'success') {
            $data = $results->data;
            $animalImage = $this->AnimalImageRepository->create([
                'animal_id' => $animalId,
                'image' => $data->ori,
                'thumbnail' => $data->thumbnail,
            ]);
        } else {
            return false;
        }
    }
    
    public function saveVideoToExternalAsset($video, $animalOwnerUserId, $currentPath = null) 
    {
        $client = new Client();
        $url = env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'api/animal/video/upload';

        $headers = [
          'Authorization' => 'PUT_YOUR_STATIC_TOKEN_IN_HERE,_SENPAI',
        ];


        // // Hit Url
        $response = $client->request('POST', $url, [
          'headers' => $headers,
          'multipart' => [
            [
              'name' => 'owner_user_id',
              'contents' => $animalOwnerUserId,
              'headers' => ['Conten-Type' => 'application/json']
            ],
            [
              'name' => 'current_path',
              'contents' => $currentPath,
              'headers' => ['Conten-Type' => 'application/json']
            ],
            [
              'name' => 'video',
              'contents' => fopen($video->getRealPath(), 'r'),
              'headers'  => ['Content-Type' => 'video/mp4']
            ]
          ]
          ]);
          
          $results = json_decode($response->getBody()->getContents());

          $status = $results->status;
          
          if ($status == 'success') {
            $data = $results->data;
            return $results->data->video;
          } else {
            return false;
          }
    }

    public function getAuction($id)
    {

        $data = $this->AnimalRepository->getAuction($id);
        if ($data->auction) {
            $data->auction["current_bid"] = 0;
            $data->auction["count_comments"] = 0;
            if ($data->auction->auction_comments->count() != 0) {
                $comments = $data->auction->auction_comments;
                $countComments = $comments->count('id');
                $data->auction["count_comments"] = $countComments;
            }
            if ($data->auction->bids->count() != 0) {
                $data->auction["current_bid"] = $data->auction->bids[0]->amount;
            }
        }

        if ($data->product) {
            $data->product["count_comments"] = 0;
            if ($data->product->product_comments->count() != 0) {
                $comments = $data->product->product_comments;
                $countComments = $comments->count('id');
                $data->product["count_comments"] = $countComments;
            }
        }

        return $data;
    }

    public function getProduct($id)
    {

        $data = $this->AnimalRepository->getProduct($id);
        if ($data->product) {
            $data->product["count_comments"] = 0;
            if ($data->product->product_comments->count() != 0) {
                $comments = $data->product->product_comments;
                $countComments = $comments->count('id');
                $data->product["count_comments"] = $countComments;
            }
        }

        return $data;
    }

    public function getAllByCategory($animal_category_id, $filter_animal_name, $sort_by = "Terbaru", $perPage)
    {
        $datas = $this->AnimalRepository->getAllByCategory($animal_category_id, $filter_animal_name, $sort_by, $perPage);
        foreach ($datas as $data) {
            $data->auction["count_comments"] = 0;
            $data->auction["current_bid"] = 0;

            if ($data->auction) {
                if ($data->auction->auction_comments->count() != 0) {
                    $comments = $data->auction->auction_comments;
                    $countComments = $comments->count('id');
                    $data->auction["count_comments"] = $countComments;
                    unset($data->auction->auction_comments);
                }
                if ($data->auction->bids->count() != 0) {
                    $data->auction["current_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->amount;
                    unset($data->auction->bids);
                }
            }
        }

        return $datas;
    }
    public function getAllBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by = "Terbaru", $perPage)
    {
        $datas = $this->AnimalRepository->getAllBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by, $perPage);
        foreach ($datas as $data) {
            $data->auction["count_comments"] = 0;
            $data->auction["current_bid"] = 0;
            if ($data->auction) {
                if ($data->auction->auction_comments->count() != 0) {
                    $comments = $data->auction->auction_comments;
                    $countComments = $comments->count('id');
                    $data->auction["count_comments"] = $countComments;
                    unset($data->auction->auction_comments);
                }
                if ($data->auction->bids->count() != 0) {
                    $data->auction["current_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->amount;
                    unset($data->auction->bids);
                }
            }
        }
        return $datas;
    }

    public function getAllProductByCategory($animal_sub_category_id, $filter_animal_name, $sort_by = "Terbaru", $perPage)
    {
        $datas = $this->AnimalRepository->getAllProductByCategory($animal_sub_category_id, $filter_animal_name, $sort_by, $perPage);

        return $datas;
    }

    public function getAllProductBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by = "Terbaru", $perPage)
    {
        $datas = $this->AnimalRepository->getAllProductBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by, $perPage);

        return $datas;
    }

    public function getAllUnauctionedByUserID($userId)
    {
        return $this->AnimalRepository->getAllUnauctionedByUserID($userId);
    }

    public function getDraftAnimal($userId, $filter_animal_name)
    {
        return $this->AnimalRepository->getDraftAnimal($userId, $filter_animal_name);
    }

    public function getAllByUserID($userId)
    {
        return $this->AnimalRepository->getAllByUserID($userId);
    }

    public function getUserAuctions($userId, $filter_animal_name)
    {
        $datas = $this->AnimalRepository->getUserAuctions($userId, $filter_animal_name);
        foreach ($datas as $data) {
            $data->auction["current_bid"] = 0.0;
            if ($data->auction) {
                if ($data->auction->bids->count() > 0) {
                    $data->auction["current_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->amount;
                    unset($data->auction->bids);
                }
            }
        }
        return $datas;
    }

    public function getUserProduct($userId, $filter_animal_name)
    {
        $datas = $this->AnimalRepository->getUserProduct($userId, $filter_animal_name);
        return $datas;
    }

    public function getUserBids($userId, $sort_by = "Terbaru")
    {
        $datas = $this->AnimalRepository->getUserBids($userId, $sort_by);

        foreach ($datas as $data) {
            $data->auction["current_bid"] = 0;
            $data->auction["last_bid"] = "-";
            $data->auction["winner_user_id"] = 0;
            unset($data->auction->auction_comments);
            if ($data->auction) {
                if ($data->auction->bids->count() > 0) {
                    $data->auction["current_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->amount;
                    $data->auction["last_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->user->username;
                }
            }
        }
        return $datas;
    }

    public function getUserCommentsAuction($userId, $sort_by = "Terbaru")
    {
        $datas = $this->AnimalRepository->getUserCommentsAuction($userId, $sort_by);

        foreach ($datas as $data) {
            $data->auction["current_bid"] = 0;
            $data->auction["last_bid"] = "-";
            $data->auction["winner_user_id"] = 0;
            if ($data->auction) {
                if ($data->auction->bids->count() > 0) {
                    $data->auction["current_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->amount;
                    $data->auction["last_bid"] = $data->auction->bids[$data->auction->bids->count() - 1]->user->username;
                    //unset($data->auction->bids);
                }
            }
        }
        return $datas;
    }

    public function getUserCommentsProduct($userId, $sort_by = "Terbaru")
    {
        $datas = $this->AnimalRepository->getUserCommentsProduct($userId, $sort_by);
        return $datas;
    }

    public function countAll()
    {
        return $this->AnimalRepository->countAll();
    }

    public function getSponsoredAnimals()
    {
        $datas = $this->AnimalRepository->getSponsoredAnimals();
        return $datas;
    }
}
