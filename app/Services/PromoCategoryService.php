<?php

namespace App\Services;

use App\Repositories\PromoCategoryRepository;

class PromoCategoryService
{
    private $PromoCategoryRepository;

    public function __construct(PromoCategoryRepository $PromoCategoryRepository) {
        $this->PromoCategoryRepository = $PromoCategoryRepository;
    }
    
    public function delete($id) {
        return $this->PromoCategoryRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->PromoCategoryRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->PromoCategoryRepository->create($data);
    }
    
    public function get($id) {
        return $this->PromoCategoryRepository->get($id);
    }
    
    public function getAllCategory($animalCategoryId) {
        return $this->PromoCategoryRepository->getAllCategory($animalCategoryId);
    }

    public function getAllSubCategory($animalSubCategoryId) {
        return $this->PromoCategoryRepository->getAllSubCategory($animalSubCategoryId);
    }

}

