<?php

namespace App\Services;

use App\Repositories\LoginLogRepository;

class LoginLogService
{
    private $LoginLogRepository;

    public function __construct(LoginLogRepository $LoginLogRepository) {
        $this->LoginLogRepository = $LoginLogRepository;
    }
    
    public function delete($id) {
        return $this->LoginLogRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->LoginLogRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->LoginLogRepository->create($data);
    }
    
    public function get($id) {
        return $this->LoginLogRepository->get($id);
    }
    
    public function getAll() {
        return $this->LoginLogRepository->getAll();
    }

}

