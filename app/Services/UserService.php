<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\PointHistoryRepository;
use App\Repositories\AnimalCategoryRepository;
use App\Http\Controllers\Api\FirebaseController;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Arr;
use Hash;
use GuzzleHttp\Client;

class UserService
{
    private $UserRepository, $PointHistoryRepository, $FirebaseController, $AnimalCategoryRepository;
    private $status = 'error';
    private $statusCode = 400;
    private $message = '';
    private $data = null;
    private $STATIC_TOKEN_JLF_ASSET = 'PUT_YOUR_STATIC_TOKEN_IN_HERE,_SENPAI';

    public function __construct(UserRepository $UserRepository, PointHistoryRepository $PointHistoryRepository, FirebaseController $FirebaseController, AnimalCategoryRepository $AnimalCategoryRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->PointHistoryRepository = $PointHistoryRepository;
        $this->FirebaseController = $FirebaseController;
        $this->AnimalCategoryRepository = $AnimalCategoryRepository;
    }

    public function register($data)
    {
        // Check if same user with email/username existed
        $user = $this->UserRepository->getAllWhere('email', $data->email);
        if (count($user) == 0) {
            $user1 = $this->UserRepository->getAllWhere('username', $data->username);
            if (count($user1) == 0) {
                $user =  $this->UserRepository->register($data);

                if ($user) {
                    if ($data['photoBase64']) {
                        $rawImage = Image::make($data['photoBase64']);

                        // Save as Original, Resize
                        $originalImage = $rawImage;
                        $originalImage->resize(300, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->encode('jpg');
                        $destinationPath = realpath('images/profile_pictures/');

                        $originalFileName = $user->id . ".jpg";

                        $originalImage->save($destinationPath . "/" . $originalFileName);

                        $update = $this->UserRepository->update($user->id, [
                            'photo' => $user->id . ".jpg"
                        ]);

                        return $user;
                    } else {
                        return $user;
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function login($data)
    {
        try {
            $user = $this->UserRepository->login($data);

            if ($user) {
                if (intval($user->blacklisted) == 1) {
                    // Blacklisted
                    $this->message = "Gagal masuk ke aplikasi JLF, akun anda telah diblacklist";
                } else {
                    $this->message = 'Berhasil login';
                    $this->statusCode = 200;
                    $this->status = 'success';
                    $this->data = $user;
                }
            } else {
                $this->message = 'Username/password tidak ditemukan';
            }
        } catch (\Throwable $th) {
            $this->message = "Throwable: " . $th;
        } catch (\Exception $e) {
            $this->message = "Exception: " . $e;
        }

        $return = [];
        $return['status'] = $this->status;
        $return['statusCode'] = $this->statusCode;
        $return['message'] = $this->message;
        $return['data'] = $this->data;

        return $return;
    }

    public function delete($id)
    {
        return $this->UserRepository->delete($id);
    }

    public function update($id, $data)
    {
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        return $this->UserRepository->update($id, $data);
    }

    public function updateFacebookUserID($id, $data)
    {
        $user = $this->UserRepository->get($id);
        
        $userWithSameFBID = $this->UserRepository->getUserByFacebookUserId($data['facebook_user_id']);

        if (!$user) {
            $this->message = 'Akun tidak ditemukan';
        } else if ($user->facebook_user_id != null) {
            $this->message = 'Akun Anda sudah dikoneksikan dengan akun Facebook';
        } else if ($userWithSameFBID) {
            $this->message = 'Akun FB yang akan disinkronkan sudah sinkron dengan akun lain';
        } else {
            $array = array();
            $array['facebook_user_id'] = $data['facebook_user_id'];
            $update = $this->UserRepository->update($id, $array);
            if ($update) {
                $this->status = 'success';
                $this->message = 'Berhasil mensinkronkan akun JLF Anda dengan Facebook Anda';
            } else {
                $this->message = 'Terjadi kesalahan pada server, silahkan coba kembali';
            }
        }
        
        $this->statusCode = 201;
        
        $return = [];
        $return['status'] = $this->status;
        $return['statusCode'] = $this->statusCode;
        $return['message'] = $this->message;

        return $return;
    }

    public function updateProfilePicture($id, $data)
    {
        // $rawImage = Image::make($data['image_base64']);

        // // Save as Original, Resize
        // $originalImage = $rawImage;
        // $originalImage->resize(300, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // })->encode('jpg');
        // $destinationPath = realpath('images/profile_pictures/');

        // $originalFileName = $data['file_name'] . ".jpg";

        // $originalImage->save($destinationPath . "/" . $originalFileName);

        // return $this->UserRepository->update($id, [
        //     'photo' => $data['file_name'] . ".jpg"
        // ]);

        
         //* For Later Use
        $client = new Client();
        $url = env('ASSET_API', 'http://192.168.1.102/jlf-asset-api/public/') . 'api/user/profile/upload';

        $headers = [
            'Authorization' => $this->STATIC_TOKEN_JLF_ASSET
        ];

        $user = $this->UserRepository->get($id);

        $params = [
            'image' => $data['image_base64'],
            'file_name' => $data['file_name'],
            'user_id' => $id,
            'current_photo' => $user->photo
        ];

        // Hit Url
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'form_params' => $params
        ]);

        $results = json_decode($response->getBody()->getContents());
        $status = $results->status;

        if ($status == 'success') {
            $file_name = $results->data->file_name;
            $url = $results->data->url;

            $update = $this->UserRepository->update($id, [
                'photo' => $file_name
            ]);

            if ($update) {
              return $url;
            } else {
              return false;
            }
        } else {
            return false;
        }
        
    }

    public function verifyByOTP($id, $data)
    {
        return $this->UserRepository->update($id, [
            'verification_status' => 'verified',
            'verification_type' => 'OTP',
            'verified_at' => \Carbon\Carbon::now()
        ]);
    }

    public function updateVerification($id, $data)
    {
        try {
            // KTP
            $rawImage = Image::make($data['ktp_base64']);

            // Save as Original, Resize
            $originalImage = $rawImage;
            $originalImage->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');
            $destinationPath = realpath('images/identities/');

            $originalFileName = "KTP-" . $id . ".jpg";

            $originalImage->save($destinationPath . "/" . $originalFileName);
            unset($data['ktp_base64']);

            // SELFIE
            $rawImage = Image::make($data['selfie_base64']);

            // Save as Original, Resize
            $originalImage = $rawImage;
            $originalImage->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');
            $destinationPath = realpath('images/identities/');

            $originalFileName = "SELFIE-" . $id . ".jpg";

            $originalImage->save($destinationPath . "/" . $originalFileName);
            unset($data['selfie_base64']);

            $data['verified_at'] = \Carbon\Carbon::now();

            $update = $this->UserRepository->update($id, $data);


            $this->message = 'Berhasil menyimpan data verifikasi';
            $this->statusCode = 202;
            $this->status = 'success';
        } catch (\Throwable $th) {
            $this->message = "Throwable: " . $th;
        } catch (\Exception $e) {
            $this->message = "Exception: " . $e;
        }

        $return = [];
        $return['status'] = $this->status;
        $return['statusCode'] = $this->statusCode;
        $return['message'] = $this->message;

        return $return;
    }

    public function create($data)
    {
        return $this->UserRepository->create($data);
    }

    public function get($id)
    {
        return $this->UserRepository->get($id);
    }

    public function getByPhoneNumber($phoneNumber)
    {
        return $this->UserRepository->getByPhoneNumber($phoneNumber);
    }

    public function getAll()
    {
        return $this->UserRepository->getAll();
    }

    public function getAllBlacklistedUser()
    {
        return $this->UserRepository->getAllBlacklistedUser();
    }

    public function getTopSellers($animalSubCategoryId)
    {
        return $this->UserRepository->getTopSellers($animalSubCategoryId);
    }

    public function getByEmail($data)
    {
        return $this->UserRepository->getAllWhere('email', $data);
    }    

    public function facebookLoginSearch($data)
    {
        // Has Email
        if (isset($data->email)) {
            $users = $this->UserRepository->getAllWhere('email', $data->email);

            if ($users && count($users) > 0) {
                // If user's passing the facebook_user_id 
                if (isset($data->facebook_user_id)) {
                    $array = array();
                    $this->UserRepository->update($users[0]->id, [
                        'facebook_user_id' => $data->facebook_user_id
                    ]);
                }
                return $users;
            }
        } else if (isset($data->facebook_user_id)) {
            $users = $this->UserRepository->getAllWhere('facebook_user_id', $data->facebook_user_id);
            if ($users && count($users) > 0) return $users;
        }
        
        return $this->UserRepository->getAllWhere('email', $data->email);
    }

    public function countAll()
    {
        return $this->UserRepository->countAll();
    }


    public function forgotPassword($userId)
    {
        $randomPass = $this->generateRandomPassword();
        $newPassword = Hash::make($randomPass);

        $this->UserRepository->update($userId, ["password" => $newPassword]);
        return $randomPass;
    }

    public function generateRandomPassword()
    {
        $stringTeks = "q w e r t y u i o p a s d f g h j k l z x c v b n m 1 2 3 4 5 6 7 8 9 0";

        $array = explode(" ", strtoupper($stringTeks));
        $random = "";
        for ($i = 0; $i < 8; $i++) {
            $random .= Arr::random($array);
        }

        return (string) $random;
    }

    public function getUserByEmail($email)
    {
        return $this->UserRepository->getUserByEmail($email);
    }

    public function verificationBonusPoint($id)
    {
        $user = $this->UserRepository->getBeforeMassUpdate($id);

        if ($user) {
            if ($user->point == 0 && $user->verification_status == 'verified') {
                $response = $this->addPoint($id, 20, "Selamat Anda telah mendapatkan 20 poin karena Anda telah melakukan verifikasi");

                return $response;
            }
        }

        return false;
    }

    public function addPoint($id, $pointToAdd, $information)
    {
        $user = $this->UserRepository->get($id);

        if ($user) {
            // Update User's point
            $userNewPoint = $user->point + $pointToAdd;

            $formData = array();
            $formData['point'] = $userNewPoint;

            $update = $this->UserRepository->update($user->id, $formData);

            if ($update) {
                // Add to History
                $history = array();
                $history['user_id'] = $user->id;
                $history['information'] = $information;
                $history['point'] = $pointToAdd;

                $fcmBody = $history['information'];

                $this->PointHistoryRepository->create($history);

                // Firebase Notification - User Notification
                $fcmTokens = array();
                $fcmTokens[] = $user->firebase_token;

                $fcmTitle = "Selamat! Poin JLF untuk Anda!";

                $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
                // End of Firebase Notification

                return true;
            }
        }

        return false;
    }

    // public function getUserByCategory($email)
    // {
    //     return $this->UserRepository->getUserByEmail($email);
    // }

    public function getPointCoupon($userId) {
        $user = array();
        $user['coupon'] = $this->UserRepository->getCoupon($userId);
        $user['point'] = $this->UserRepository->getPoint($userId);

        return $user;
    }
}
