<?php

namespace App\Services;

use App\Repositories\AuctionEventRepository;

class AuctionEventService
{
    private $AuctionEventRepository;

    public function __construct(AuctionEventRepository $AuctionEventRepository) {
        $this->AuctionEventRepository = $AuctionEventRepository;
    }
    
    public function delete($id) {
        return $this->AuctionEventRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->AuctionEventRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->AuctionEventRepository->create($data);
    }
    
    public function get($id) {
        return $this->AuctionEventRepository->get($id);
    }
    
    public function getAll() {
        return $this->AuctionEventRepository->getAll();
    }

}

