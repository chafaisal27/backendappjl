<?php

namespace App\Services;

use App\Repositories\SliderRepository;

class SliderService
{
    private $SliderRepository;

    public function __construct(SliderRepository $SliderRepository) {
        $this->SliderRepository = $SliderRepository;
    }
    
    public function delete($id) {
        return $this->SliderRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->SliderRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->SliderRepository->create($data);
    }
    
    public function get($id) {
        return $this->SliderRepository->get($id);
    }
    
    public function getAll() {
        return $this->SliderRepository->getAll();
    }

}

