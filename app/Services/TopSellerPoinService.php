<?php

namespace App\Services;

use App\Repositories\TopSellerPoinRepository;

class TopSellerPoinService
{
    private $TopSellerPoinRepository;

    public function __construct(TopSellerPoinRepository $TopSellerPoinRepository) {
        $this->TopSellerPoinRepository = $TopSellerPoinRepository;
    }
    
    public function delete($id) {
        return $this->TopSellerPoinRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->TopSellerPoinRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->TopSellerPoinRepository->create($data);
    }
    
    public function get($id) {
        return $this->TopSellerPoinRepository->get($id);
    }
    
    public function getAll() {
        return $this->TopSellerPoinRepository->getAll();
    }

    public function getByCategory($categoryId)
    {
        
        return $this->TopSellerPoinRepository->getByCategory($categoryId);
    }

    public function getBySubCategory($subCategoryId)
    {
        return $this->TopSellerPoinRepository->getBySubCategory($subCategoryId);
    }

}

