<?php

namespace App\Services;

use App\Repositories\ProductCommentRepository;
use App\Repositories\ProductRepository;
use App\Repositories\HistoryRepository;
use App\Http\Controllers\Api\FirebaseController;

class ProductCommentService
{
    private $ProductCommentRepository;
    private $FirebaseController;
    private $ProductRepository;
    private $HistoryRepository;

    public function __construct(ProductCommentRepository $ProductCommentRepository, ProductRepository $ProductRepository, HistoryRepository $HistoryRepository, FirebaseController $FirebaseController) {
        $this->ProductCommentRepository = $ProductCommentRepository;
        $this->ProductRepository = $ProductRepository;
        $this->HistoryRepository = $HistoryRepository;
        $this->FirebaseController = $FirebaseController;
    }
    
    public function delete($id) {
        return $this->ProductCommentRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->ProductCommentRepository->update($id,$data);
    }
    
    public function create($data) {
        // Get list of product comments
        $product = $this->ProductRepository->get($data["product_id"]);

        $product_comments = $product->product_comments;

        // If the one's commenting is not the owner himself, notify the product owner
        if (intval($data['user_id']) != $product->owner_user_id) {
            // NOTIFY PRODUCT OWNER
            // Create history
            $history = array();
            $history['user_id'] = $product->owner_user_id;
            $history['product_id'] = $product->id;
            $history['animal_id'] = $product->animal_id;
            $history['type'] = 'product';
            $information = array();
            
            $information[] = "Komentar baru pada produk hewan '" . $product->animal->name . "' milik Anda";
    
            $history['information'] = "Komentar baru pada produk hewan '" . $product->animal->name . "' milik Anda";

            // Firebase Notification - Product Owner
            $fcmTokens = array();
            $fcmTokens[] = $product->owner->firebase_token;
            
            $fcmTitle = "Komentar Baru Pada Produk hewan " . $product->animal->name;
            $fcmBody = $history['information'];

            $history['information'] = json_encode($information);
            $this->HistoryRepository->create($history);
    
            $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
            // End of Firebase Notification
        }
        
        $history = array();
        $history['product_id'] = $product->id;
        $history['animal_id'] = $product->animal_id;
        $history['type'] = 'product';

        if (count($product_comments) > 0) {
            $notifiedPlayers = array($data['user_id']);
            // If there are previous comments, add notifications
            
            // Firebase Notification - For the other bidder 
            $fcmTokens = array();
            
            $fcmTitle = "Komentar Baru Pada Produk hewan " . $product->animal->name;

            foreach ($product_comments as $key => $comment) {
                $information = array();
                
                // Notify all user that involved in the comment, except the product owner
                if (intval($comment->user_id) != intval($data['user_id']) && !in_array($comment->user_id, $notifiedPlayers) && $product->owner_user_id != $comment->user_id) {
                    $history['user_id'] = $comment->user_id;
                    $notifiedPlayers[] = $comment->user_id;
                    
                    $history['information'] = "Komentar baru pada produk hewan '" . $product->animal->name . "'";

                    $information[] = "Komentar baru pada produk hewan '" . $product->animal->name . "'";

                    $fcmBody = $history['information'];
                    
                    $history['information'] = json_encode($information);

                    if ($comment->user->firebase_token != null) $fcmTokens[] = $comment->user->firebase_token;
                    
                    $this->HistoryRepository->create($history);
                }
            }
            
            if (count($fcmTokens) > 0) $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
        }

        return $this->ProductCommentRepository->create($data);
    }
    
    public function get($id) {
        return $this->ProductCommentRepository->get($id);
    }
    
    public function getAll() {
        return $this->ProductCommentRepository->getAll();
    }

}

