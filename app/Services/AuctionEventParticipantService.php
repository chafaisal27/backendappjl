<?php

namespace App\Services;

use App\Repositories\AuctionEventParticipantRepository;

class AuctionEventParticipantService
{
    private $AuctionEventParticipantRepository;

    public function __construct(AuctionEventParticipantRepository $AuctionEventParticipantRepository) {
        $this->AuctionEventParticipantRepository = $AuctionEventParticipantRepository;
    }
    
    public function delete($id) {
        return $this->AuctionEventParticipantRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->AuctionEventParticipantRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->AuctionEventParticipantRepository->create($data);
    }
    
    public function get($id) {
        return $this->AuctionEventParticipantRepository->get($id);
    }
    
    public function getAll() {
        return $this->AuctionEventParticipantRepository->getAll();
    }

}

