<?php

namespace App\Services;

use App\Repositories\ProvinceRepository;

class ProvinceService
{
    private $ProvinceRepository;

    public function __construct(ProvinceRepository $ProvinceRepository) {
        $this->ProvinceRepository = $ProvinceRepository;
    }
    
    public function delete($id) {
        return $this->ProvinceRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->ProvinceRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->ProvinceRepository->create($data);
    }
    
    public function get($id) {
        return $this->ProvinceRepository->get($id);
    }
    
    public function getAll() {
        return $this->ProvinceRepository->getAll();
    }

}

