<?php

namespace App\Services;

use App\Repositories\BlacklistAnimalRepository;

class BlacklistAnimalService
{
    private $BlacklistAnimalRepository;

    public function __construct(BlacklistAnimalRepository $BlacklistAnimalRepository) {
        $this->BlacklistAnimalRepository = $BlacklistAnimalRepository;
    }
    
    public function delete($id) {
        return $this->BlacklistAnimalRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->BlacklistAnimalRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->BlacklistAnimalRepository->create($data);
    }
    
    public function get($id) {
        return $this->BlacklistAnimalRepository->get($id);
    }
    
    public function getAll() {
        return $this->BlacklistAnimalRepository->getAll();
    }

}

