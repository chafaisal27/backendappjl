<?php

namespace App\Services;

use App\Repositories\UserRatingRepository;

class UserRatingService
{
    private $UserRatingRepository;

    public function __construct(UserRatingRepository $UserRatingRepository) {
        $this->UserRatingRepository = $UserRatingRepository;
    }
    
    public function delete($id) {
        return $this->UserRatingRepository->delete($id);
    }
    
    public function update($id,$data) {
        return $this->UserRatingRepository->update($id,$data);
    }
    
    public function create($data) {        
        return $this->UserRatingRepository->create($data);
    }
    
    public function get($id) {
        return $this->UserRatingRepository->get($id);
    }
    
    public function getAll() {
        return $this->UserRatingRepository->getAll();
    }

}

