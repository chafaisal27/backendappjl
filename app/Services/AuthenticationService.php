<?php

namespace App\Services;

use Illuminate\Support\Facades\Redis;

class AuthenticationService
{

    public function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function setTokenData($key, $value)
    {
        Redis::set($key, json_encode($value));
    }

    public function getTokenData($key)
    {
        return (json_decode(Redis::get($key)));
    }

    public function removeToken($key)
    {
        return Redis::del($key);
    }
}
