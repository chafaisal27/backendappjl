<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TopSellerPoinService;
use App\Helpers\ResponseHelper;

class TopSellerPoinController extends Controller
{
    private $TopSellerPoinService;

    public function __construct(TopSellerPoinService $TopSellerPoinService)
    {
        $this->TopSellerPoinService = $TopSellerPoinService;
    }

    public function create(Request $request)
    {
        $request = $request->all();
        $this->TopSellerPoinService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id)
    {
        $this->TopSellerPoinService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->all();
        $this->TopSellerPoinService->update($id, $request);
        return ResponseHelper::put();
    }

    public function get($id)
    {
        $data = $this->TopSellerPoinService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->TopSellerPoinService->getAll();
        return ResponseHelper::get($data);
    }

    public function getByCategory($categoryId)
    {
        
        $data = $this->TopSellerPoinService->getByCategory($categoryId);
        return ResponseHelper::get($data);
    }

    public function getBySubCategory($subCategoryId)
    {
        $data = $this->TopSellerPoinService->getBySubCategory($subCategoryId);
        return ResponseHelper::get($data);
    }
}
