<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\StaticService;
use Illuminate\Http\Request;
use Response;

class StaticController extends Controller
{
    private $StaticService;

    public function __construct(StaticService $StaticService)
    {
        $this->StaticService = $StaticService;
    }

    public function create(Request $request)
    {
        $request = $request->all();
        $this->StaticService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id)
    {
        $this->StaticService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->all();
        $this->StaticService->update($id, $request);
        return ResponseHelper::put();
    }

    public function get($id)
    {
        $data = $this->StaticService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->StaticService->getAll();
        return ResponseHelper::get($data);
    }

    public function checkAvailable($type)
    {

        // if ($type == "LELANG") {
        //     return ResponseHelper::error("Under Maintenance", 401);
        // }

        return ResponseHelper::get([]);
    }

    public function downloadTermsPolicy()
    {
        $file = public_path() . "/files/Kebijakan_Privasi_JLF.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'Kebijakan_Privasi_JLF.pdf', $headers);
    }

    public function downloadHowToBecomeSponsoredSeller()
    {
        $file = public_path() . "/files/tata_cara_sponsored_seller.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'tata_cara_sponsored_seller.pdf', $headers);
    }

    public function downloadHadiahJlf()
    {
        $file = public_path() . "/files/hadiah_jlf.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'hadiah_jlf.pdf', $headers);
    }

}
