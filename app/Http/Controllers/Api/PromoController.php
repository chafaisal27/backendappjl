<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PromoService;
use App\Helpers\ResponseHelper;

class PromoController extends Controller
{
    private $PromoService;

    public function __construct(PromoService $PromoService) {
        $this->PromoService = $PromoService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->PromoService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->PromoService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->PromoService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->PromoService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll($type, $location) {
        $data = $this->PromoService->getAll($type, $location);
        return ResponseHelper::get($data);
    }

    public function countPromos($type, $location) {
        $data = $this->PromoService->countPromos($type, $location);
        return ResponseHelper::get($data);
    }

}
