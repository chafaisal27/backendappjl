<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $ProductService;

    public function __construct(ProductService $ProductService)
    {
        $this->ProductService = $ProductService;
    }

    public function create(Request $request, $animalId)
    {
        $response = $this->ProductService->create($request, $animalId);

        if (!$response) {
            return ResponseHelper::error("Gagal membuat produk jual", 406);
        } else {
            return ResponseHelper::create();
        }
    }

    public function delete($id)
    {
        $this->ProductService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->all(); 
        $this->ProductService->update($id, $request);
        return ResponseHelper::put();
    }

    public function sold($id)
    {
        $response = $this->ProductService->update($id, ["status" => "sold out"]);

        if (!$response) {
            return ResponseHelper::error("Gagal membuat produk jual", 406);
        } else {
            return ResponseHelper::put();
        }
    }

    public function get($id)
    {
        $data = $this->ProductService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->ProductService->getAll();
        return ResponseHelper::get($data);
    }

}
