<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\HistoryService;
use App\Helpers\ResponseHelper;

class HistoryController extends Controller
{
    private $HistoryService;

    public function __construct(HistoryService $HistoryService) {
        $this->HistoryService = $HistoryService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->HistoryService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->HistoryService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->HistoryService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->HistoryService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->HistoryService->getAll();
        return ResponseHelper::get($data);
    }

    public function getAllByUserID($userId)
    {
        $data = $this->HistoryService->getAllByUserID($userId);
        return ResponseHelper::get($data);
    }

    public function countHistoryByUserID($userId)
    {
        $data = $this->HistoryService->countHistoryByUserID($userId);
        return $data;
    }

    public function markAsRead($historyId) 
    {
        $data = $this->HistoryService->markAsRead($historyId);

        if ($data) {
            return ResponseHelper::put();
        } else {
            return ResponseHelper::error("Error", 406);
        }
    }
}
