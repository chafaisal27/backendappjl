<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AuthenticationService;
use App\Services\UserService;
use App\Services\LoginLogService;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;

class UserController extends Controller
{
    private $UserService;
    private $AuthenticationService;
    private $LoginLogService;
    private $status = 'error';
    private $statusCode = 400;
    private $message = '';
    private $data = null;

    public function __construct(UserService $UserService, AuthenticationService $AuthenticationService, LoginLogService $LoginLogService)
    {
        $this->UserService = $UserService;
        $this->AuthenticationService = $AuthenticationService;
        $this->LoginLogService = $LoginLogService;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $this->message = $validator->errors()->first();
        } else {
            // Assign Variable for Logging IP
            $clientIP = $request->server('REMOTE_ADDR');
            $loginSource = 'USERNAME';

            $request = $request->all();

            $return = $this->UserService->login($request);

            if ($return["statusCode"] == 200) {
                $token = $this->AuthenticationService->generateToken();
                $return['data']["token_redis"] = $token;
                $this->AuthenticationService->setTokenData($token, $return['data']);

                // Write Log User's IP
                $dataRequestLogin = [
                    'user_id' => $return['data']['id'],
                    'client_ip' => $clientIP,
                    'login_source' => $loginSource
                ];
                $this->LoginLogService->create($dataRequestLogin);
            }

            $this->status = $return['status'];
            $this->message = $return['message'];
            $this->statusCode = $return['statusCode'];
            $this->data = $return['data'];
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message,
            'data' => $this->data,
        ], $this->statusCode);

        // if (!$data) {
        //     return ResponseHelper::error("Username/Password salah atau tidak ditemukan", 404);
        // } else {
        //     if ($data->blacklisted == 1) {
        //         return ResponseHelper::error("Login gagal, Anda masuk dalam blacklist user", 405);
        //     } else {
        //         return ResponseHelper::get($data);
        //     }
        // }
    }

    public function create(Request $request)
    {
        $request = $request->all();
        $data = $this->UserService->create($request);

        if (!$data) {
            return ResponseHelper::error("Gagal menyimpan data pengguna", 404);
        } else {
            return ResponseHelper::get($data);
        }
    }

    public function register(Request $request)
    {
        $data = $this->UserService->register($request);

        if (!$data) {
            return ResponseHelper::error("Gagal registrasi, username/email sudah digunakan", 404);
        } else {
            $user = $this->UserService->get($data->id);

            $token = $this->AuthenticationService->generateToken();
            $user["token_redis"] = $token;
            $this->AuthenticationService->setTokenData($token, $user);

            return ResponseHelper::get($user);
        }
    }

    public function delete($id)
    {
        $this->UserService->delete($id);
        return ResponseHelper::delete();
    }

    public function verificationBonusPoint($id, Request $request)
    {
        $token = $request->header('Authorization');

        $bool = $this->UserService->verificationBonusPoint($id);

        $this->updateDataRedis($token, $id);

        if ($bool) {
            $this->status = 'succes';
            $this->message = 'Berhasil mengupdate';
            $this->statusCode = 201;
        } else {
            $this->status = 'failed';
            $this->message = 'Gagal mengupdate';
            $this->statusCode = 201;
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message,
        ], $this->statusCode);
    }

    public function update($id, Request $request)
    {
        $token = $request->header('Authorization');

        $request = $request->only(['name', 'description', 'regency_id', 'firebase_token', 'password']);

        $update = $this->UserService->update($id, $request);
        $this->updateDataRedis($token, $id);

        if ($update) return ResponseHelper::put();
        else return ResponseHelper::error("Gagal melakukan update", 400);
    }

    public function updateFacebookUserID($id, Request $request)
    {
        $token = $request->header('Authorization');

        // $request = $request->only(['facebook_user_id']);

        $validator = Validator::make($request->all(), [
            'facebook_user_id' => 'required'
        ]);
        
        if ($validator->fails()) {
            $this->message = $validator->errors()->first();
        } else {
            $return = $this->UserService->updateFacebookUserID($id, $request);
            
            $this->status = $return['status'];
            $this->message = $return['message'];
            $this->statusCode = $return['statusCode'];
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message
        ], $this->statusCode);
    }

    public function updateProfilePicture($id, Request $request)
    {
        $token = $request->header('Authorization');

        $request = $request->all();
        $resp = $this->UserService->updateProfilePicture($id, $request);

        $this->updateDataRedis($token, $id);

        return ResponseHelper::get($resp);
    }

    public function verifyByOTP($id, Request $request)
    {
        $token = $request->header('Authorization');

        $request = $request->all();

        $statusVeification = $this->UserService->get($id)->verification_status;

        if ($statusVeification != 'verified') {
            $this->UserService->verifyByOTP($id, $request);
            $this->UserService->addPoint($id, 5, "Selamat Anda telah mendapatkan 5 poin karena Anda telah melakukan verifikasi via OTP");

            $this->updateDataRedis($token, $id);
        } 

        return ResponseHelper::put();
    }

    public function updateVerification($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ktp_base64' => 'required',
            'selfie_base64' => 'required',
            'verification_status' => 'required',
            'identity_number' => 'required',
        ]);

        if ($validator->fails()) {
            $this->message = $validator->errors()->first();
        } else {
            $request = $request->all();
            $return = $this->UserService->updateVerification($id, $request);

            $this->status = $return['status'];
            $this->message = $return['message'];
            $this->statusCode = $return['statusCode'];
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message,
        ], $this->statusCode);
    }

    public function get($id)
    {
        $data = $this->UserService->get($id);
        return ResponseHelper::get($data);
    }

    public function getUserByEmail($email)
    {
        $data = $this->UserService->getUserByEmail($email);
        return ResponseHelper::get($data);
    }

    public function getByPhoneNumber($phoneNumber)
    {
        $data = $this->UserService->getByPhoneNumber($phoneNumber);
        if ($data) {
            return ResponseHelper::get(true);
        } else {
            return ResponseHelper::get(false);
        }
    }

    public function getAll()
    {
        $data = $this->UserService->getAll();
        return ResponseHelper::get($data);
    }

    public function getAllBlacklistedUser()
    {
        $data = $this->UserService->getAllBlacklistedUser();
        return ResponseHelper::get($data);
    }

    public function getTopSellers($animalSubCategoryId)
    {
        $data = $this->UserService->getTopSellers($animalSubCategoryId);
        return ResponseHelper::get($data);
    }

    public function facebookLoginSearch(Request $request)
    {
        $data = $this->UserService->facebookLoginSearch($request);

        if (count($data)) {
            $token = $this->AuthenticationService->generateToken();
            $data[0]["token_redis"] = $token;
            $this->AuthenticationService->setTokenData($token, $data[0]);

            // Assign Variable for Logging IP
            $clientIP = $request->server('REMOTE_ADDR');
            $userID = $data[0]['id'];
            $loginSource = 'FACEBOOK';

            // Write Log User's IP
            $dataRequestLogin = [
                'user_id' => $userID,
                'client_ip' => $clientIP,
                'login_source' => $loginSource
            ];
            $this->LoginLogService->create($dataRequestLogin);
        }

        return ResponseHelper::get($data);
    }

    public function getByEmail(Request $request)
    {
        $data = $this->UserService->getByEmail($request->email);
        return ResponseHelper::get($data);
    }

    public function countAll()
    {
        $data = $this->UserService->countAll();
        return $data ? $data : 0;
        // return ResponseHelper::get($data);
    }

    public function getTimeNow()
    {
        $data = Carbon::parse(date_format(now(), 'd/m/Y H:i:s'));
        return ResponseHelper::get($data);
    }

    public function getTimeNowCarbon()
    {
        $data = Carbon::parse(date_format(\Carbon\Carbon::now()->addHours(intval(5)), 'd/m/Y H:i:s'));

        return ResponseHelper::get($data);
    }

    public function getAllUserLoginSession()
    {
        $keys = Redis::keys("*");
        $clients = [];
        foreach ($keys as $key) {
            $stored = Redis::get($key);
            $tamp = json_decode($stored);

            $dataTake = [];

            $dataTake["id"] = $tamp->id;
            $dataTake["name"] = $tamp->name;
            $dataTake["email"] = $tamp->email;
            $dataTake["username"] = $tamp->username;
            $dataTake["email"] = $tamp->email;
            $dataTake["token_redis"] = $tamp->token_redis;

            $clients[] = $dataTake;
        }

        return ResponseHelper::get($clients);
    }

    public function getUserLoginSession($key)
    {
        $data = json_decode(Redis::get($key));

        return ResponseHelper::get($data);
    }

    public function deleteUserLoginSession($key)
    {
        $this->AuthenticationService->removeToken($key);

        return ResponseHelper::delete();
    }

    public function verifyToken(Request $request)
    {
        $token = $request->header('Authorization');

        $data = $this->AuthenticationService->getTokenData($token);
        if ($data) {
            // Assign Variable for Logging IP
            $clientIP = $request->server('REMOTE_ADDR');
            $userID = $data->id;
            $loginSource = 'VERIFY-TOKEN';

            // Write Log User's IP
            $dataRequestLogin = [
                'user_id' => $userID,
                'client_ip' => $clientIP,
                'login_source' => $loginSource
            ];
            $this->LoginLogService->create($dataRequestLogin);

            return ResponseHelper::get($data);
        } else {
            return ResponseHelper::error("", 400);
        }
    }

    public function forgotPassword(Request $request)
    {
        $userData = $this->UserService->getByEmail($request->email);
        if (count($userData) > 0) {

            $newPassword = $this->UserService->forgotPassword($userData[0]->id);

            $this->sendNewPassword($userData[0]->username, $userData[0]->email, $newPassword, $userData[0]->phone_number);

            if ($newPassword) {
                return ResponseHelper::get($newPassword);
            }
        } else {
            return ResponseHelper::error("User tidak ditemukan", 401);
        }
    }

    public function sendNewPassword($username, $email, $newPassword, $phoneNumber)
    {
        $client = new Client();
        $res = $client
            ->request('GET', 'https://jlfbackend.xyz/jlf-admin-panel/public/api/forgetpassword/' . $username . '/' . $phoneNumber . '/' . $email . '/' . $newPassword);

        // {"type":"User"...'
    }

    public function logout(Request $request)
    {
        $token = $request->header('Authorization');
        $this->AuthenticationService->removeToken($token);
        return ResponseHelper::put();
    }

    public function updateDataRedis($token, $userId)
    {
        $data = $this->UserService->get($userId);
        $data["token_redis"] = $token;
        $this->AuthenticationService->setTokenData($token, $data);
    }

    public function addPoinVerifOTP($userId, Request $request)
    {
        $token = $request->header('Authorization');

        $bool = $this->UserService->addPoint($userId, 1, "Selamat Anda telah mendapatkan 1 poin karena Anda telah melakukan verifikasi via OTP");
        $this->updateDataRedis($token, $userId);

        if ($bool) {
            $this->status = 'succes';
            $this->message = 'Berhasil mengupdate';
            $this->statusCode = 201;
        } else {
            $this->status = 'failed';
            $this->message = 'Gagal mengupdate';
            $this->statusCode = 201;
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message,
        ], $this->statusCode);
    }

    public function getPointCoupon($userId)
    {
        $data = $this->UserService->getPointCoupon($userId);
        return ResponseHelper::get($data);
    }
}
