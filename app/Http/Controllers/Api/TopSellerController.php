<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TopSellerService;
use App\Helpers\ResponseHelper;

class TopSellerController extends Controller
{
    private $TopSellerService;

    public function __construct(TopSellerService $TopSellerService) {
        $this->TopSellerService = $TopSellerService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->TopSellerService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->TopSellerService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->TopSellerService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($animalCategoryId) {
        $data = $this->TopSellerService->get($animalCategoryId);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->TopSellerService->getAll();
        return ResponseHelper::get($data);
    }

    public function getAllBySubCategory($animalSubCategoryId) {
        $data = $this->TopSellerService->getAllBySubCategory($animalSubCategoryId);
        return ResponseHelper::get($data);
    }
    
    public function getAllByCategory($animalCategoryId) {
        $data = $this->TopSellerService->getAllByCategory($animalCategoryId);
        return ResponseHelper::get($data);
    }

    public function getPromotedTopSeller() {
        $data = $this->TopSellerService->getPromotedTopSeller();
        return ResponseHelper::get($data);
    }    
}
