<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TransactionService;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Schema;
use App\Models\Transaction;
use App\Models\BalanceUser;

class TransactionController extends Controller
{
    private $TransactionService;

    public function __construct(TransactionService $TransactionService, Transaction $Transaction,BalanceUser $PointHistory) {
        $this->TransactionService = $TransactionService;
        $this->Transaction = $Transaction;
        $this->BalanceUser = $PointHistory;
    }

      function generateReference()
    {
         $today = date('YmdHi');
    $startDate = date('YmdHi', strtotime('-10 days'));
    $range = $today - $startDate;
    $rand1 = rand(0, $range);
    $rand2 = rand(0, 600000);
    return  $value=($rand1+$rand2);
    }
    
    
    public function buyitem(Request $request) {
        $request = $request->all();
          $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("YmdHis",$date_array[1]);
        
          $url = 'https://www.nicepay.co.id/nicepay/api/vacctCustomerRegist.do';
    
        $trxid= $this->generateReference();
$mcid='JLFAUNA002';

$mkey='q4XTK6wA7Vw4PHAB2k5Bf8jdLR1zINkOVdak2PirJHZG4626YZMnAqrkZDXvIfq2gWcI8fnqBSa3HG97+baiMg==';

$mtoken = hash('sha256',$mcid.$trxid.$mkey);

      $headers= [
         
          'Accept: */*',
          'Accept-Encoding: gzip, deflate',
          'Cache-Control: no-cache',
         'Connection: keep-alive',
         'Content-Length: 133',
         'Content-Type: application/x-www-form-urlencoded' ,
         'Host: dev.nicepay.co.id',
         'Postman-Token: 9cc0823a-85ce-41b2-8693-875f075ea4b1,db0d4a4b-d829-4690-aec2-d451f8b0523a',
         'User-Agent: PostmanRuntime/7.20.1',
        'cache-control: no-cache'
          
          
          ];
     
      
     
      
     

      $curl = curl_init();

     
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //  curl_setopt($curl, CURLOPT_USERPWD, $apiKey.":");
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, 'iMid='.$mcid.'&customerId='.$trxid.'&customerNm=bejotenan&merchantToken='.$mtoken);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $result = curl_exec($curl);
      
      
      $response = json_decode($result);
      $data = json_encode($response->vacctInfoList);
      $datas = json_decode($data,true);
      
   

$testAnswers = "";
$kodebank = "";
$nomerVacc = "";
 $csID = $response->customerId;
      
              
         
                
                $balanceUser = new Transaction();
                $balanceUser->animal_id = $request['animal_id'];
                $balanceUser->seller_user_id = $request['seller_user_id'];
                $balanceUser->buyer_user_id = $request['buyer_user_id'];
                $balanceUser->admin_user_id = $request['admin_user_id'];
                $balanceUser->type = $request['type'];
                $balanceUser->price = $request['price'];
                $balanceUser->invoice_number = $request['invoice_number'];
                $balanceUser->seller_bank_name = $request['seller_bank_name'];
                
                foreach ($datas as $question) {
    $testAnswers .= $question['bankCd'] . ": " . $question['vacctNo'] . ",";
    
      $test = array(
          'result' => $testAnswers
          );
          //$arr = $datas["bankCd"];
    //if($request['seller_bank_name'] == $resStr){
    $balanceUser->seller_bank_account_number =  $testAnswers;
   // }
}

                
               
                $balanceUser->save();
                
             
             
              $balanceUsers = new BalanceUser();
                $balanceUsers->information = 'Deposit user';
                $balanceUsers->user_id = $request['buyer_user_id'];
                $balanceUsers->animal_id = $request['animal_id'];
                 $balanceUsers->type = $request['type'];
                $balanceUsers->balances = $request['price'];
                
                $balanceUsers->callbackUrl = '#';
               // $balanceUser->dbProcessUrl = $response->dbProcessUrl;
                $balanceUsers->merchantToken = $mtoken;
                $balanceUsers->paymentdate = $date;
                $balanceUsers->paymenttime = $date;
                $balanceUsers->paymentdate = $date;
               
                
                $balanceUsers->paymentmethod = '02';
               
               
               foreach ($datas as $question) {
    $kodebank .= $question['bankCd'] .",";
     $nomerVacc .= $question['vacctNo'] . ", ";
    
    /*
     if ( strpos($nomerVacc, 'BMRI' ) !== false ) {
         
         $nomerVacc .= $question['vacctNo'] . ", ";
  $balanceUser->bmandiri =   'BMRI';
} else if ( strpos($kodebank, 'BDIN' ) !== false ){
 // echo "Not found";
}
*/
       
 
  
}
 $balanceUsers->bankCode =  $kodebank;
   $balanceUsers->vaccNo =  $nomerVacc;
     $balanceUsers->virtualacc =   $testAnswers;
   
 $balanceUsers->tXid = $csID;
    $balanceUsers->referenceno = '0';
  
$balanceUsers->status = '1';
 $balanceUsers->save();
            
           return ResponseHelper::get(    $balanceUsers); 
       
      
               
        //$this->TransactionService->create($request);
        
    }
    
    
    public function create(Request $request) {
        $request = $request->all();
        $this->TransactionService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->TransactionService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $data = $request->only(Schema::getColumnListing("transactions"));
        $this->TransactionService->update($id, $data);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->TransactionService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->TransactionService->getAll();
        return ResponseHelper::get($data);
    }

    public function getOrGenerateAuctionTransaction($auctionId) {
        $data = $this->TransactionService->getOrGenerateAuctionTransaction($auctionId);
        return ResponseHelper::get($data);
    }

}
