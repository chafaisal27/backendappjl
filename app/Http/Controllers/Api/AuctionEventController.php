<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AuctionEventService;
use App\Helpers\ResponseHelper;

class AuctionEventController extends Controller
{
    private $AuctionEventService;

    public function __construct(AuctionEventService $AuctionEventService) {
        $this->AuctionEventService = $AuctionEventService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->AuctionEventService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AuctionEventService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->AuctionEventService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->AuctionEventService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->AuctionEventService->getAll();
        return ResponseHelper::get($data);
    }

}
