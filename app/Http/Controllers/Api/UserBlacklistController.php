<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserBlacklistService;
use App\Helpers\ResponseHelper;

class UserBlacklistController extends Controller
{
    private $UserBlacklistService;

    public function __construct(UserBlacklistService $UserBlacklistService) {
        $this->UserBlacklistService = $UserBlacklistService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->UserBlacklistService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->UserBlacklistService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->UserBlacklistService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->UserBlacklistService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->UserBlacklistService->getAll();
        return ResponseHelper::get($data);
    }

}
