<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnimalSubCategoryService;
use App\Helpers\ResponseHelper;

class AnimalSubCategoryController extends Controller
{
    private $AnimalSubCategoryService;

    public function __construct(AnimalSubCategoryService $AnimalSubCategoryService) {
        $this->AnimalSubCategoryService = $AnimalSubCategoryService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->AnimalSubCategoryService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AnimalSubCategoryService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->AnimalSubCategoryService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->AnimalSubCategoryService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->AnimalSubCategoryService->getAll();
        return ResponseHelper::get($data);
    }

    public function getByAnimalCategoryId($id) {
        $data = $this->AnimalSubCategoryService->getByAnimalCategoryId($id);
        return ResponseHelper::get($data);   
    }
}
