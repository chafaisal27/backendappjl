<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AuctionEventParticipantService;
use App\Helpers\ResponseHelper;

class AuctionEventParticipantController extends Controller
{
    private $AuctionEventParticipantService;

    public function __construct(AuctionEventParticipantService $AuctionEventParticipantService) {
        $this->AuctionEventParticipantService = $AuctionEventParticipantService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->AuctionEventParticipantService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AuctionEventParticipantService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->AuctionEventParticipantService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->AuctionEventParticipantService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->AuctionEventParticipantService->getAll();
        return ResponseHelper::get($data);
    }

}
