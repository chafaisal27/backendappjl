<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LoginLogService;
use App\Helpers\ResponseHelper;

class LoginLogController extends Controller
{
    private $LoginLogService;

    public function __construct(LoginLogService $LoginLogService) {
        $this->LoginLogService = $LoginLogService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->LoginLogService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->LoginLogService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->LoginLogService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->LoginLogService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->LoginLogService->getAll();
        return ResponseHelper::get($data);
    }

}
