<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BalanceUserService;
use App\Helpers\ResponseHelper;
use App\Lib\NicepayDirect\NicepayLib;
use App\Models\BalanceUser;

class BalanceUserController extends Controller
{
    private $PointHistoryService;

    public function __construct(BalanceUserService $PointHistoryService,BalanceUser $PointHistory) {
        $this->BalanceUserService = $PointHistoryService;
        $this->BalanceUser = $PointHistory;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->BalanceUserService->create($request);
        return ResponseHelper::create();
    }
    
    
     function generateReference()
    {
         $today = date('YmdHi');
    $startDate = date('YmdHi', strtotime('-10 days'));
    $range = $today - $startDate;
    $rand1 = rand(0, $range);
    $rand2 = rand(0, 600000);
    return  $value=($rand1+$rand2);
    }
    
    
     public function DBProcessURL(Request $request)
    {
        if($request->isMethod('post')){
          $iMid=$request->input('iMid');
          print_r($iMid);exit;
        }
        // $data = $request->getContent();
        $tXid=$request->input('tXid');
        print_r($tXid);exit;

        $nicepay = new NicepayLib();

        //Listen for parameters passed

        $pushParameters = array(
            'tXid',
            'referenceNo',
            'merchantToken',
            'amt',
        );

        $nicepay->extractNotification($pushParameters);

        $iMid         = $nicepay->iMid;
        $tXid         = $nicepay->getNotification('tXid');
        $referenceNo  = $nicepay->getNotification('referenceNo');
        $amt          = $nicepay->getNotification('amt');
        $pushedToken  = $nicepay->getNotification('merchantToken');

        $nicepay->set('tXid',$tXid);
        $nicepay->set('amt',$amt);
        $nicepay->set('iMid',$iMid);

        if(isset($referenceNo))
        {
            $nicepay->set('referenceNo',$referenceNo);
            $transactionToken = $nicepay->transactionToken();
            $nicepay->set('merchantToken',$transactionToken);

            $paymentStatus = $nicepay->checkPaymentStatus($tXid, $referenceNo, $amt);

            //echo $pushedToken.'</br>'.$transactionToken;

            $response = array(
            		'reqTm'			=> $paymentStatus->reqTm,
            		'instmntType'	=> $paymentStatus->instmntType,
            		'resultMsg'		=> $paymentStatus->resultMsg,
            		'reqDt'			=> $paymentStatus->reqDt,
            		'instmntMon'	=> $paymentStatus->instmntMon,
            		'status'		=> $paymentStatus->status,
            		'tXid'			=> $paymentStatus->tXid
            );

            if($pushedToken == $transactionToken)
            {
                if(isset($paymentStatus->status) && $paymentStatus->status == '0')
                {
                    $datas = "<pre>Paid</pre>";
                    
                     return  ResponseHelper::get($datas);
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '1')
                {
                     $datas = "<pre>Reversal</pre>";
                      return  ResponseHelper::get($datas);
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '3')
                {
                    $datas =  "<pre>Cancel</pre>";
                    return  ResponseHelper::get($datas);
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '4')
                {
                    $datas = "<pre>Expired</pre>";
                    return  ResponseHelper::get($datas);
                }
                else
                {
                    $datas = "<pre>Status Unknown</pre>";
                    return  ResponseHelper::get($datas);
                }
            }
        }
    }
    
     public function deposit(Request $request)
    {
         //$request = $request->all();
         $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("YmdHis",$date_array[1]);
        
         $url = 'https://www.nicepay.co.id/nicepay/api/vacctCustomerRegist.do';
    
        $trxid= $this->generateReference();
$mcid='JLFAUNA002';

$mkey='q4XTK6wA7Vw4PHAB2k5Bf8jdLR1zINkOVdak2PirJHZG4626YZMnAqrkZDXvIfq2gWcI8fnqBSa3HG97+baiMg==';

$mtoken = hash('sha256',$mcid.$trxid.$mkey);

      $headers= [
         
          'Accept: */*',
          'Accept-Encoding: gzip, deflate',
          'Cache-Control: no-cache',
         'Connection: keep-alive',
         'Content-Length: 133',
         'Content-Type: application/x-www-form-urlencoded' ,
         'Host: dev.nicepay.co.id',
         'Postman-Token: 9cc0823a-85ce-41b2-8693-875f075ea4b1,db0d4a4b-d829-4690-aec2-d451f8b0523a',
         'User-Agent: PostmanRuntime/7.20.1',
        'cache-control: no-cache'
          
          
          ];
     
      
     
      
     

      $curl = curl_init();

     
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //  curl_setopt($curl, CURLOPT_USERPWD, $apiKey.":");
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, 'iMid='.$mcid.'&customerId='.$trxid.'&customerNm=bejotenan&merchantToken='.$mtoken);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $result = curl_exec($curl);
      
      
      $response = json_decode($result);
      $data = json_encode($response->vacctInfoList);
      $datas = json_decode($data,true);
      $datasd = json_encode($response);
      
   

$kodebank = "";
$nomerVacc = "";

$csID = $response->customerId;
        
                $balanceUser = new BalanceUser();
                $balanceUser->information = 'Deposit user';
                $balanceUser->user_id = $request['id'];
                $balanceUser->animal_id = $request['auction_id'];
                 $balanceUser->type = $request['role'];
                $balanceUser->balances = $request['winamount'];
                
                $balanceUser->callbackUrl = '#';
               // $balanceUser->dbProcessUrl = $response->dbProcessUrl;
                $balanceUser->merchantToken = $mtoken;
                $balanceUser->paymentdate = $date;
                $balanceUser->paymenttime = $date;
                $balanceUser->paymentdate = $date;
               
                
                $balanceUser->paymentmethod = '02';
               
               
               foreach ($datas as $question) {
    $kodebank .= $question['bankCd'] .",";
     $nomerVacc .= $question['vacctNo'] . ", ";
    
    /*
     if ( strpos($nomerVacc, 'BMRI' ) !== false ) {
         
         $nomerVacc .= $question['vacctNo'] . ", ";
  $balanceUser->bmandiri =   'BMRI';
} else if ( strpos($kodebank, 'BDIN' ) !== false ){
 // echo "Not found";
}
*/
       
 
  
}
 $balanceUser->bankCode =  $kodebank;
   $balanceUser->vaccNo =  $nomerVacc;
     $balanceUser->virtualacc =  '0';
   
 $balanceUser->tXid = $csID;
    $balanceUser->referenceno = '0';
  
$balanceUser->status = '1';
 $balanceUser->save();
            // $data = $this->BalanceUserService->deposit($request);
                
                return  ResponseHelper::get($balanceUser);

    }
    
    
     public function get($id) {
        $data = $this->BalanceUserService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAllByUserID($userId) {
        $data = $this->BalanceUserService->getAllByUserID($userId);
        return ResponseHelper::get($data);
    }

   
}
