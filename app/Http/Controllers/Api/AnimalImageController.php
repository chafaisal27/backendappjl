<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnimalImageService;
use App\Helpers\ResponseHelper;

class AnimalImageController extends Controller
{
    private $AnimalImageService;

    public function __construct(AnimalImageService $AnimalImageService) {
        $this->AnimalImageService = $AnimalImageService;
    }

    public function create($animalId, Request $request) {
        $request = $request->all();
        $this->AnimalImageService->create($animalId, $request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AnimalImageService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->AnimalImageService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->AnimalImageService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->AnimalImageService->getAll();
        return ResponseHelper::get($data);
    }

}
