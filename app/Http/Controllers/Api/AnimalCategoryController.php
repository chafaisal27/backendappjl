<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnimalCategoryService;
use App\Helpers\ResponseHelper;

class AnimalCategoryController extends Controller
{
    private $AnimalCategoryService;

    public function __construct(AnimalCategoryService $AnimalCategoryService) {
        $this->AnimalCategoryService = $AnimalCategoryService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->AnimalCategoryService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AnimalCategoryService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->AnimalCategoryService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->AnimalCategoryService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAllAnimals() {
        $data = $this->AnimalCategoryService->getAllAnimals();
        return ResponseHelper::get($data);
    }

    public function getAllProductAnimals() {
        $data = $this->AnimalCategoryService->getAllProductAnimals();
        return ResponseHelper::get($data);
    }

    public function getAllProductAccessoryAnimals() {
        $data = $this->AnimalCategoryService->getAllProductAccessoryAnimals();
        return ResponseHelper::get($data);
    }

    public function getAllProductFeedsAnimals() {
        $data = $this->AnimalCategoryService->getAllProductFeedsAnimals();
        return ResponseHelper::get($data);
    }

    public function getAllAnimalsWithoutCount($type) {
        $data = $this->AnimalCategoryService->getAllAnimalsWithoutCount($type);
        return ResponseHelper::get($data);
    }

    

    

}
