<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RollingJobAdminService;
use App\Helpers\ResponseHelper;

class RollingJobAdminController extends Controller
{
    private $RollingJobAdminService;

    public function __construct(RollingJobAdminService $RollingJobAdminService) {
        $this->RollingJobAdminService = $RollingJobAdminService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->RollingJobAdminService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->RollingJobAdminService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->RollingJobAdminService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get() {
        $data = $this->RollingJobAdminService->getNextAdminId();
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->RollingJobAdminService->getAll();
        return ResponseHelper::get($data);
    }

}
