<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PointHistoryService;
use App\Helpers\ResponseHelper;

class PointHistoryController extends Controller
{
    private $PointHistoryService;

    public function __construct(PointHistoryService $PointHistoryService) {
        $this->PointHistoryService = $PointHistoryService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->PointHistoryService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->PointHistoryService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->PointHistoryService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->PointHistoryService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAllByUserID($userId) {
        $data = $this->PointHistoryService->getAllByUserID($userId);
        return ResponseHelper::get($data);
    }
}
