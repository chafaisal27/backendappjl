<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProvinceService;
use App\Helpers\ResponseHelper;

class ProvinceController extends Controller
{
    private $ProvinceService;

    public function __construct(ProvinceService $ProvinceService) {
        $this->ProvinceService = $ProvinceService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->ProvinceService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->ProvinceService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->ProvinceService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->ProvinceService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->ProvinceService->getAll();
        return ResponseHelper::get($data);
    }

}
