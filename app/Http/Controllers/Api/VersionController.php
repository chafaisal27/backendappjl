<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\StaticService;

class VersionController extends Controller
{
    private $StaticService;

    public function __construct(StaticService $StaticService)
    {
        $this->StaticService = $StaticService;
    }

    public function checkVersion($version)
    {
        $curentVersion = "v0.1.9";
        $isUpToDate = true;

        $isForceUpdate = true;

        if ($curentVersion != $version) {
            $isUpToDate = false;
        }

        $data = $this->StaticService->getAll();
        

        $url = "https://play.google.com/store/apps/details?id=com.jlf.mobile";
        $msg = $data[0]["pop_up_text_update_app"];

        return ResponseHelper::get(["is_up_to_date" => $isUpToDate, "is_force_update" => $isForceUpdate, "url" => $url, "message" => $msg]);
    }
}
