<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserReportService;
use App\Helpers\ResponseHelper;

class UserReportController extends Controller
{
    private $UserReportService;

    public function __construct(UserReportService $UserReportService) {
        $this->UserReportService = $UserReportService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->UserReportService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->UserReportService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->UserReportService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->UserReportService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->UserReportService->getAll();
        return ResponseHelper::get($data);
    }

}
