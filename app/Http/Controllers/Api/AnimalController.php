<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AnimalImageService;
use App\Services\AnimalService;
use App\Services\AuctionService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Response;
use Schema;

class AnimalController extends Controller
{
    private $AnimalService, $AuctionService, $AnimalImageService, $UserService;

    public function __construct(AnimalService $AnimalService, AuctionService $AuctionService, AnimalImageService $AnimalImageService, UserService $UserService)
    {
        $this->AnimalService = $AnimalService;
        $this->UserService = $UserService;
    }

    public function downloadRestrictedAnimals()
    {
        $file = public_path() . "/files/sample-file.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'Daftar Hewan Terlarang UU Republik Indonesia.pdf', $headers);
    }

    public function test()
    {
        return realpath('images/animals/originals');
    }

    public function create(Request $request)
    {
        $userData = $this->UserService->get($request['animal']['owner_user_id']);

        if ($userData) {
            if ($userData->blacklisted == 1) {
                return ResponseHelper::error("Gagal menambah produk, Anda masuk dalam blacklist user", 407);
            }
            if ($userData->verification_status != 'verified') {
                return ResponseHelper::error("Gagal menambah produk, data diri belum terverifikasi oleh Admin", 408);
            }
        }


        $response = $this->AnimalService->create($request);


        if (!$response) {
            return ResponseHelper::error("Gagal menambah produk", 406);
        } else {
            return ResponseHelper::create();
        }
    }

    public function delete($id)
    {
        $this->AnimalService->delete($id);
        return ResponseHelper::delete();
    }

    public function updateAnimal($id, Request $request)
    {

        $this->UserService->get($request['animal']['owner_user_id']);

        $response = $this->AnimalService->update($id, $request);


        if (!$response) {
            return ResponseHelper::error("Gagal mengubah produk", 406);
        } else {
            return ResponseHelper::put();
        }
    }

    public function update($id, Request $request)
    {
        $request = $request->only(Schema::getColumnListing('animals'));

        $this->AnimalService->basicUpdate($id, $request);
        return ResponseHelper::put();
    }

    public function getAuction($id)
    {
        $data = $this->AnimalService->getAuction($id);
        return ResponseHelper::get($data);
    }

    public function getProduct($id)
    {
        $data = $this->AnimalService->getProduct($id);
        return ResponseHelper::get($data);
    }

    public function getAllAuctionCategory(Request $request, $per_page = 5)
    {
        $data = $this->AnimalService->getAllByCategory($request->categoryId, $request->animal_name, $request->sort_by, 20);

        return ResponseHelper::get($data);
    }

    public function getAllAuctionSubCategory(Request $request, $per_page = 5)
    {
        $data = $this->AnimalService->getAllBySubCategory($request->subCategoryId, $request->animal_name, $request->sort_by, 20);

        return ResponseHelper::get($data);
    }

    public function getAllProductSubCategory(Request $request, $per_page = 5)
    {
        $data = $this->AnimalService->getAllProductBySubCategory($request->subCategoryId, $request->animal_name, $request->sort_by, 20);

        return ResponseHelper::get($data);
    }

    public function getAllProductCategory(Request $request, $per_page = 5)
    {
        $data = $this->AnimalService->getAllProductByCategory($request->categoryId, $request->animal_name, $request->sort_by, 20);

        return ResponseHelper::get($data);
    }

    public function getAllUnauctionedByUserID($userId)
    {
        $data = $this->AnimalService->getAllUnauctionedByUserID($userId);
        return ResponseHelper::get($data);
    }

    public function getDraftAnimal(Request $request, $userId)
    {
        $data = $this->AnimalService->getDraftAnimal($userId, $request->animal_name);
        return ResponseHelper::get($data);
    }

    public function getAllByUserID($userId)
    {
        $data = $this->AnimalService->getAllByUserID($userId);
        return ResponseHelper::get($data);
    }

    public function getUserAuctions(Request $request, $userId)
    {
        $data = $this->AnimalService->getUserAuctions($userId, $request->animal_name);
        return ResponseHelper::get($data);
    }

    public function getUserProduct(Request $request, $userId)
    {
        $data = $this->AnimalService->getUserProduct($userId, $request->animal_name);
        return ResponseHelper::get($data);
    }

    public function getUserBids($userId, Request $request)
    {
        $data = $this->AnimalService->getUserBids($userId, $request->sort_by);
        return ResponseHelper::get($data);
    }

    public function getUserCommentsAuction($userId, Request $request)
    {
        $data = $this->AnimalService->getUserCommentsAuction($userId, $request->sort_by);
        return ResponseHelper::get($data);
    }

    public function getUserCommentsProduct($userId, Request $request)
    {
        $data = $this->AnimalService->getUserCommentsProduct($userId, $request->sort_by);
        return ResponseHelper::get($data);
    }

    public function countAll()
    {
        $data = $this->AnimalService->countAll();
        return $data ? $data : 0;
    }

    public function getSponsoredAnimals()
    {
        $data = $this->AnimalService->getSponsoredAnimals();
        return ResponseHelper::get($data);
    }
    
}
