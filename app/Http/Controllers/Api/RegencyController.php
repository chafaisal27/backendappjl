<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RegencyService;
use App\Helpers\ResponseHelper;

class RegencyController extends Controller
{
    private $RegencyService;

    public function __construct(RegencyService $RegencyService) {
        $this->RegencyService = $RegencyService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->RegencyService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->RegencyService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->RegencyService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->RegencyService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->RegencyService->getAll();
        return ResponseHelper::get($data);
    }

    public function getByProvinceId($id) {
        $data = $this->RegencyService->getByProvinceId($id);
        return ResponseHelper::get($data);   
    }

}
