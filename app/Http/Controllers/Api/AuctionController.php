<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AuctionService;
use App\Services\UserService;
use App\Services\RollingJobAdminService;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\FirebaseController;
use Schema;

class AuctionController extends Controller
{
    private $FirebaseController;
    private $AuctionService;
    private $UserService;
    private $RollingJobAdminService;


    public function __construct(AuctionService $AuctionService, RollingJobAdminService $RollingJobAdminService, UserService $UserService, FirebaseController $FirebaseController)
    {
        $this->AuctionService = $AuctionService;
        $this->UserService = $UserService;
        $this->RollingJobAdminService = $RollingJobAdminService;
        $this->FirebaseController = $FirebaseController;
    }

    public function create(Request $request, $animalId)
    {
        $response = $this->AuctionService->create($request, $animalId);

        if (!$response) {
            return ResponseHelper::error("Gagal membuat lelang", 406);
        } else {
            return ResponseHelper::create();
        }
    }

    public function delete($id)
    {
        $this->AuctionService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->only(Schema::getColumnListing('auctions'));
        
        $this->AuctionService->update($id, $request);
        return ResponseHelper::put();
    }

    public function close($id)
    {
        $update = array();
    }

    public function setChatRoom($id, Request $request)
    {
        $request = $request->only(Schema::getColumnListing('auctions'));

        DB::transaction(function () use ($id, $request) {
            $adminId =  $this->RollingJobAdminService->getNextAdminId($id);
            $request["admin_id"] = $adminId;

            // Firebase Notification - ADMIN
            $admin = $this->UserService->get($adminId);

            $fcmTokens = array();
            $fcmTokens[] = $admin->firebase_token;

            $fcmTitle = "Ada Lelang Baru Dimenangkan!";
            $fcmBody = "Cek 'Chat Rekber' Segera";

            $this->FirebaseController->sendNotifications($fcmTokens, $fcmTitle, $fcmBody);
            // End of Firebase Notification

            $this->AuctionService->update($id, $request);
        });

        return ResponseHelper::put();
    }

    public function getChatRoom($id)
    {
        $data = $this->AuctionService->get($id);

        return $data->firebase_chat_id;
    }

    public function get($id)
    {
        $data = $this->AuctionService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->AuctionService->getAll();
        return ResponseHelper::get($data);
    }

    public function getPopularAuctions()
    {
        $data = $this->AuctionService->getPopularAuctions();
        return ResponseHelper::get($data);
    }

    public function getAllByUserID($userId)
    {
        $data = $this->AuctionService->getAllByUserID($userId);
        return ResponseHelper::get($data);
    }

    public function setWinner($id)
    {
        $data = $this->AuctionService->setWinner($id);
        if ($data) {
            return ResponseHelper::put();
        } else {
            return ResponseHelper::error("Gagal memilih pemenang lelang", 406);
        }
    }

    public function cancel($id)
    {
        $data = $this->AuctionService->cancel($id);
        if ($data) {
            return ResponseHelper::put();
        } else {
            return ResponseHelper::error("Gagal membatalkan lelang", 406);
        }
    }

    public function start($id)
    {
        $data = $this->AuctionService->start($id);
        if ($data) {
            return ResponseHelper::put();
        } else {
            return ResponseHelper::error("Gagal memulai lelang", 406);
        }
    }

    public function autoClose()
    {
        $data = $this->AuctionService->autoClose();
        if ($data) {
            return ResponseHelper::put();
        } else {
            return ResponseHelper::error("Gagal menutup otomatis lelang", 406);
        }
    }

    public function getAllWithActiveChat(Request $request, $userId)
    {
        $data = $this->AuctionService->getAllWithActiveChat($userId, $request);
        return ResponseHelper::get($data);
    }

    public function getAllWithActiveChatAdmin(Request $request)
    {
        $data = $this->AuctionService->getAllWithActiveChatAdmin($request);
        return ResponseHelper::get($data);
    }

    public function getAuctionWinnerNotConfirmed()
    {
        $data = $this->AuctionService->getAuctionWinnerNotConfirmed();
        return ResponseHelper::get($data);
    }
    public function getAllWithActiveChatNotPaginate($userId)
    {
        $data = $this->AuctionService->getAllWithActiveChatNotPaginate($userId);
        return ResponseHelper::get($data);
    }

    public function getAuctionEventParticipants()
    {
        $data = $this->AuctionService->getAuctionEventParticipants();
        return ResponseHelper::get($data);
    }
}
