<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PromoCategoryService;
use App\Helpers\ResponseHelper;

class PromoCategoryController extends Controller
{
    private $PromoCategoryService;

    public function __construct(PromoCategoryService $PromoCategoryService) {
        $this->PromoCategoryService = $PromoCategoryService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->PromoCategoryService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->PromoCategoryService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->PromoCategoryService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->PromoCategoryService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAllCategory($animalCategoryId) {
        $data = $this->PromoCategoryService->getAllCategory($animalCategoryId);
        return ResponseHelper::get($data);
    }

    public function getAllSubCategory($animalSubCategoryId) {
        $data = $this->PromoCategoryService->getAllSubCategory($animalSubCategoryId);
        return ResponseHelper::get($data);
    }

}
