<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SliderService;
use App\Helpers\ResponseHelper;

class SliderController extends Controller
{
    private $SliderService;

    public function __construct(SliderService $SliderService) {
        $this->SliderService = $SliderService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->SliderService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->SliderService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->SliderService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->SliderService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->SliderService->getAll();
        return ResponseHelper::get($data);
    }

}
