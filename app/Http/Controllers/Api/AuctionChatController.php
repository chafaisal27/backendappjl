<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AuctionChatService;
use App\Helpers\ResponseHelper;
use App\Lib\NicepayDirect\NicepayLib;
use App\Models\BalanceUser;

class AuctionChatController extends Controller
{
    private $AuctionChatService;

    public function __construct(AuctionChatService $AuctionChatService) {
        $this->AuctionChatService = $AuctionChatService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->AuctionChatService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->AuctionChatService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update(Request $request) {
        $request = $request->all();
        
        $this->AuctionChatService->update($request);
        return ResponseHelper::put();
    }
    
    function generateReference()
    {
        $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("YmdHis",$date_array[1]);
        $date_array[0] = preg_replace('/[^\p{L}\p{N}\s]/u', '', $date_array[0]);
        return "Ref".$date.$date_array[0].rand(100,999);
    }
    
    public function reset(Request $request) {
        $request = $request->all();
  

$nicepay = new NicepayLib();
$dateNow        = date('Ymd');
            $vaExpiryDate   = date('Ymd', strtotime($dateNow . ' +1 day')); // Set VA expiry date +1 day (optional)
            $bankCd         = 'CENA';

            // Populate Mandatory parameters to send
            $nicepay->set('payMethod', '02');
            $nicepay->set('currency', 'IDR');
            $nicepay->set('amt', $request['winamount']); // Total gross amount
            $nicepay->set('referenceNo', $this->generateReference()); // Invoice Number or Referenc Number Generated by merchant
            $nicepay->set('description', 'Payment of Invoice No '.$nicepay->get('referenceNo')); // Transaction description
            $nicepay->set('bankCd', $bankCd);

            $nicepay->set('billingNm', 'John Doe'); // Customer name
            $nicepay->set('billingPhone', '02112345678'); // Customer phone number
            $nicepay->set('billingEmail', 'john@example.com'); //
            $nicepay->set('billingAddr', 'Jl. Jend. Sudirman No. 28');
            $nicepay->set('billingCity', 'Jakarta Pusat');
            $nicepay->set('billingState', 'DKI Jakarta');
            $nicepay->set('billingPostCd', '10210');
            $nicepay->set('billingCountry', 'Indonesia');

            $nicepay->set('deliveryNm', 'John Doe'); // Delivery name
            $nicepay->set('deliveryPhone', '02112345678');
            $nicepay->set('deliveryEmail', 'john@example.com');
            $nicepay->set('deliveryAddr', 'Jl. Jend. Sudirman No. 28');
            $nicepay->set('deliveryCity', 'Jakarta Pusat');
            $nicepay->set('deliveryState', 'DKI Jakarta');
            $nicepay->set('deliveryPostCd', '10210');
            $nicepay->set('deliveryCountry', 'Indonesia');

            $nicepay->set('vacctValidDt', $vaExpiryDate); // Set VA expiry date example: +1 day
            $nicepay->set('vacctValidTm', date('His')); // Set VA Expiry Time

            // Send Data
            $response = $nicepay->requestVA();

            // Response from NICEPAY
            // var_dump($response);
            // exit();
           
            if (isset($response->resultCd) && $response->resultCd == "0000")
            {
                $balanceUser = new BalanceUser();
                $balanceUser->tXid = $response->tXid;
                $balanceUser->callbackUrl = $response->callbackUrl;
                $balanceUser->paymentdate = $response->transDt;
                $balanceUser->paymenttime = $response->transTm;
                $balanceUser->paymentdate = $response->transDt;
                $balanceUser->virtualacc = $response->bankVacctNo;
                $balanceUser->referenceno = $response->referenceNo;
                $balanceUser->paymentmethod = $response->payMethod;
                $reslt = $balanceUser->save();
                
                return  ResponseHelper::get($reslt);
               
            }
            elseif(isset($response->data->resultCd))
            {
               
                 $reslt = "Oops! Something happened, please notice your system administrator.\n\n";
                return  ResponseHelper::get($reslt);
            }
            else
            {
              
                
                $reslt = "<pre>Connection Timeout. Please Try again.</pre>";
                return  ResponseHelper::get($reslt);
            }
          

       // $this->AuctionChatService->reset($request);
        
    }

    public function get($id) {
        $data = $this->AuctionChatService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->AuctionChatService->getAll();
        return ResponseHelper::get($data);
    }

    public function count($userId)
    {
        $data = $this->AuctionChatService->count($userId);
        return $data;
    }
}
