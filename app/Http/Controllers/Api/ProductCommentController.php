<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AnimalService;
use App\Services\ProductCommentService;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Http\Request;
use DB;

class ProductCommentController extends Controller
{
    private $ProductCommentService;
    private $AnimalService;
    private $ProductService;
    private $UserService;

    public function __construct(ProductCommentService $ProductCommentService, AnimalService $AnimalService, ProductService $ProductService, UserService $UserService)
    {
        $this->ProductCommentService = $ProductCommentService;
        $this->AnimalService = $AnimalService;
        $this->ProductService = $ProductService;
        $this->UserService = $UserService;
    }

    public function create(Request $request)
    {
        $request = $request->all();

        $userData = $this->UserService->get($request["user_id"]);

        if($userData->blacklisted == 1){
            return ResponseHelper::error("Gagal memasang komentar, Anda masuk dalam blacklist user", 407);
        }

        // Commented, for later use
        // if ($userData->verification_status != 'verified') {
        //     return ResponseHelper::error("Gagal memasang komentar, data diri belum terverifikasi oleh Admin", 408);
        // }

        DB::transaction(function () use ($request) {
            $request["created_at"] = now();
            $this->ProductCommentService->create($request);

            $animalId = $this->ProductService->get($request["product_id"])->animal_id;
            $this->AnimalService->updateDate($animalId, ["updated_at" => now()]);
        }, 5);

        return ResponseHelper::create();
    }

    public function delete($id)
    {
        $this->ProductCommentService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->all();
        $this->ProductCommentService->update($id, $request);
        return ResponseHelper::put();
    }

    public function get($id)
    {
        $data = $this->ProductCommentService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->ProductCommentService->getAll();
        return ResponseHelper::get($data);
    }

}
