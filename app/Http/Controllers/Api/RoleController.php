<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RoleService;
use App\Helpers\ResponseHelper;

class RoleController extends Controller
{
    private $RoleService;

    public function __construct(RoleService $RoleService) {
        $this->RoleService = $RoleService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->RoleService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->RoleService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->RoleService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->RoleService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->RoleService->getAll();
        return ResponseHelper::get($data);
    }

}
