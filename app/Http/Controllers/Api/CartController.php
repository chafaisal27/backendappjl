<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CartService;
use App\Helpers\ResponseHelper;

class CartController extends Controller
{
    private $CartService;

    public function __construct(CartService $CartService) {
        $this->CartService = $CartService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->CartService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->CartService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->CartService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->CartService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->CartService->getAll();
        return ResponseHelper::get($data);
    }

}
