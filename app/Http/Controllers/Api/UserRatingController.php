<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserRatingService;
use App\Helpers\ResponseHelper;

class UserRatingController extends Controller
{
    private $UserRatingService;

    public function __construct(UserRatingService $UserRatingService) {
        $this->UserRatingService = $UserRatingService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->UserRatingService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->UserRatingService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->UserRatingService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->UserRatingService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->UserRatingService->getAll();
        return ResponseHelper::get($data);
    }

}
