<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FirebaseController extends Controller
{
    private $status = 'error';
    private $statusCode = 400;
    private $message = '';
    private $data;

    public function sendNotifications($tokens, $title, $body) {
        $array = array();
        $array['tokens'] = $tokens;
        $array['title'] = $title;
        $array['body'] = $body;
        
        $validator = Validator::make($array, [
            'tokens' => 'required',
            'title' => 'required',
            'body'  => 'required'
        ]);

        if ($validator->fails()) {
            $this->message = $validator->errors();
        } else {
            // $tokens, $title, $body
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);
    
            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($body)
                                ->setSound('default');
    
            $dataBuilder = new PayloadDataBuilder();
            // $dataBuilder->addData(['a_data' => 'my_data']);
    
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
    
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    
            if ($downstreamResponse->numberSuccess()) {
                $this->status = 'success';
                $this->message = 'Berhasil mengirimkan notifikasi';
                $this->statusCode = 200;
            }
            // return $downstreamResponse->numberSuccess();
        }

        return response()->json([
            'status' => $this->status,
            'message' => $this->message,
            'data' => $this->data
        ], $this->statusCode);
    }

    public function test() {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        // $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "c4jO-KuDFzA:APA91bFIOaxfYTqpW7QLQiCA5nbGaz8yQDG_kXHDbdD3o9pcyYNYMbEzcHBvMfnQA1-XpSMh6qLk3OYQelXor7ymBvLDD12VGscN-n7WZEc0INpW1poyfVZpKeqOx1arh8YUfKwbEtUd";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        return $downstreamResponse->numberSuccess();
    }
}
