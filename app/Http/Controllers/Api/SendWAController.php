<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SendWAService;
use App\Helpers\ResponseHelper;

class SendWAController extends Controller
{
    private $SendWAService;

    public function __construct(SendWAService $SendWAService) {
        $this->SendWAService = $SendWAService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->SendWAService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->SendWAService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->SendWAService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->SendWAService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->SendWAService->getAll();
        return ResponseHelper::get($data);
    }

    public function sendOTP(Request $request) {
        $request = $request->all();
        $phone = $request['phone'];
        $message = $request['message'];
        $this->SendWAService->sendMessage($phone, $message);
        return ResponseHelper::create();
    }
}
