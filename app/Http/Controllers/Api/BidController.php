<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AuctionService;
use App\Services\AnimalService;
use App\Services\BidService;
use App\Services\UserService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class BidController extends Controller
{
    private $BidService;
    private $AuctionService;
    private $AnimalService;
    private $UserService;

    public function __construct(BidService $BidService, AuctionService $AuctionService, AnimalService $AnimalService, UserService $UserService)
    {
        $this->BidService = $BidService;
        $this->AuctionService = $AuctionService;
        $this->AnimalService = $AnimalService;
        $this->UserService = $UserService;
    }

    public function create(Request $request)
    {
        $data = $request->only(Schema::getColumnListing("bids"));
        $result;

        $userData = $this->UserService->get($data["user_id"]);
        if ($userData->blacklisted == 1) {
            return ResponseHelper::error("Bid gagal, Anda masuk dalam blacklist user", 407);
        }
        if ($userData->verification_status != 'verified') {
            return ResponseHelper::error("Bid gagal, data diri belum terverifikasi oleh Admin", 408);
        }



        DB::transaction(function () use ($data, &$result) {
            $result = $this->BidService->create($data);
            $this->AuctionService->update($data["auction_id"], ["updated_at" => now()]);

            $animalId = $this->AuctionService->get($data["auction_id"])->animal_id;
            $this->AnimalService->updateDate($animalId, ["updated_at" => now()]);
        }, 5);

        //check if have some active chat rekber
        $tampChat = $this->AuctionService->getAllWithActiveChatNotPaginate($data["user_id"]);
        if (count($tampChat) > 0) {
            return ResponseHelper::error("Selesaikan Rekber yang lain terlebih dahulu", 409);
        }

        if (!$result) {
            return ResponseHelper::error("Tawaran tidak sesuai", 406);
        } else {
            return ResponseHelper::create();
        }
    }

    public function delete($id)
    {
        $delete = $this->BidService->delete($id);
        if ($delete) {
            return ResponseHelper::delete();
        } else {
            return ResponseHelper::error("Terjadi error", 406);
        }
    }

    public function update($id, Request $request)
    {
        $request = $request->all();
        $this->BidService->update($id, $request);
        return ResponseHelper::put();
    }

    public function get($id)
    {
        $data = $this->BidService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->BidService->getAll();
        return ResponseHelper::get($data);
    }

    public function countBidByUserID($userId)
    {
        $data = $this->BidService->countBidByUserID($userId);
        return $data;
    }
}
