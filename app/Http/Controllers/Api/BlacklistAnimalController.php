<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BlacklistAnimalService;
use App\Helpers\ResponseHelper;

class BlacklistAnimalController extends Controller
{
    private $BlacklistAnimalService;

    public function __construct(BlacklistAnimalService $BlacklistAnimalService) {
        $this->BlacklistAnimalService = $BlacklistAnimalService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->BlacklistAnimalService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->BlacklistAnimalService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->BlacklistAnimalService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->BlacklistAnimalService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->BlacklistAnimalService->getAll();
        return ResponseHelper::get($data);
    }

}
