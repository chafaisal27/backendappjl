<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use App\Helpers\ResponseHelper;

class ArticleController extends Controller
{
    private $ArticleService;

    public function __construct(ArticleService $ArticleService) {
        $this->ArticleService = $ArticleService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->ArticleService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->ArticleService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->ArticleService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->ArticleService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll($type) {
        $data = $this->ArticleService->getAll($type);
        return ResponseHelper::get($data);
    }

}
