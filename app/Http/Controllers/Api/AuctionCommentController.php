<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Services\AuctionCommentService;
use App\Services\AuctionService;
use App\Services\AnimalService;
use App\Services\UserService;
use DB;
use Illuminate\Http\Request;

class AuctionCommentController extends Controller
{
    private $AuctionCommentService;
    private $AuctionService;
    private $AnimalService;
    private $UserService;

    public function __construct(AuctionCommentService $AuctionCommentService, AuctionService $AuctionService, AnimalService $AnimalService, UserService $UserService)
    {
        $this->AuctionCommentService = $AuctionCommentService;
        $this->AuctionService = $AuctionService;
        $this->AnimalService = $AnimalService;
        $this->UserService = $UserService;
    }

    public function create(Request $request)
    {
        $userData = $this->UserService->get($request["user_id"]);

        if ($userData->blacklisted == 1) {
            return ResponseHelper::error("Gagal memasang komentar, Anda masuk dalam blacklist user", 407);
        }

        // Commented, for later use
        // if ($userData->verification_status != 'verified') {
        //     return ResponseHelper::error("Gagal memasang komentar, data diri belum terverifikasi oleh Admin", 408);
        // }

        

        $request = $request->all();
        DB::transaction(function () use ($request) {
            $this->AuctionCommentService->create($request);
            $this->AuctionService->update($request["auction_id"], ["updated_at" => now()]);

            $animalId = $this->AuctionService->get($request["auction_id"])->animal_id;
            $this->AnimalService->updateDate($animalId, ["updated_at" => now()]);
        }, 5);

        //check if have some active chat rekber
        $tampChat = $this->AuctionService->getAllWithActiveChatNotPaginate($request["user_id"]);
        if (count($tampChat) > 0) {
            return ResponseHelper::error("Selesaikan Rekber yang lain terlebih dahulu", 409);
        }
        return ResponseHelper::create();
    }

    public function delete($id)
    {
        $this->AuctionCommentService->delete($id);
        return ResponseHelper::delete();
    }

    public function update($id, Request $request)
    {
        $request = $request->all();
        $this->AuctionCommentService->update($id, $request);
        return ResponseHelper::put();
    }

    public function get($id)
    {
        $data = $this->AuctionCommentService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll()
    {
        $data = $this->AuctionCommentService->getAll();
        return ResponseHelper::get($data);
    }
}
