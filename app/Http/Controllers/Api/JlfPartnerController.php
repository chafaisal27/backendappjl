<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\JlfPartnerService;
use App\Helpers\ResponseHelper;

class JlfPartnerController extends Controller
{
    private $JlfPartnerService;

    public function __construct(JlfPartnerService $JlfPartnerService) {
        $this->JlfPartnerService = $JlfPartnerService;
    }

    public function create(Request $request) {
        $request = $request->all();
        $this->JlfPartnerService->create($request);
        return ResponseHelper::create();
    }

    public function delete($id) {
        $this->JlfPartnerService->delete($id);
        return ResponseHelper::delete();
    }
    
    public function update($id, Request $request) {
        $request = $request->all();
        $this->JlfPartnerService->update($id,$request);
        return ResponseHelper::put();
    }

    public function get($id) {
        $data = $this->JlfPartnerService->get($id);
        return ResponseHelper::get($data);
    }

    public function getAll() {
        $data = $this->JlfPartnerService->getAll();
        return ResponseHelper::get($data);
    }

}
