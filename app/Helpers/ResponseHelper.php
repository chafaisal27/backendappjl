<?php

namespace App\Helpers;

class ResponseHelper
{

    public static function get($data)
    {
        return response()->json($data, 200);
        // return response()->json($data, 200, [], JSON_NUMERIC_CHECK);
    }

    public static function create()
    {
        return response()->json(['content' => "created"], 201);
    }

    public static function put()
    {
        return $status = response()->json(['content' => "updated"], 202);
    }

    public static function delete()
    {
        return response()->json(['content' => "deleted"], 204);
    }

    public static function error($content, $responseCode)
    {
        return response()->json(['content' => $content], $responseCode);
    }

}
