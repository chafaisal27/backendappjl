<?php

namespace App\Lib\NicepayDirect;

/*
 * ____________________________________________________________
 *
 * Copyright (C) 2016 NICE IT&T
 *
 *
 * This config file may used as it is, there is no warranty.
 *
 * @ description : PHP SSL Client module.
 * @ name        : NicepayLite.php
 * @ author      : NICEPAY I&T (tech@nicepay.co.kr)
 * @ date        :
 * @ modify      : 09.03.2016
 *
 * 09.03.2016 Update Log
 *
 * ____________________________________________________________
 */

// Please set the following

// Merchant ID
define("NICEPAY_IMID",              "JLFAUNA002");      // IMID Default
// define("NICEPAY_IMID",              "CVSNORMAL0");      // IMID for test CVS
// define("NICEPAY_IMID",              "BMRITEST01");   // IMID for test VA & CC (Real Charge)

// API Key
define("NICEPAY_MERCHANT_KEY",      "q4XTK6wA7Vw4PHAB2k5Bf8jdLR1zINkOVdak2PirJHZG4626YZMnAqrkZDXvIfq2gWcI8fnqBSa3HG97+baiMg==");

// Merchant's result page URL
// define("NICEPAY_CALLBACK_URL",     "http://httpresponder.com/nicepay");
define("NICEPAY_CALLBACK_URL",      url('/')."/ThankYou");
// Merchant's notification handler URL
define("NICEPAY_DBPROCESS_URL",     url('/')."/Notification");

/* TIMEOUT - Define as needed (in seconds) */
define( "NICEPAY_TIMEOUT_CONNECT", 15 );
define( "NICEPAY_TIMEOUT_READ", 25 );


// Please do not change
define("NICEPAY_PROGRAM",           "NicepayLite");
define("NICEPAY_VERSION",           "1.11");
define("NICEPAY_BUILDDATE",         "20160309");

// Production
// define("NICEPAY_3DSECURE_URL",      "https://www.nicepay.co.id/nicepay/api/secureVeRequest.do");    // 3D Secure API URL
// define("NICEPAY_CHARGE_URL",        "https://www.nicepay.co.id/nicepay/api/onePass.do");            // Charge Credit Card API URL
// define("NICEPAY_REQ_VA_URL",        "https://www.nicepay.co.id/nicepay/api/onePass.do");            // Request Virtual Account API URL
// define("NICEPAY_REQ_CVS_URL",       "https://www.nicepay.co.id/nicepay/api/onePass.do");            // Request Virtual Account API URL
// define("NICEPAY_CANCEL_URL",        "https://www.nicepay.co.id/nicepay/api/onePassAllCancel.do");   // API URL FOR CANCEL ALL TYPES OF TRANSACTIONS
// define("NICEPAY_ORDER_STATUS_URL",  "https://www.nicepay.co.id/nicepay/api/onePassStatus.do");      // API URL FOR CHECK PAYMENT STATUS ALL TYPES OF TRANSACTIONS

// Development
define("NICEPAY_3DSECURE_URL",      "https://dev.nicepay.co.id/nicepay/api/secureVeRequest.do");    // 3D Secure API URL
define("NICEPAY_CHARGE_URL",        "https://dev.nicepay.co.id/nicepay/api/onePass.do");            // Charge Credit Card API URL
define("NICEPAY_REQ_VA_URL",        "https://www.nicepay.co.id/nicepay/api/vacctCustomerRegist.do");            // Request Virtual Account API URL
define("NICEPAY_REQ_CVS_URL",       "https://dev.nicepay.co.id/nicepay/api/onePass.do");            // Request Virtual Account API URL
define("NICEPAY_CANCEL_URL",        "https://dev.nicepay.co.id/nicepay/api/onePassAllCancel.do");   // API URL FOR CANCEL ALL TYPES OF TRANSACTIONS
define("NICEPAY_ORDER_STATUS_URL",  "https://dev.nicepay.co.id/nicepay/api/onePassStatus.do");      // API URL FOR CHECK PAYMENT STATUS ALL TYPES OF TRANSACTIONS

define("NICEPAY_READ_TIMEOUT_ERR",  "10200");

/* LOG LEVEL */

define("NICEPAY_LOG_CRITICAL", 1);
define("NICEPAY_LOG_ERROR", 2);
define("NICEPAY_LOG_NOTICE", 3);
define("NICEPAY_LOG_INFO", 5);
define("NICEPAY_LOG_DEBUG", 7);
