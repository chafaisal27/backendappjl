<?php

namespace App\Repositories;

use App\Models\StaticData;

class StaticRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(StaticData $Static) {
        $this->Static = $Static;
    }

    public function create($data) {
        return $this->Static::create($data);
    }

    public function update($id,$data) {
        return $this->Static::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Static::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Static::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Static::orderBy("created_at", "desc")->get();
    }

}
