<?php

namespace App\Repositories;

use App\Models\Article;

class ArticleRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(Article $Article)
    {
        $this->Article = $Article;
    }

    public function create($data)
    {
        return $this->Article::create($data);
    }

    public function update($id, $data)
    {
        return $this->Article::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->Article::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->Article::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAll($type)
    {
        return $this->Article::where("type", $type)->orderBy("order", "asc")->get();
    }

}
