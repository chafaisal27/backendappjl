<?php

namespace App\Repositories;

use App\Models\History;

class HistoryRepository
{
    CONST PRIMARY_KEY = 'id';

    public function __construct(History $History) {
        $this->History = $History;
    }

    public function create($data) {
        return $this->History::create($data);
    }

    public function update($id,$data) {
        return $this->History::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function updateWhereIn($id, $data) {
        return $this->History::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id) {
        return $this->History::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->History::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->History::orderBy("created_at", "desc")->get();
    }

    public function getAllByUserID($userId)
    {
        return $this->History::whereUserId($userId)->whereDate('created_at', '>=', now()->subDays(14))->orderBy("created_at", "desc")->get();
    }

    public function countHistoryByUserID($userId)
    {
        // Active = [0, new], [1, read]
        return $this->History::whereUserId($userId)->whereRead(0)->whereDate('created_at', '>=', now()->subDays(14))->count();
    }

}
