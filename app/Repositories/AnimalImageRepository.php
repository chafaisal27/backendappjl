<?php

namespace App\Repositories;

use App\Models\AnimalImage;

class AnimalImageRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AnimalImage $AnimalImage) {
        $this->AnimalImage = $AnimalImage;
    }

    public function create($data) {
        return $this->AnimalImage::create($data);
    }

    public function update($id,$data) {
        return $this->AnimalImage::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AnimalImage::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AnimalImage::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->AnimalImage::orderBy("created_at", "desc")->get();
    }

    public function getLastId() {
        return $this->AnimalImage::max('id');
    }

}
