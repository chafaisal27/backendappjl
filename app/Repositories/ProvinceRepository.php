<?php

namespace App\Repositories;

use App\Models\Province;

class ProvinceRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Province $Province) {
        $this->Province = $Province;
    }

    public function create($data) {
        return $this->Province::create($data);
    }

    public function update($id,$data) {
        return $this->Province::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Province::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Province::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Province::orderBy("created_at", "desc")->get();
    }

}
