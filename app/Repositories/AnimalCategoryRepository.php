<?php

namespace App\Repositories;

use App\Models\AnimalCategory;

class AnimalCategoryRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(AnimalCategory $AnimalCategory)
    {
        $this->AnimalCategory = $AnimalCategory;
    }

    public function create($data)
    {
        return $this->AnimalCategory::create($data);
    }

    public function update($id, $data)
    {
        return $this->AnimalCategory::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->AnimalCategory::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->AnimalCategory::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAllAnimals()
    {
        return $this->AnimalCategory::
            withCount(['animals' => function ($query) {
            $query->whereHas('auction', function ($query) {
                $query->where('active', 1);
            });
        }])
            ->with(['animal_sub_categories' => function ($query) {
                $query->withCount(['animals' => function ($query) {
                    $query->whereHas('auction', function ($query) {
                        $query->where('active', 1);
                    });
                }]);
            }])
            ->where("type", "animal")
            ->orderBy("created_at", "desc")
            ->get();
    }

    public function getAllProductAnimals()
    {
        return $this->AnimalCategory::
            withCount(['animals' => function ($query) {
            $query->whereHas('product', function ($query) {
                $query->where('status', "active");
            });
        }])
            ->with(['animal_sub_categories' => function ($query) {
                $query->withCount(['animals' => function ($query) {
                    $query->whereHas('product', function ($query) {
                        $query->where('status', "active");
                    });
                }]);
            }])
            ->where("type", "animal")
            ->orderBy("created_at", "desc")
            ->get();
    }

    public function getAllAnimalsWithoutCount($type)
    {
        return $this->AnimalCategory::select("id", "name", "is_video_allowed")
            ->where("type", $type)
            ->orderBy("name", "asc")
            ->get();
    }

    public function getAllProductAccessoryAnimals()
    {
        return $this->AnimalCategory::
            withCount(['animals' => function ($query) {
            $query->whereHas('product', function ($query) {
                $query->where('status', "active");
            });
        }])
            ->with(['animal_sub_categories' => function ($query) {
                $query->withCount(['animals' => function ($query) {
                    $query->whereHas('product', function ($query) {
                        $query->where('status', "active");
                    });
                }]);
            }])
            ->where("type", "accessory")
            ->orderBy("created_at", "desc")
            ->get();
    }

    public function getAllProductFeedsAnimals()
    {
        return $this->AnimalCategory::
            withCount(['animals' => function ($query) {
            $query->whereHas('product', function ($query) {
                $query->where('status', "active");
            });
        }])
            ->with(['animal_sub_categories' => function ($query) {
                $query->withCount(['animals' => function ($query) {
                    $query->whereHas('product', function ($query) {
                        $query->where('status', "active");
                    });
                }]);
            }])
            ->where("type", "feed")
            ->orderBy("created_at", "desc")
            ->get();
    }

}
