<?php

namespace App\Repositories;

use App\Models\UserReport;

class UserReportRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(UserReport $UserReport) {
        $this->UserReport = $UserReport;
    }

    public function create($data) {
        return $this->UserReport::create($data);
    }

    public function update($id,$data) {
        return $this->UserReport::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->UserReport::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->UserReport::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->UserReport::orderBy("created_at", "desc")->get();
    }

}
