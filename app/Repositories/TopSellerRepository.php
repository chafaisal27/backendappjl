<?php

namespace App\Repositories;

use App\Models\TopSeller;

class TopSellerRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(TopSeller $TopSeller) {
        $this->TopSeller = $TopSeller;
    }

    public function create($data) {
        return $this->TopSeller::create($data);
    }

    public function update($id,$data) {
        return $this->TopSeller::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->TopSeller::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($animalCategoryId) {
        return $this->TopSeller::with('user')->whereAnimalSubCategoryId($animalCategoryId)->get();
    }

    public function getAll() {
        return $this->TopSeller::orderBy("created_at", "desc")->get();
    }

    public function getAllBySubCategory($animalSubCategoryId) {
        return $this->TopSeller::with('user')->whereIn('animal_sub_category_id', $animalSubCategoryId)->inRandomOrder()->get();
    }

    public function getPromotedTopSeller() {
        return $this->TopSeller::where('is_promoted', 1)
        ->whereNull('deleted_at')
        ->get();
    }

}
