<?php

namespace App\Repositories;

use App\Models\JlfPartner;

class JlfPartnerRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(JlfPartner $JlfPartner) {
        $this->JlfPartner = $JlfPartner;
    }

    public function create($data) {
        return $this->JlfPartner::create($data);
    }

    public function update($id,$data) {
        return $this->JlfPartner::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->JlfPartner::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->JlfPartner::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->JlfPartner::orderBy("order", "asc")->get();
    }

}
