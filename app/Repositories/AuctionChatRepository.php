<?php

namespace App\Repositories;

use App\Models\AuctionChat;

class AuctionChatRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AuctionChat $AuctionChat) {
        $this->AuctionChat = $AuctionChat;
    }

    public function create($data) {
        return $this->AuctionChat::create($data);
    }

    public function update($id, $data) {
        return $this->AuctionChat::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AuctionChat::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AuctionChat::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getByAuctionChatId($id) {
        return $this->AuctionChat::where('firebase_chat_id',$id)->first();
    }

    public function getAll() {
        return $this->AuctionChat::orderBy("created_at", "desc")->get();
    }

    public function count($userId)
    {
        $count = $this->AuctionChat::whereHas('auction', function($q) {
            return $q->whereNull('deleted_at');
        })->whereSellerUserId($userId)->sum('seller_unread_count') ?? 0;

        $count += $this->AuctionChat::whereHas('auction', function($q) {
            return $q->whereNull('deleted_at');
        })->whereBuyerUserId($userId)->sum('buyer_unread_count') ?? 0;

        $count += $this->AuctionChat::whereHas('auction', function($q) {
            return $q->whereNull('deleted_at');
        })->whereAdminUserId($userId)->sum('admin_unread_count') ?? 0;
        
        return $count;
    }
}
