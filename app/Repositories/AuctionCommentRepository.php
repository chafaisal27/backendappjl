<?php

namespace App\Repositories;

use App\Models\AuctionComment;

class AuctionCommentRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AuctionComment $AuctionComment) {
        $this->AuctionComment = $AuctionComment;
    }

    public function create($data) {
        return $this->AuctionComment::create($data);
    }

    public function update($id,$data) {
        return $this->AuctionComment::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AuctionComment::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AuctionComment::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->AuctionComment::orderBy("created_at", "desc")->get();
    }

}
