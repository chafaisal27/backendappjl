<?php

namespace App\Repositories;

use App\Models\AuctionEventParticipant;

class AuctionEventParticipantRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AuctionEventParticipant $AuctionEventParticipant) {
        $this->AuctionEventParticipant = $AuctionEventParticipant;
    }

    public function create($data) {
        return $this->AuctionEventParticipant::create($data);
    }

    public function update($id,$data) {
        return $this->AuctionEventParticipant::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AuctionEventParticipant::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AuctionEventParticipant::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->AuctionEventParticipant::orderBy("created_at", "desc")->get();
    }

}
