<?php

namespace App\Repositories;

use App\Models\ProductComment;

class ProductCommentRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(ProductComment $ProductComment) {
        $this->ProductComment = $ProductComment;
    }

    public function create($data) {
        return $this->ProductComment::create($data);
    }

    public function update($id,$data) {
        return $this->ProductComment::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->ProductComment::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->ProductComment::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->ProductComment::orderBy("created_at", "desc")->get();
    }

}
