<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Role $Role) {
        $this->Role = $Role;
    }

    public function create($data) {
        return $this->Role::create($data);
    }

    public function update($id,$data) {
        return $this->Role::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Role::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Role::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Role::orderBy("created_at", "desc")->get();
    }

}
