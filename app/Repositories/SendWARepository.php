<?php

namespace App\Repositories;

use App\Models\SendWA;

class SendWARepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(SendWA $SendWA) {
        $this->SendWA = $SendWA;
    }

    public function create($data) {
        return $this->SendWA::create($data);
    }

    public function update($id,$data) {
        return $this->SendWA::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->SendWA::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->SendWA::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->SendWA::orderBy("created_at", "desc")->get();
    }

}
