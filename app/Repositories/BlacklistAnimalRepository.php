<?php

namespace App\Repositories;

use App\Models\BlacklistAnimal;

class BlacklistAnimalRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(BlacklistAnimal $BlacklistAnimal) {
        $this->BlacklistAnimal = $BlacklistAnimal;
    }

    public function create($data) {
        return $this->BlacklistAnimal::create($data);
    }

    public function update($id,$data) {
        return $this->BlacklistAnimal::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->BlacklistAnimal::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->BlacklistAnimal::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->BlacklistAnimal::orderBy("created_at", "desc")->get();
    }

}
