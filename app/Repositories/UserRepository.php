<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(User $User)
    {
        $this->User = $User;
    }

    public function login($data)
    {
        $found = false;
        $user = $this->User::where('username', $data['username'])
            ->first();

        if ($user != null) {
            $found = true;
        } else {
            $user = $this->User::where('phone_number', $data['username'])
                ->first();
            if ($user != null) {
                $found = true;
            }
        }

        if (!$found) {
            return null;
        }

        if (Hash::check($data['password'], $user->password)) {
            return $user;
        }

        return null;
    }

    public function register($data)
    {
        $newUser = new User();
        $newUser->name = $data->name;
        $newUser->photo = $data->photo;
        $newUser->gender = $data->gender;
        $newUser->username = $data->username;
        $newUser->password = Hash::make($data->password);
        $newUser->regency_id = $data->regency_id;
        $newUser->phone_number = $data->phone_number;
        $newUser->email = $data->email;
        $newUser->role_id = 2;
        $newUser->facebook_user_id = $data->facebook_user_id;

        $success = $newUser->save();

        // $success = $this->User::create($newUser);

        return $newUser;
    }

    public function create($data)
    {
        return $this->User::create($data);
    }

    public function update($id, $data)
    {
        if (isset($data['firebase_token'])) {
            $this->User::where(self::PRIMARY_KEY, '<>', $id)->whereFirebaseToken($data['firebase_token'])->update([
                'firebase_token' => null,
            ]);
        }

        return $this->User::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->User::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->User::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getBeforeMassUpdate($id)
    {
        return $this->User::where(self::PRIMARY_KEY, $id)->where('verified_at', '<=', '2019-10-07 11:30:00')->first();
    }

    public function getFirstByRoleAdmin()
    {
        return $this->User::where("role_id", 1)->first();
    }

    public function getAllByRoleAdmin()
    {
        return $this->User::where("role_id", 1)->get();
    }

    public function getByPhoneNumber($phoneNumber)
    {
        return $this->User::where("phone_number", $phoneNumber)->first();
    }

    public function getWhere($phoneNumber)
    {
        return $this->User::where("phone_number", $phoneNumber)->first();
    }

    public function getAll()
    {
        return $this->User::orderBy("created_at", "desc")->get();
    }

    public function getAllWhere($field, $data)
    {
        return $this->User::where($field, $data)->get();
    }

    public function getAllBlacklistedUser()
    {
        return $this->User::withCount('reports')->whereBlacklisted(1)->get();
    }

    public function getTopSellers($animalSubCategoryId)
    {
        return $this->User::whereHas('top_sellers', function ($query) use ($animalSubCategoryId) {
            $query->whereAnimalSubCategoryId($animalSubCategoryId);
        })->get();
    }

    public function countAll()
    {
        return $this->User::whereRoleId(2)->count();
    }

    public function getUserByFacebookUserId($facebookUserId)
    {
        return $this->User::where("facebook_user_id", $facebookUserId)->first();
    }

    public function getUserByEmail($email)
    {
        return $this->User::where("email", $email)->first();
    }

    public function getCoupon($userId)
    {
        return $this->User::where(self::PRIMARY_KEY, $userId)->first()->coupon;
    }

    public function getPoint($userId)
    {
        return $this->User::where(self::PRIMARY_KEY, $userId)->first()->point;
    }
}
