<?php

namespace App\Repositories;

use App\Models\Promo;

class PromoRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(Promo $Promo)
    {
        $this->Promo = $Promo;
    }

    public function create($data)
    {
        return $this->Promo::create($data);
    }

    public function update($id, $data)
    {
        return $this->Promo::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->Promo::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->Promo::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAll($type, $location)
    {
        return $this->Promo::where("type", $type)
            ->where("location", $location)
            ->orderBy("order", "asc")
            ->get();
    }

    public function countPromos($type, $location)
    {
        return $this->Promo::where("type", $type)
            ->where("location", $location)
            ->orderBy("order", "asc")
            ->count();
    }
}
