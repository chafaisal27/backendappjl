<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Lib\NicepayDirect\NicepayLib;

class TransactionRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Transaction $Transaction) {
        $this->Transaction = $Transaction;
    }
    
     function generateReference()
    {
         $today = date('YmdHi');
    $startDate = date('YmdHi', strtotime('-10 days'));
    $range = $today - $startDate;
    $rand1 = rand(0, $range);
    $rand2 = rand(0, 600000);
    return  $value=($rand1+$rand2);
    }

    public function create($data) {
     
     /*
       if (isset($request['resultCd']) && $request['resultCd'] == "0000")
            {
             
                $datato = json_decode($request);
                //$csID = $request->customerID;
                
                $balanceUser = new Transaction();
                $balanceUser->animal_id = $data['animal_id'];
                $balanceUser->seller_user_id = $data['seller_user_id'];
                $balanceUser->buyer_user_id = $data['buyer_user_id'];
                $balanceUser->admin_user_id = $data['admin_user_id'];
                $balanceUser->type = $data['type'];
                $balanceUser->price = $data['price'];
                $balanceUser->invoice_number = $data['invoice_number'];
                $balanceUser->seller_bank_name = $data['seller_bank_name'];
                $balanceUser->seller_bank_account_number = $datato;
                $balanceUser->save();
               
              
                
               
                
               return $balanceUser;
               
            }
            elseif(isset($response->data->resultCd))
            {
               
                 $reslt = "Oops! Something happened, please notice your system administrator.\n\n";
                return $reslt;
            }
            else
            {
              
                
                $reslt = "<pre>Connection Timeout. Please Try again.</pre>";
                 return $reslt;
            }
            */
                 
   return $this->Transaction::create($data);
             
        
    }

    public function update($id,$data) {
        return $this->Transaction::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Transaction::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Transaction::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Transaction::orderBy("created_at", "desc")->get();
    }

}
