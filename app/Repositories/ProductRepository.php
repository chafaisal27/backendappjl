<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Product $Product) {
        $this->Product = $Product;
    }

    public function create($data) {
        return $this->Product::create($data);
    }

    public function update($id,$data) {
        return $this->Product::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Product::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Product::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Product::orderBy("created_at", "desc")->get();
    }

}
