<?php

namespace App\Repositories;

use App\Models\LoginLog;

class LoginLogRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(LoginLog $LoginLog) {
        $this->LoginLog = $LoginLog;
    }

    public function create($data) {
        return $this->LoginLog::create($data);
    }

    public function update($id,$data) {
        return $this->LoginLog::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->LoginLog::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->LoginLog::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->LoginLog::orderBy("created_at", "desc")->get();
    }

}
