<?php

namespace App\Repositories;

use App\Models\Slider;

class SliderRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Slider $Slider) {
        $this->Slider = $Slider;
    }

    public function create($data) {
        return $this->Slider::create($data);
    }

    public function update($id,$data) {
        return $this->Slider::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Slider::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Slider::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Slider::orderBy("created_at", "desc")->get();
    }

}
