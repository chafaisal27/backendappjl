<?php

namespace App\Repositories;

use App\Models\TopSellerPoin;
use DB;

class TopSellerPoinRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(TopSellerPoin $TopSellerPoin)
    {
        $this->TopSellerPoin = $TopSellerPoin;
    }

    public function create($data)
    {
        return $this->TopSellerPoin::create($data);
    }

    public function update($id, $data)
    {
        return $this->TopSellerPoin::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->TopSellerPoin::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->TopSellerPoin::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAll()
    {
        return $this->TopSellerPoin::orderBy("created_at", "desc")->get();
    }

    public function getByCategory($categoryId)
    {
        return $this->TopSellerPoin
            ->select(DB::raw("SUM(point) as point, owner_user_id"))
            ->groupBy(DB::raw("owner_user_id"))
            ->where("animal_category_id", $categoryId)
            ->with("user")
            ->take(10)
            ->orderBy("point", "desc")
            ->get();
    }

    public function getBySubCategory($subCategoryId)
    {
        return

            $this->TopSellerPoin
            ->select(DB::raw("SUM(point) as point, owner_user_id"))
            ->groupBy(DB::raw("owner_user_id"))
            ->where("animal_sub_category_id", $subCategoryId)
            ->with("user")
            ->take(10)
            ->orderBy("point", "desc")
            ->get();
    }
}
