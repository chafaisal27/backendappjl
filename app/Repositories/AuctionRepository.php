<?php

namespace App\Repositories;

use App\Models\Auction;
use DB;

class AuctionRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(Auction $Auction)
    {
        $this->Auction = $Auction;
    }

    public function create($data)
    {
        return $this->Auction::create($data);
    }

    public function update($id, $data)
    {
        return $this->Auction::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->Auction::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->Auction::where(self::PRIMARY_KEY, $id)->with("bids")->first();
    }

    public function getAll()
    {
        return $this->Auction::orderBy("created_at", "desc")->get();
    }

    public function getPopularAuctions()
    {
        return $this->Auction::with('animal', 'bids')->has('bids', '>', 0)->withCount('bids')->where('active', 1)->orderBy('bids_count', 'DESC')->take(20)->get();
        // return $this->Auction::where('active', 1)->has('bids', '>', 20)->get();
    }

    public function getAllWithActiveChat($userId, $request) {
        $auctions = $this->Auction::with(['chat', 'owner', 'winner_bid', 'animal'])->whereNotNull('firebase_chat_id')->where(function($query) use ($userId) {
            $query->whereHas('owner', function($query) use ($userId) {
                $query->where('id', $userId);
            })->orWhereHas('winner_bid', function($query) use ($userId) {
                $query->where('user_id', $userId);
            });
        })->leftJoin(DB::raw('(SELECT firebase_chat_id AS "firebase_chat_id_relation", seller_unread_count, seller_user_id, buyer_unread_count, buyer_user_id, admin_unread_count, admin_user_id, updated_at FROM auction_chats) auction_chats'), 'auctions.firebase_chat_id', '=', 'auction_chats.firebase_chat_id_relation');

        if (isset($request->search) && strlen($request->search) > 0) {
            $auctions = $auctions->where('id', 'LIKE', '%' . $request->search . '%')->orWhere('verification_code', 'LIKE', '%' . $request->search . '%');
        }

        return $auctions->orderBy("auction_chats.updated_at", "desc")->orderBy("winner_accepted_date","desc")->paginate(5); 
    }

    public function getAllWithActiveChatAdmin($request) {        
        $auctions = $this->Auction::with(['chat', 'owner', 'winner_bid', 'animal'])->whereNotNull('auctions.firebase_chat_id')
        ->leftJoin(DB::raw('(SELECT firebase_chat_id AS "firebase_chat_id_relation", seller_unread_count, seller_user_id, buyer_unread_count, buyer_user_id, admin_unread_count, admin_user_id, updated_at FROM auction_chats) auction_chats'), 'auctions.firebase_chat_id', '=', 'auction_chats.firebase_chat_id_relation');
        
        if (isset($request->search) && strlen($request->search) > 0) {
            $auctions = $auctions->where('id', 'LIKE', '%' . $request->search . '%')->orWhere('verification_code', 'LIKE', '%' . $request->search . '%');
        }

        return $auctions->orderBy("auction_chats.updated_at", "desc")->orderBy("winner_accepted_date","desc")->paginate(5);
    }

    public function getAllByUserID($userId)
    {
        return $this->Auction::whereOwnerUserId($userId)->orderBy("created_at", "desc")->get();
    }

    public function getExpiredAuctions()
    {
        return $this->Auction::where('expiry_date', '<=', \Carbon\Carbon::now())->whereActive(1)->get();
    }


    public function getAuctionWinnerNotConfirmed()
    {
        // $data = DB::table('auctions');
        $data = $this->Auction::whereNotNull('winner_accepted_date')
            ->whereActive(0)
            ->whereNotNull('winner_bid_id')
            ->whereNull('deleted_at')
            ->get();
        return $data;
    }

    public function getAllWithActiveChatNotPaginate($userId)
    {
        $auctions = $this->Auction::with(['owner', 'winner_bid', 'animal'])->whereNotNull('firebase_chat_id')->where(function ($query) use ($userId) {
            $query->orWhereHas('winner_bid', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            });
        });

        return $auctions->orderBy("winner_accepted_date", "desc")->get();
    }

    public function getAuctionEventParticipants() 
    {
        $auctions = $this->Auction::with(['animal', 'auction_event_participant.auction_event'])->whereHas('auction_event_participant.auction_event', function ($query) {
            $query->where('active', 1);
            $query->where('end_date', '>=', date("Y-m-d"))->orWhereNull('end_date');
        })->where('active', 0)->get();

        return $auctions;
    }
}
