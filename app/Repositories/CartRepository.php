<?php

namespace App\Repositories;

use App\Models\Cart;

class CartRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Cart $Cart) {
        $this->Cart = $Cart;
    }

    public function create($data) {
        return $this->Cart::create($data);
    }

    public function update($id,$data) {
        return $this->Cart::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Cart::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Cart::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Cart::orderBy("created_at", "desc")->get();
    }

}
