<?php

namespace App\Repositories;

use App\Models\UserBlacklist;

class UserBlacklistRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(UserBlacklist $UserBlacklist) {
        $this->UserBlacklist = $UserBlacklist;
    }

    public function create($data) {
        return $this->UserBlacklist::create($data);
    }

    public function update($id,$data) {
        return $this->UserBlacklist::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->UserBlacklist::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->UserBlacklist::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->UserBlacklist::orderBy("created_at", "desc")->get();
    }

}
