<?php

namespace App\Repositories;

use App\Models\RollingJobAdmin;

class RollingJobAdminRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(RollingJobAdmin $RollingJobAdmin)
    {
        $this->RollingJobAdmin = $RollingJobAdmin;
    }

    public function create($data)
    {
        return $this->RollingJobAdmin::create($data);
    }

    public function update($id, $data)
    {
        return $this->RollingJobAdmin::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->RollingJobAdmin::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->RollingJobAdmin::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAll()
    {
        return $this->RollingJobAdmin::orderBy("created_at", "desc")->get();
    }

    public function getLastBatch()
    {
        return $this->RollingJobAdmin::latest('id')->first();
    }

    public function getByBatch($batchNumber)
    {
        return $this->RollingJobAdmin::where("batch_number", $batchNumber)->get();
    }

}
