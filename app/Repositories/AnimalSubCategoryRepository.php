<?php

namespace App\Repositories;

use App\Models\AnimalSubCategory;

class AnimalSubCategoryRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AnimalSubCategory $AnimalSubCategory) {
        $this->AnimalSubCategory = $AnimalSubCategory;
    }

    public function create($data) {
        return $this->AnimalSubCategory::create($data);
    }

    public function update($id,$data) {
        return $this->AnimalSubCategory::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AnimalSubCategory::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AnimalSubCategory::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->AnimalSubCategory::orderBy("created_at", "desc")->get();
    }

    public function getByAnimalCategoryId($animalCategoryId) {
        return $this->AnimalSubCategory::where('animal_category_id', $animalCategoryId)->get();
    }

    public function getAllByCategoryId($animalCategoryId) {
        $animal_sub_categories = $this->AnimalSubCategory::select('id')->where('animal_category_id', $animalCategoryId)->get();

        $id = array();

        foreach ($animal_sub_categories as $key => $value) {
            $id[] = $value->id;
        }

        return $id;
    }
}
