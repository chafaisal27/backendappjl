<?php

namespace App\Repositories;

use App\Models\PointHistory;

class PointHistoryRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(PointHistory $PointHistory) {
        $this->PointHistory = $PointHistory;
    }

    public function create($data) {
        return $this->PointHistory::create($data);
    }

    public function update($id,$data) {
        return $this->PointHistory::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->PointHistory::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->PointHistory::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAllByUserID($userId) {
        return $this->PointHistory::whereUserId($userId)->orderBy("created_at", "desc")->get();
    }

}
