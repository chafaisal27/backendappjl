<?php

namespace App\Repositories;

use App\Models\Regency;

class RegencyRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Regency $Regency) {
        $this->Regency = $Regency;
    }

    public function create($data) {
        return $this->Regency::create($data);
    }

    public function update($id,$data) {
        return $this->Regency::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Regency::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Regency::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Regency::orderBy("created_at", "desc")->get();
    }

    public function getByProvinceId($id) {
        return $this->Regency::where('province_id', $id)->get();
    }

}
