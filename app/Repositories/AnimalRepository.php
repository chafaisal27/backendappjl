<?php

namespace App\Repositories;

use App\Models\Animal;
use App\Models\AnimalSubCategory;

class AnimalRepository
{
    const PRIMARY_KEY = 'id';
    public function __construct(Animal $Animal)
    {
        $this->Animal = $Animal;
    }
    public function create($data)
    {
        return $this->Animal::create($data);
    }
    public function update($id, $data)
    {
        return $this->Animal::where(self::PRIMARY_KEY, $id)->update($data);
    }
    public function delete($id)
    {
        return $this->Animal::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->Animal::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAuction($id)
    {
        return $this->Animal::where(self::PRIMARY_KEY, $id)
            ->with(['auction.bids' => function ($q) {
                $q->orderBy("id", "desc");
            }])
            ->with(['auction.auction_comments' => function ($q) {
                $q->orderBy("id", "desc");
            }])
            ->with(['product.product_comments' => function ($q) {
                $q->orderBy("id", "desc");
            }])

            ->with(['product.product_comments.user' => function ($q) {
                $q->orderBy("id", "desc");
            }])
            ->first();
    }

    public function getProduct($id)
    {
        return $this->Animal::where(self::PRIMARY_KEY, $id)
            ->with(['product.product_comments' => function ($q) {
                $q->orderBy("id", "desc");
            }])
            ->first();
    }

    public function getAllByCategory($animal_category_id, $filter_animal_name, $sort_by, $perPage)
    {
        $data = $this->Animal::whereHas('animal_sub_category', function ($q) use ($animal_category_id) {
            $q->where('animal_category_id', $animal_category_id);
        })
            ->whereHas('auction', function ($q) {
                $q->where('active', 1);
            });

        if ($filter_animal_name) {
            $data = $data
                // animal name
                ->where('name', 'like', '%' . $filter_animal_name . '%')
                ->orWhereHas('owner', function ($e) use ($filter_animal_name) {
                    // owner name
                    $e->where('name', 'like', '%' . $filter_animal_name . '%')
                        // regency name
                        ->orWhereHas('regency', function ($w) use ($filter_animal_name) {
                            $w->where('name', 'like', '%' . $filter_animal_name . '%');
                        });
                })
                ->whereHas('auction', function ($r) {
                    $r->where('active', 1);
                });
        }

        if ($sort_by == "Populer") {
            $data = $data->orderBy("updated_at", "desc");
        }
        if ($sort_by == "Expiry_Date") {
            $data = $data->whereHas('auction', function ($r) {
                $r->orderBy('expiry_date', 'asc');
            });
        } else {
            $data = $data->orderBy("created_at", "desc");
        }

        $data = $data->paginate($perPage);
        return $data;
    }

    public function getAllUnauctionedByUserID($userId)
    {
        return $this->Animal::whereDoesntHave('auction')->whereOwnerUserId($userId)->orderBy("created_at", "desc")->get();
    }

    public function getDraftAnimal($userId, $filter_animal_name)
    {
        $data = $this->Animal::whereDoesntHave('auction')->whereDoesntHave('product')->whereOwnerUserId($userId)->orderBy("created_at", "desc");

        if ($filter_animal_name) {
            $data = $data->where('name', 'like', '%' . $filter_animal_name . '%');
        }

        return $data = $data->get();
    }

    public function getAllBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by, $perPage)
    {
        $data = $this->Animal::whereHas('animal_sub_category', function ($q) use ($animal_sub_category_id) {
            $q->where('id', $animal_sub_category_id);
        })
            ->whereHas('auction', function ($q) {
                $q->where('active', 1);
            });

        if ($filter_animal_name) {
            $data = $data
                // animal name
                ->where('name', 'like', '%' . $filter_animal_name . '%')
                ->orWhereHas('owner', function ($e) use ($filter_animal_name) {
                    // owner name
                    $e->where('name', 'like', '%' . $filter_animal_name . '%')
                        // regency name
                        ->orWhereHas('regency', function ($w) use ($filter_animal_name) {
                            $w->where('name', 'like', '%' . $filter_animal_name . '%');
                        });
                })
                ->whereHas('auction', function ($r) {
                    $r->where('active', 1);
                });
        }

        if ($sort_by == "Populer") {
            $data = $data->orderBy("updated_at", "desc");
        } else {
            $data = $data->orderBy("created_at", "desc");
        }

        $data = $data->paginate($perPage);
        return $data;
    }

    public function getAllByUserID($userId)
    {
        return $this->Animal::whereOwnerUserId($userId)->orderBy("created_at", "desc")->get();
    }

    public function getUserAuctions($userId, $filter_animal_name)
    {
        $data = $this->Animal::orderBy("created_at", "desc")
            ->whereHas('auction', function ($q) use ($userId) {
                $q->where('owner_user_id', $userId);
            });

        if ($filter_animal_name) {
            $data = $data->where('name', 'like', '%' . $filter_animal_name . '%');
        }

        // if ($sort_by == "Populer") {
        //     $data = $data->withCount('animals_comments');
        //     $data = $data->orderBy("animals_comments_count", "desc");
        // }

        $data = $data->get();
        return $data;
    }

    public function getUserProduct($userId, $filter_animal_name)
    {
        $data = $this->Animal::orderBy("created_at", "desc")
            ->whereHas('product', function ($q) use ($userId) {
                $q->where('owner_user_id', $userId);
            })
            ->with('product');

        if ($filter_animal_name) {
            $data = $data->where('name', 'like', '%' . $filter_animal_name . '%');
        }

        // if ($sort_by == "Populer") {
        //     $data = $data->withCount('animals_comments');
        //     $data = $data->orderBy("animals_comments_count", "desc");
        // }

        $data = $data->get();
        return $data;
    }

    public function getUserBids($userId, $sort_by)
    {
        $data = $this->Animal::whereHas('auction', function ($auction) use ($sort_by) {
            if ($sort_by == "Selesai" || $sort_by == "Dimenangkan" || $sort_by == "Kalah") {
                $auction->where("active", 0);
            }
        })
            ->with("auction.winner_bid")

            ->whereHas('auction.bids', function ($q) use ($userId) {
                $q->where('user_id', $userId);
            });
        if ($sort_by == "Dimenangkan" || $sort_by == "Kalah") {
            $data = $data->whereHas("auction.winner_bid", function ($winner_bid) use ($userId, $sort_by) {
                if ($sort_by == "Dimenangkan") {
                    $winner_bid->where("user_id", $userId);
                }
                if ($sort_by == "Kalah") {
                    $winner_bid->where("user_id", "<>", $userId);
                }
            });
        }

        $data = $data->orderBy("created_at", "desc");
        $data = $data->get();

        return $data;
    }

    public function getUserCommentsAuction($userId, $sort_by)
    {
        $data = $this->Animal::whereHas('auction', function ($auction) use ($sort_by) {
            if ($sort_by == "Selesai" || $sort_by == "Dimenangkan" || $sort_by == "Kalah") {
                $auction->where("active", 0);
            }
        })
            ->with(['auction.auction_comments' => function ($q) use ($userId) {
                $q->orderBy("id", "desc");
                $q->where('user_id', $userId);
                $q->take(1);
            }])
            ->with(['animal_images' => function ($q) use ($userId) {
                $q->take(1);
            }])
            ->whereHas('auction.auction_comments', function ($q) use ($userId) {
                $q->where('user_id', $userId);
            });

        $data = $data->orderBy("created_at", "desc");

        $data = $data->get();
        return $data;
    }

    public function getUserCommentsProduct($userId, $sort_by)
    {
        $data = $this->Animal::whereHas('product', function ($auction) use ($sort_by) { })
            ->with(['animal_images' => function ($q) use ($userId) {
                $q->take(1);
            }])
            ->with('product')
            ->whereHas('product.product_comments', function ($q) use ($userId) {
                $q->where('user_id', $userId);
            });

        if ($sort_by == "Populer") {
            $data = $data->orderBy("updated_at", "desc");
        } else {
            $data = $data->orderBy("created_at", "desc");
        }

        $data = $data->get();
        return $data;
    }

    public function getAllProductByCategory($animal_category_id, $filter_animal_name, $sort_by, $perPage)
    {
        $data = $this->Animal::leftJoin('products', 'animals.id', '=', 'products.animal_id')
            ->selectRaw("animals.*, products.price")
            ->whereHas('animal_sub_category', function ($q) use ($animal_category_id) {
                $q->where('animal_category_id', $animal_category_id);
            })->whereHas('product', function ($q) {
                $q->where('status', "active");
            })->with('product');

        // $data = $this->Animal::selectRaw("animals.*, product.price")
        //     whereHas('animal_sub_category', function ($q) use ($animal_category_id) {
        //     $q->where('animal_category_id', $animal_category_id);
        // })->whereHas('product', function ($q) {
        //     $q->where('status', "active");
        // })->with('product');

        if ($filter_animal_name) {
            $data = $data
                // animal name
                ->where('name', 'like', '%' . $filter_animal_name . '%')
                ->orWhereHas('owner', function ($e) use ($filter_animal_name) {
                    // owner name
                    $e->where('name', 'like', '%' . $filter_animal_name . '%')
                        // regency name
                        ->orWhereHas('regency', function ($w) use ($filter_animal_name) {
                            $w->where('name', 'like', '%' . $filter_animal_name . '%');
                        });
                })
                ->whereHas('product', function ($r) {
                    $r->where('status', "active");
                });
        }

        if ($sort_by == "Populer") {
            $data = $data->orderBy("updated_at", "desc");
        } else if ($sort_by == "Termurah") {
            $data = $data->orderBy("products.price", "asc");
        } else {
            $data = $data->orderBy("created_at", "desc");
        }

        $data = $data->paginate($perPage);

        return $data;
    }

    public function getAllProductBySubCategory($animal_sub_category_id, $filter_animal_name, $sort_by, $perPage)
    {

        $data = $this->Animal::leftJoin('products', 'animals.id', '=', 'products.animal_id')
            ->selectRaw("animals.*, products.price")
            ->whereHas('animal_sub_category', function ($q) use ($animal_sub_category_id) {
                $q->where('id', $animal_sub_category_id);
            })->whereHas('product', function ($q) {
                $q->where('status', "active");
            })->with('product');

        // $data = $this->Animal::
        //     whereHas('animal_sub_category', function ($q) use ($animal_sub_category_id) {
        //     $q->where('id', $animal_sub_category_id);
        // })->whereHas('product', function ($q) {
        //         $q->where('status', "active");
        //     })->with('product');

        if ($filter_animal_name) {
            $data = $data
                // animal name
                ->where('name', 'like', '%' . $filter_animal_name . '%')
                ->orWhereHas('owner', function ($e) use ($filter_animal_name) {
                    // owner name
                    $e->where('name', 'like', '%' . $filter_animal_name . '%')
                        // regency name
                        ->orWhereHas('regency', function ($w) use ($filter_animal_name) {
                            $w->where('name', 'like', '%' . $filter_animal_name . '%');
                        });
                })
                ->whereHas('animal_sub_category', function ($q) use ($animal_sub_category_id) {
                    $q->where('id', $animal_sub_category_id);
                })
                ->whereHas('product', function ($r) {
                    $r->where('status', "active");
                });
                // dd(count($data->get()));
        }

        if ($sort_by == "Populer") {
            $data = $data->orderBy("updated_at", "desc");
        } else if ($sort_by == "Termurah") {
            $data = $data->orderBy("products.price", "asc");
        } else {
            $data = $data->orderBy("created_at", "desc");
        }

        $data = $data->paginate($perPage);
        return $data;
    }

    public function countAll()
    {
        return $this->Animal::select("id")->count();
    }

    public function getSponsoredAnimals()
    {
        return $this->Animal::where('is_sponsored', 1)
            ->whereNull('deleted_at')
            ->get();
    }

    public function getAllUserIdByCategory($categoryId)
    {
        $subCategory = AnimalSubCategory::select("id")->where("animal_category_id", $categoryId)->get();
        return $this->Animal::select("owner_user_id")->distinct()->whereIn("animal_sub_category_id", $subCategory)->get();
    }

    public function getAllUserIdBySubCategory($subCategoryId)
    {
        return $this->Animal::select("owner_user_id")->distinct()->where("animal_sub_category_id", $subCategoryId)->get();
    }
}
