<?php

namespace App\Repositories;

use App\Models\PromoCategory;

class PromoCategoryRepository
{

    const PRIMARY_KEY = 'id';

    public function __construct(PromoCategory $PromoCategory)
    {
        $this->PromoCategory = $PromoCategory;
    }

    public function create($data)
    {
        return $this->PromoCategory::create($data);
    }

    public function update($id, $data)
    {
        return $this->PromoCategory::where(self::PRIMARY_KEY, $id)->update($data);
    }

    public function delete($id)
    {
        return $this->PromoCategory::where(self::PRIMARY_KEY, $id)->delete();
    }

    public function get($id)
    {
        return $this->PromoCategory::where(self::PRIMARY_KEY, $id)->first();
    }

    public function getAllSubCategory($animalSubCategoryId)
    {
        return $this->PromoCategory::where("animal_sub_category_id", $animalSubCategoryId)->orderBy("created_at", "desc")->get();
    }

    public function getAllCategory($animalCategoryId)
    {
        return $this->PromoCategory::whereHas("animal_sub_category", function ($r) use ($animalCategoryId) {
            $r->where('animal_category_id', $animalCategoryId);
        })->orderBy("created_at", "desc")->inRandomOrder()->get();
    }
}
