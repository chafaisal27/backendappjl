<?php

namespace App\Repositories;

use App\Models\AuctionEvent;

class AuctionEventRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(AuctionEvent $AuctionEvent) {
        $this->AuctionEvent = $AuctionEvent;
    }

    public function create($data) {
        return $this->AuctionEvent::create($data);
    }

    public function update($id,$data) {
        return $this->AuctionEvent::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->AuctionEvent::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->AuctionEvent::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->AuctionEvent::orderBy("created_at", "desc")->get();
    }

}
