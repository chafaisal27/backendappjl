<?php

namespace App\Repositories;

use App\Models\Bid;

class BidRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(Bid $Bid) {
        $this->Bid = $Bid;
    }

    public function create($data) {
        return $this->Bid::create($data);
    }

    public function update($id,$data) {
        return $this->Bid::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->Bid::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->Bid::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->Bid::orderBy("created_at", "desc")->get();
    }

    public function countBidByUserID($userId)
    {
        return $this->Bid::whereUserId($userId)->distinct('auction_id')->count('auction_id');
    }

}
