<?php

namespace App\Repositories;

use App\Models\UserRating;

class UserRatingRepository
{

    CONST PRIMARY_KEY = 'id';

    public function __construct(UserRating $UserRating) {
        $this->UserRating = $UserRating;
    }

    public function create($data) {
        return $this->UserRating::create($data);
    }

    public function update($id,$data) {
        return $this->UserRating::where(self::PRIMARY_KEY,$id)->update($data);
    }

    public function delete($id) {
        return $this->UserRating::where(self::PRIMARY_KEY,$id)->delete();
    }

    public function get($id) {
        return $this->UserRating::where(self::PRIMARY_KEY,$id)->first();
    }

    public function getAll() {
        return $this->UserRating::orderBy("created_at", "desc")->get();
    }

}
