<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
</head>

<body>
    <form action="{{ url('video') }}" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="exampleInputVideo">Upload Video</label>
            <input type="file" accept="video/*" name=" video" id="exampleInputVideo" />
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <script>
        // to limit video size
        var uploadField = document.getElementById("exampleInputVideo");

        uploadField.onchange = function() {
            //5 mb = 6291456 bytes
            if (this.files[0].size > 6291456) {
                alert("File is too big!");
            };
        };
    </script>

</body>

</html>