<?php

Route::get('/', function () {
    return "Selamat datang di Jual Lelang Fauna";
});

// Route::get('sendNotifications', 'Api\FirebaseController@sendNotifications');
// Route::get('testNotification', 'Api\FirebaseController@test');
// Route::get('animal-sub-categories', 'Api\AnimalSubCategoryController@getAll');
// Route::get('animal-categories/{id}/top-sellers', 'Api\UserController@getTopSellers');
// Route::put('auctions/{id}/set/winner', 'Api\AuctionController@setWinner');

// Products
// Route::get('products/{id}', 'Api\ProductController@get');

// Animal Images
// Route::post('animals/{id}/animal-images', 'Api\AnimalImageController@create');
// Route::delete('animal-images/{id}', 'Api\AnimalImageController@delete');


//####dont need token
// login + register
Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::post('forgot-password', 'Api\UserController@forgotPassword');



Route::post('users/search/email', 'Api\UserController@getByEmail');
Route::post('users/facebook-login-search', 'Api\UserController@facebookLoginSearch');
Route::get('users/{phoneNumber}/phone-number', 'Api\UserController@getByPhoneNumber');
Route::get('users/count', 'Api\UserController@countAll');
Route::get('animals/count', 'Api\AnimalController@countAll');
Route::get('type/{type}/location/{location}/promos/count', 'Api\PromoController@countPromos');


//redis session
Route::get('session', 'Api\UserController@getAllUserLoginSession');
Route::get('session/{key}', 'Api\UserController@getUserLoginSession');
Route::delete('session/{key}', 'Api\UserController@deleteUserLoginSession');

//statics data
Route::get('statics', 'Api\StaticController@getAll');
Route::get('check-version/{version}', 'Api\VersionController@checkVersion');

// Location
Route::get('provinces', 'Api\ProvinceController@getAll');
Route::get('provinces/{id}/regencies', 'Api\RegencyController@getByProvinceId');

// FILES
Route::get('download/restricted-animals', 'Api\AnimalController@downloadRestrictedAnimals');
Route::get('download/terms-policy', 'Api\StaticController@downloadTermsPolicy');
Route::get('download/sponsored_seller', 'Api\StaticController@downloadHowToBecomeSponsoredSeller');
Route::get('download/hadiah_jlf', 'Api\StaticController@downloadHadiahJlf');

//check datetime server
Route::get('get-time-now', 'Api\UserController@getTimeNow');
Route::get('get-time-now-carbon', 'Api\UserController@getTimeNowCarbon');
//####dont need token


// for forget password
Route::get('users/{email}/email', 'Api\UserController@getUserByEmail');

//Send WA OTP
Route::post('send-OTP', 'Api\SendWAController@sendOTP');



Route::group(['middleware' => ['auth.redis']], function () {
    //users
    Route::get('users/{userId}/histories', 'Api\HistoryController@getAllByUserID');
    Route::get('users/{userId}/point-histories', 'Api\PointHistoryController@getAllByUserID');
    Route::get('users/{userId}/bids/animals', 'Api\AnimalController@getUserBids');
    Route::get('users/{userId}/comments-auction/animals', 'Api\AnimalController@getUserCommentsAuction');
    Route::get('users/{userId}/comments-product/animals', 'Api\AnimalController@getUserCommentsProduct');
    Route::get('users/{userId}/animals', 'Api\AnimalController@getAllByUserID');
    Route::get('users/{userId}/histories/count', 'Api\HistoryController@countHistoryByUserID');
    Route::get('users/{userId}/bids/count', 'Api\BidController@countBidByUserID');
    Route::put('users/{id}/update-verification', 'Api\UserController@updateVerification');
    Route::post('users/{id}/verification-bonus-point', 'Api\UserController@verificationBonusPoint');
    Route::post('users/{userId}/verification-bonus-poin-otp', 'Api\UserController@addPoinVerifOTP');
    Route::get('users/{userId}/get/point-coupon', 'Api\UserController@getPointCoupon');

    Route::put('users/{id}/update-facebook-user-id', 'Api\UserController@updateFacebookUserID');
    
    // /users/$userId/verification-bonus-point
    Route::put('logout', 'Api\UserController@logout');

    // user profile
    Route::get('users/{id}', 'Api\UserController@get');
    Route::get('users/{userId}/auctions/animals', 'Api\AnimalController@getUserAuctions');
    Route::get('users/{userId}/products/animals', 'Api\AnimalController@getUserProduct');
    Route::get('users/{userId}/animals/draft', 'Api\AnimalController@getDraftAnimal');
    Route::put('users/{id}/update', 'Api\UserController@update');
    Route::put('users/{id}/updateProfilePicture', 'Api\UserController@updateProfilePicture');
    Route::put('users/{id}/verifyByOTP', 'Api\UserController@verifyByOTP');
    Route::get('verify-token', 'Api\UserController@verifyToken');

    
    

    // user  notification
    Route::delete('history/{id}', 'Api\HistoryController@delete');
    Route::put('histories/mark/{historyId}', 'Api\HistoryController@markAsRead');

    //create update delete animal
    Route::get('animals/{id}', 'Api\AnimalController@getAuction');
    Route::put('animals/{id}', 'Api\AnimalController@update');
    Route::post('animals-update/{id}', 'Api\AnimalController@updateAnimal');
    Route::post('animals', 'Api\AnimalController@create');
    Route::delete('animals/{id}', 'Api\AnimalController@delete');
    Route::get('is-sponsored-animals', 'Api\AnimalController@getSponsoredAnimals');

    //animal categories count
    Route::get('animal-categories/{id}', 'Api\AnimalCategoryController@get');
    Route::get('animal/animal-categories', 'Api\AnimalCategoryController@getAllAnimals');
    Route::get('product/animal-categories', 'Api\AnimalCategoryController@getAllProductAnimals');
    Route::get('accessory/animal-categories', 'Api\AnimalCategoryController@getAllProductAccessoryAnimals');
    Route::get('feed/animal-categories', 'Api\AnimalCategoryController@getAllProductFeedsAnimals');

    //animal categories data
    Route::get('animals/category/{categoryId}/auction', 'Api\AnimalController@getAllAuctionCategory');
    Route::get('animals/sub-category/{subCategoryId}/auction', 'Api\AnimalController@getAllAuctionSubCategory');
    Route::get('animals/category/{categoryId}/product', 'Api\AnimalController@getAllProductCategory');
    Route::get('animals/sub-category/{subCategoryId}/product', 'Api\AnimalController@getAllProductSubCategory');
    Route::get('animal-categories/{id}/animal-sub-categories', 'Api\AnimalSubCategoryController@getByAnimalCategoryId');
    Route::get('type/{type}/animal-categories', 'Api\AnimalCategoryController@getAllAnimalsWithoutCount');

    // Blacklisted
    Route::get('blacklists', 'Api\UserController@getAllBlacklistedUser');
    Route::get('blacklist-animals', 'Api\BlacklistAnimalController@getAll');

    // Top Sellers / Star Sellers
    Route::get('animal-sub-categories/{id}/top-sellers', 'Api\TopSellerController@getAllBySubCategory');
    Route::get('animal-categories/{id}/top-sellers', 'Api\TopSellerController@getAllByCategory');
    Route::get('is-promoted-top-sellers', 'Api\TopSellerController@getPromotedTopSeller');
    Route::get('top-seller/category/{categoryId}', 'Api\TopSellerPoinController@getByCategory');
    Route::get('top-seller/sub-category/{subCategoryId}', 'Api\TopSellerPoinController@getBySubCategory');


    // Auctions
    Route::post('animals/{id}/auctions/create', 'Api\AuctionController@create');
    Route::put('animals/{id}/auctions/update', 'Api\AnimalController@update');

    // Auction Events (Hot Auctions)
    Route::get('auctions/events', 'Api\AuctionController@getAuctionEventParticipants');
    Route::get('auctions/popular', 'Api\AuctionController@getPopularAuctions');


    Route::get('auctions/autoClose', 'Api\AuctionController@autoClose');
    Route::get('users/{id}/auctions/chats', 'Api\AuctionController@getAllWithActiveChat');
    Route::get('users/{id}/auctions/chats/no-paginate', 'Api\AuctionController@getAllWithActiveChatNotPaginate');

    Route::get('auctions/active-chats-admin', 'Api\AuctionController@getAllWithActiveChatAdmin');
    Route::put('auctions/{id}/cancel', 'Api\AuctionController@cancel');
    Route::delete('auctions/{id}', 'Api\AuctionController@delete');
    Route::get('auction-not-confirmed', 'Api\AuctionController@getAuctionWinnerNotConfirmed');

// Payment

     Route::post('balance-user/add', 'Api\BalanceUserController@deposit');
     Route::post('Notification', 'Api\BalanceUserController@DBProcessURL');
     Route::get('users/{userId}/balance-histories', 'Api\BalanceUserController@getAllByUserID');
     
    // Auction Chats Unread Messages Count
    Route::put('auction-chats/update', 'Api\AuctionChatController@update');
    Route::put('auction-chats/reset', 'Api\AuctionChatController@reset');
    Route::get('users/{id}/auction-chats/count', 'Api\AuctionChatController@count');

    // Get/Generate Transaction (Form Rekber)
    Route::get('auctions/{id}/transactions', 'Api\TransactionController@getOrGenerateAuctionTransaction');
    Route::get('transactions/{id}', 'Api\TransactionController@get');
    Route::put('transactions/{id}', 'Api\TransactionController@update');
    Route::post('transactions/buyitem', 'Api\TransactionController@buyitem');

    //set firebase chat room
    Route::put('auctions/{id}/set-chat-room', 'Api\AuctionController@setChatRoom');
    Route::get('auctions/{id}/get-chat-room', 'Api\AuctionController@getChatRoom');

    // check auction is available ? 
    Route::get('check-available/{type}', 'Api\StaticController@checkAvailable');

    Route::post('bids', 'Api\BidController@create');
    Route::delete('bids/{id}', 'Api\BidController@delete');
    Route::post('auction-comment', 'Api\AuctionCommentController@create');
    Route::post('product-comment', 'Api\ProductCommentController@create');

    //product
    Route::put('sold/products/{id}', 'Api\ProductController@sold');
    Route::put('products/{id}', 'Api\ProductController@update');
    Route::delete('products/{id}', 'Api\ProductController@delete');
    Route::post('animals/{id}/products/create', 'Api\ProductController@create');

    //home data
    Route::get('jlf-partners', 'Api\JlfPartnerController@getAll');
    Route::get('type/{type}/location/{location}/promos', 'Api\PromoController@getAll');
    Route::get('type/{type}/article', 'Api\ArticleController@getAll');

    //promo animal category
    Route::get('promo/animal-category/{animalCategoryId}', 'Api\PromoCategoryController@getAllCategory');
    Route::get('promo/animal-sub-category/{animalSubCategoryId}', 'Api\PromoCategoryController@getAllSubCategory');
    
});
