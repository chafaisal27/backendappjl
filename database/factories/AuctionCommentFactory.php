<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\AuctionComment;
use Faker\Generator as Faker;

$factory->define(AuctionComment::class, function (Faker $faker) {
    return [
        'auction_id' => $faker->numberBetween(1,30),
        'comment' => $faker->paragraph(1),
        'user_id' => $faker->numberBetween(2,31)
    ];
});
