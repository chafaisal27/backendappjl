<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Animal;
use Faker\Generator as Faker;

$factory->define(Animal::class, function (Faker $faker) {
    $animal_name = $faker->unique()->name;

    return [
        'animal_sub_category_id' => $faker->numberBetween(1,8),
        'name' => $animal_name,
        'description' => $faker->paragraph,
        'gender' => $faker->randomElement($array = array('M', 'F')),
        'date_of_birth' => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = null),
        'regency_id' => $faker->numberBetween(1, 514),
        'owner_user_id' => $faker->numberBetween(1, 30),
        'slug' => str_slug($animal_name)
    ];
});
