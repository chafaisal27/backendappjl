<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Auction;
use Faker\Generator as Faker;

$factory->define(Auction::class, function (Faker $faker) {
    return [
        'animal_id' => $faker->unique()->numberBetween(20,50),
        'open_bid' => $faker->numberBetween(10000, 100000),
        'multiply' => $faker->numberBetween(500, 1000),
        'buy_it_now' => $faker->numberBetween(140000, 150000),
        'expiry_date' => $faker->dateTimeBetween($startDate = '+10 days', $endDate = '+40 days', $timezone = null),
        'active' => 1,
        'owner_user_id' => $faker->numberBetween(2,31),
        'slug' => str_slug($faker->unique()->name)
    ];
});
