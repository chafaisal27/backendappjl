<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\History;
use Faker\Generator as Faker;

$factory->define(History::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 51),
        'information' => $faker->paragraph($nbSentences = 1),
    ];
});
