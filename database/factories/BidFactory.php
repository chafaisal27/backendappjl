<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Bid;
use Faker\Generator as Faker;

$factory->define(Bid::class, function (Faker $faker) {
    return [
        'auction_id' => $faker->numberBetween(1,30),
        'amount' => $faker->numberBetween(5000,10000),
        'user_id' => $faker->numberBetween(2,31)
    ];
});
