<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    $username = $faker->unique()->userName;
    return [
        'name' => $faker->name,
        'username' => $username,
        'password' => Hash::make($username),
        'email' => $faker->unique()->freeEmail,
        'description' => $faker->paragraph,
        'phone_number' => $faker->numerify("############"),
        'address' => $faker->address,
        'role_id' => 2,
        'regency_id' => $faker->numberBetween(1, 100),
        'photo'=> 'https://placeimg.com/500/500/people?' . $faker->numberBetween(1, 100),
        'blacklisted' => $faker->numberBetween(0, 1)
    ];
});
