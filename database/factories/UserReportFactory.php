<?php
use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\UserReport::class, function (Faker $faker) {
    return [
        'reason' => $faker->name,
        'reported_user_id' => $faker->numberBetween(2, 31),
        'user_id' => $faker->numberBetween(2, 31)
    ];
});