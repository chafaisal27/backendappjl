<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProvincesTableSeeder::class);
        $this->call(RegenciesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserRatingsTableSeeder::class);
        $this->call(AnimalCategoriesTableSeeder::class);
        $this->call(AnimalSubCategoriesTableSeeder::class);
        $this->call(AnimalsTableSeeder::class);
        $this->call(AnimalImagesTableSeeder::class);
        $this->call(AuctionsTableSeeder::class);
        $this->call(AuctionCommentsTableSeeder::class);
        $this->call(BidsTableSeeder::class);
        $this->call(UserReportsTableSeeder::class);
        $this->call(UserBlacklistsTableSeeder::class);        
        $this->call(HistoryTableSeeder::class);        
        $this->call(SlidersTableSeeder::class);
        $this->call(PromosTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductCommentsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(JlfPartnersTableSeeder::class);
        $this->call(StaticsTableSeeder::class);
    }
}
