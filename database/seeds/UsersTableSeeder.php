<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $users = [
            [
                'name' => 'Admin',
                'username' => 'admin',
                'password' => Hash::make('admin'),
                'email' => 'admin@admin.com',
                'role_id' => 1,
                'regency_id' => 217,
                'description' => "Halo semua, saya admin disini",
                'photo'=> "https://66.media.tumblr.com/d3a12893ef0dfec39cf7335008f16c7f/tumblr_pcve4yqyEO1uaogmwo8_400.png",
                'phone_number'=>"081234567891"    
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }

        factory(App\Models\User::class, 100)->create();
    }
}
