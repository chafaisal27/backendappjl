<?php

use Illuminate\Database\Seeder;
use App\Models\Bid;

class BidsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('bids')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        for ($i=1; $i <= 30; $i++) {
            $amountA = 200000;
            $amountB = 400000;
            for ($j=1; $j <= 5; $j++) {
                if($i % 2 == 0) {
                    $amountB += 40000;
                    Bid::create([
                        'auction_id' => $i,
                        'amount' => $amountB,
                        'user_id' => rand(3,50)
                    ]);
                } else {
                    $amountA += 20000;
                    Bid::create([
                        'auction_id' => $i,
                        'amount' => $amountA,
                        'user_id' => rand(3,50)
                    ]);
                }
            }
        }

    }
}
