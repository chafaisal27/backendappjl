<?php

use Illuminate\Database\Seeder;

class PromosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('promos')->delete();
        
        \DB::table('promos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'link' => 'https://placeimg.com/520/200/animals?4',
                'name' => 'qwe',
                'description' => 'A',
                'file_name' => 'qwe',
                'type' => 'iklan',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'A',
            ),
            1 => 
            array (
                'id' => 2,
                'link' => 'https://placeimg.com/520/200/animals?5',
                'name' => 'qwe',
                'description' => 'A',
                'file_name' => 'qwe',
                'type' => 'iklan',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 2,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'A',
            ),
            2 => 
            array (
                'id' => 3,
                'link' => 'https://placeimg.com/520/200/animals?6',
                'name' => 'qwe',
                'description' => 'A',
                'file_name' => 'qwe',
                'type' => 'iklan',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 3,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'A',
            ),
            3 => 
            array (
                'id' => 4,
                'link' => 'https://placeimg.com/520/200/animals?7',
                'name' => 'qwe',
                'description' => 'B',
                'file_name' => 'qwe',
                'type' => 'iklan',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 3,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'B',
            ),
            4 => 
            array (
                'id' => 6,
                'link' => 'https://placeimg.com/520/200/animals?9',
                'name' => 'qwe',
                'description' => 'C',
                'file_name' => 'qwe',
                'type' => 'iklan',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 3,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'C',
            ),
            5 => 
            array (
                'id' => 8,
                'link' => 'https://www.youtube.com/watch?v=IQgA_6jp82w',
                'name' => 'qwe',
                'description' => 'Video A',
                'file_name' => 'qwe',
                'type' => 'video',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 3,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'location' => 'A',
            ),
        ));
        
        
    }
}