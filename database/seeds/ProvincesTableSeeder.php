<?php

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Province::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::table('provinces')->insert([
            ['name' => 'Aceh', 'slug' => str_slug('Aceh')],
            ['name' => 'Sumatera Utara', 'slug' => str_slug('Sumatera Utara')],
            ['name' => 'Sumatera Barat', 'slug' => str_slug('Sumatera Barat')],
            ['name' => 'Riau', 'slug' => str_slug('Riau')],
            ['name' => 'Jambi', 'slug' => str_slug('Jambi')],
            ['name' => 'Sumatera Selatan', 'slug' => str_slug('Sumatera Selatan')],
            ['name' => 'Bengkulu', 'slug' => str_slug('Bengkulu')],
            ['name' => 'Lampung', 'slug' => str_slug('Lampung')],
            ['name' => 'Bangka Belitung', 'slug' => str_slug('Bangka Belitung')],
            ['name' => 'Kepulauan Riau', 'slug' => str_slug('Kepulauan Riau')],
            ['name' => 'DKI Jakarta', 'slug' => str_slug('DKI Jakarta')],
            ['name' => 'Jawa Barat', 'slug' => str_slug('Jawa Barat')],
            ['name' => 'Jawa Tengah', 'slug' => str_slug('Jawa Tengah')],
            ['name' => 'Yogyakarta', 'slug' => str_slug('Yogyakarta')],
            ['name' => 'Jawa Timur', 'slug' => str_slug('Jawa Timur')],
            ['name' => 'Banten', 'slug' => str_slug('Banten')],
            ['name' => 'Bali', 'slug' => str_slug('Bali')],
            ['name' => 'Nusa Tenggara Barat', 'slug' => str_slug('Nusa Tenggara Barat')],
            ['name' => 'Nusa Tenggara Timur', 'slug' => str_slug('Nusa Tenggara Timur')],
            ['name' => 'Kalimantan Barat', 'slug' => str_slug('Kalimantan Barat')],
            ['name' => 'Kalimantan Tengah', 'slug' => str_slug('Kalimantan Tengah')],
            ['name' => 'Kalimantan Selatan', 'slug' => str_slug('Kalimantan Selatan')],
            ['name' => 'Kalimantan Timur', 'slug' => str_slug('Kalimantan Timur')],
            ['name' => 'Kalimantan Utara', 'slug' => str_slug('Kalimantan Utara')],
            ['name' => 'Sulawesi Utara', 'slug' => str_slug('Sulawesi Utara')],
            ['name' => 'Sulawesi Tengah', 'slug' => str_slug('Sulawesi Tengah')],
            ['name' => 'Sulawesi Selatan', 'slug' => str_slug('Sulawesi Selatan')],
            ['name' => 'Sulawesi Tenggara', 'slug' => str_slug('Sulawesi Tenggara')],
            ['name' => 'Gorontalo', 'slug' => str_slug('Gorontalo')],
            ['name' => 'Sulawesi Barat', 'slug' => str_slug('Sulawesi Barat')],
            ['name' => 'Maluku', 'slug' => str_slug('Maluku')],
            ['name' => 'Maluku Utara', 'slug' => str_slug('Maluku Utara')],
            ['name' => 'Papua Barat', 'slug' => str_slug('Papua Barat')],
            ['name' => 'Papua', 'slug' => str_slug('Papua')],
        ]);
    }
}
