<?php

use Illuminate\Database\Seeder;

class HistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('histories')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // factory(App\Models\History::class, 50)->create();
        
    }
}
