<?php

use Illuminate\Database\Seeder;
use App\Models\AnimalCategory;

class AnimalCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // \DB::table('animal_categories')->delete();
        AnimalCategory::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $categories = [
            [
                'name' => 'Kura-Kura',
                'image' => 'https://disk.mediaindonesia.com/thumbs/1200x-/news/2018/11/26a9b88e4abd6d640063569d482e675f.jpg',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Kadal',
                'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Maby_multif_F_050222_061_kng.jpg/230px-Maby_multif_F_050222_061_kng.jpg',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Ular',
                'image' => 'https://cdn-asset.jawapos.com/wp-content/uploads/2019/01/rentetan-teror-ular-ganas-yang-jadi-sorotan-media-luar-negeri_m_222237-640x410.jpeg',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Ikan',
                'image' => 'https://cdn.idntimes.com/content-images/community/2017/07/91slcoqpunl-fd874fcefa9003f4e539ad6d233191c5_600x400.png',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Sugar Glider',
                'image' => 'https://images.bluethumb.com.au/uploads/listing/234692/lyn-cooke-sugar-glider-bluethumb-8055.jpg?w=800&auto=compress&cs=tinysrgb&q=70&s=60c62077315fa52f6ecb54d51ec4ba98',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Anjing',
                'image' => 'https://media.mercola.com/imageserver/public/2011/May/two-cute-pet-puppies05.03.jpg',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Kucing',
                'image' => 'https://ichef.bbci.co.uk/news/ws/660/amz/worldservice/live/assets/images/2015/09/11/150911101705_cat_640x360_bbc_nocredit.jpg',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Unggas',
                'image' => 'https://cdn.moneysmart.id/wp-content/uploads/2019/04/12113906/Burung-kicau-mahal-yang-sering-diperlombakan-WP-700x497.jpg',
                'is_video_allowed' => true
            ],
            [
                'name' => 'Amphibi',
                'image' => 'https://4.bp.blogspot.com/-Wtuuy-ahLmM/VwxdSs5W2NI/AAAAAAAAQOE/N7DjxZPx0E0ZJTYrok6TsIk-0ypK48PdwCLcB/s1600/contoh-hewan-amphibi.png',
                'is_video_allowed' => false
            ],
            [
                'name' => 'Tarantula',
                'image' => 'https://www.aqua.org/-/media/Images/blog/2017/Animals/blog-tarantula-species-spotlight-640x420.ashx?la=en&hash=EDB36F9EF94C5EF5FC656E2C410C606BB2EE9CCE',
                'is_video_allowed' => false
            ],
        ];
        foreach ($categories as $category) {
            AnimalCategory::create([
                'name' => $category['name'],
                'image' => $category['image'],
                'thumbnail' => NULL,
                'slug' => str_slug($category['name']),
                'type' => 'animal',
                'is_video_allowed' => $category['is_video_allowed']
            ]);
        }
    }
}
