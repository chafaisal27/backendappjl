<?php

use Illuminate\Database\Seeder;

class JlfPartnersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jlf_partners')->delete();
        
        \DB::table('jlf_partners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 2,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 3,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 4,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 5,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 6,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 7,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 8,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 9,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 10,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 11,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'a',
                'description' => 'a',
                'image' => 'https://via.placeholder.com/150/92c952',
                'thumbnail' => 'https://via.placeholder.com/150/92c952',
                'link' => 'https://via.placeholder.com/150/92c952',
                'order' => 12,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}