<?php

use Illuminate\Database\Seeder;

class AuctionCommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('auction_comments')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        
        \DB::table('auction_comments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'auction_id' => 2,
                'comment' => 'Veniam adipisci enim sed qui soluta.',
                'user_id' => 3,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'auction_id' => 27,
                'comment' => 'Quidem corrupti quaerat voluptas eos.',
                'user_id' => 20,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'auction_id' => 20,
                'comment' => 'Officia et inventore ut accusamus. Quas molestiae voluptatem rem voluptatem.',
                'user_id' => 17,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'auction_id' => 2,
                'comment' => 'Velit iusto ut doloremque perferendis dolore qui. Mollitia debitis expedita iusto vitae porro alias quasi.',
                'user_id' => 28,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'auction_id' => 9,
                'comment' => 'Quia aut soluta amet. Sit nulla sed accusantium aut.',
                'user_id' => 8,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'auction_id' => 17,
                'comment' => 'Placeat rerum nulla molestiae id doloribus quasi similique. Consectetur sit labore quidem aut quae libero.',
                'user_id' => 29,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'auction_id' => 21,
                'comment' => 'Non ea non consequuntur ut adipisci.',
                'user_id' => 21,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'auction_id' => 8,
                'comment' => 'Sed vitae est eos inventore et molestiae. Et quos error magnam ullam optio tempore qui.',
                'user_id' => 9,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'auction_id' => 23,
                'comment' => 'Velit non eos et reprehenderit. Ipsa quas odit maxime odit.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'auction_id' => 2,
                'comment' => 'Tempora velit distinctio et omnis consequatur.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'auction_id' => 12,
                'comment' => 'Sunt quia exercitationem incidunt et officia animi rem similique.',
                'user_id' => 27,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'auction_id' => 30,
                'comment' => 'Perferendis dolorum et perspiciatis quia incidunt praesentium natus.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'auction_id' => 5,
                'comment' => 'Iure quos maiores aut ratione libero et amet. Ut aut inventore laboriosam qui magnam.',
                'user_id' => 12,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'auction_id' => 17,
                'comment' => 'Eaque ea id animi.',
                'user_id' => 30,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'auction_id' => 17,
                'comment' => 'Corrupti repudiandae aliquam illum rerum perspiciatis.',
                'user_id' => 16,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'auction_id' => 14,
                'comment' => 'Incidunt quas sed sunt amet aliquam.',
                'user_id' => 7,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'auction_id' => 30,
                'comment' => 'Sint iste quod consequatur omnis id corporis assumenda. Aut non dolore similique doloremque ut omnis.',
                'user_id' => 24,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'auction_id' => 21,
                'comment' => 'Sed ea dolorem odio. Nulla ut dolore assumenda et blanditiis illo quos.',
                'user_id' => 29,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'auction_id' => 18,
                'comment' => 'Architecto blanditiis voluptate beatae et nihil in optio.',
                'user_id' => 2,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'auction_id' => 1,
                'comment' => 'Accusamus esse dolores aut aspernatur tenetur est.',
                'user_id' => 31,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'auction_id' => 16,
                'comment' => 'Aut sunt delectus quia.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'auction_id' => 2,
                'comment' => 'Facilis voluptatem voluptatem fugiat.',
                'user_id' => 6,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'auction_id' => 11,
                'comment' => 'Nemo et ea beatae quisquam accusamus quia ipsa.',
                'user_id' => 14,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'auction_id' => 12,
                'comment' => 'Fuga qui totam omnis quia. Occaecati consequatur voluptatem delectus adipisci dolores vel placeat.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'auction_id' => 1,
                'comment' => 'Illo alias autem vitae laudantium asperiores iste animi. Est reiciendis voluptatem tenetur sit reiciendis.',
                'user_id' => 25,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'auction_id' => 24,
                'comment' => 'Odit voluptatibus voluptas voluptas aspernatur.',
                'user_id' => 29,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'auction_id' => 8,
                'comment' => 'Cumque minus est nam est aspernatur dignissimos voluptatem est.',
                'user_id' => 16,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'auction_id' => 24,
                'comment' => 'Deleniti facere qui tenetur qui natus esse.',
                'user_id' => 20,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'auction_id' => 17,
                'comment' => 'Voluptatum a unde cumque est.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'auction_id' => 17,
                'comment' => 'Cumque porro qui a aut odit id a.',
                'user_id' => 7,
                'created_at' => '2019-06-12 13:53:44',
                'updated_at' => '2019-06-12 13:53:44',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'auction_id' => 9,
                'comment' => 'Incidunt enim autem necessitatibus quasi repudiandae excepturi est.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'auction_id' => 22,
                'comment' => 'Commodi est et sit quo et est accusantium.',
                'user_id' => 16,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'auction_id' => 10,
                'comment' => 'Molestiae laudantium deserunt voluptatum cupiditate.',
                'user_id' => 31,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'auction_id' => 2,
                'comment' => 'Voluptatem omnis mollitia alias recusandae porro quam.',
                'user_id' => 9,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'auction_id' => 14,
                'comment' => 'Iure velit sint atque rerum. Sunt et sit dolores a id assumenda.',
                'user_id' => 22,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'auction_id' => 28,
                'comment' => 'Tenetur laborum est molestias velit excepturi quo minima. Nemo officia sapiente commodi.',
                'user_id' => 7,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'auction_id' => 20,
                'comment' => 'Aut voluptatem dignissimos ipsum ratione. Iste asperiores dolor est quibusdam eum.',
                'user_id' => 30,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'auction_id' => 10,
                'comment' => 'Omnis sint voluptatem molestiae sapiente. Quos vel eum est id.',
                'user_id' => 25,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'auction_id' => 19,
                'comment' => 'Ipsum facere voluptatem itaque corrupti et.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'auction_id' => 24,
                'comment' => 'Voluptas aut rerum eos laudantium maxime velit omnis voluptatum.',
                'user_id' => 14,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'auction_id' => 6,
                'comment' => 'Aperiam eos odio odit. Saepe et quidem repellendus quam.',
                'user_id' => 17,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'auction_id' => 10,
                'comment' => 'Iste et et at a quia.',
                'user_id' => 19,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'auction_id' => 10,
                'comment' => 'Facilis molestiae ratione ut quae id sunt. Tempora corrupti ut officia consequatur enim.',
                'user_id' => 16,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'auction_id' => 23,
                'comment' => 'Doloremque omnis eos accusamus est accusantium nihil animi. Quasi alias inventore fuga non eum amet.',
                'user_id' => 22,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'auction_id' => 18,
                'comment' => 'Sunt dolorem consequatur molestiae et architecto.',
                'user_id' => 29,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'auction_id' => 26,
                'comment' => 'Dolorem veniam quo dolore aut pariatur. Quasi et asperiores eum tempora quidem soluta est.',
                'user_id' => 3,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'auction_id' => 11,
                'comment' => 'Corporis et dolorem saepe ut commodi.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'auction_id' => 13,
                'comment' => 'Est incidunt non et itaque.',
                'user_id' => 11,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'auction_id' => 15,
                'comment' => 'Dignissimos maiores voluptatem iusto est eligendi.',
                'user_id' => 2,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'auction_id' => 30,
                'comment' => 'Voluptas qui nostrum esse sapiente aut omnis.',
                'user_id' => 31,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'auction_id' => 16,
                'comment' => 'Autem repellendus blanditiis enim ullam.',
                'user_id' => 25,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'auction_id' => 8,
                'comment' => 'Vel maiores qui ut suscipit quos distinctio quia. Repudiandae quia amet voluptas est ut quia odit.',
                'user_id' => 29,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'auction_id' => 11,
                'comment' => 'Qui velit voluptatibus officiis consequuntur.',
                'user_id' => 21,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'auction_id' => 29,
                'comment' => 'Aut architecto est quia. Molestiae voluptate sed maiores perspiciatis.',
                'user_id' => 27,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'auction_id' => 3,
                'comment' => 'In nesciunt quia qui qui. Ipsum recusandae facere dolorem soluta aut velit quas.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'auction_id' => 14,
                'comment' => 'Minus deserunt eius numquam soluta et sit aut tempora. Est id nihil qui omnis ipsa.',
                'user_id' => 24,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'auction_id' => 16,
                'comment' => 'Omnis vitae cupiditate similique voluptate. Qui quos eum mollitia sint deserunt sunt.',
                'user_id' => 10,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'auction_id' => 9,
                'comment' => 'Aliquam ex quae eligendi nulla cumque saepe. Sit sint delectus nesciunt voluptatem soluta vel quas.',
                'user_id' => 5,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'auction_id' => 17,
                'comment' => 'Harum omnis et consequatur rerum perspiciatis sit. Hic necessitatibus ut facere similique.',
                'user_id' => 10,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'auction_id' => 24,
                'comment' => 'Qui molestiae et sed et aut molestiae veritatis.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'auction_id' => 25,
                'comment' => 'Deserunt quo perferendis et tenetur et.',
                'user_id' => 5,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'auction_id' => 5,
                'comment' => 'Omnis temporibus nostrum accusamus ut molestiae voluptatem repudiandae.',
                'user_id' => 16,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'auction_id' => 25,
                'comment' => 'Minima totam dicta dolorem.',
                'user_id' => 3,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'auction_id' => 1,
                'comment' => 'Accusamus laboriosam perferendis ut qui.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'auction_id' => 30,
                'comment' => 'Necessitatibus qui iste saepe necessitatibus dolorem distinctio iste provident. Labore ratione et autem deserunt.',
                'user_id' => 19,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'auction_id' => 20,
                'comment' => 'Inventore explicabo dolorum nostrum perferendis nisi facere.',
                'user_id' => 26,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'auction_id' => 21,
                'comment' => 'Et accusamus quam recusandae et autem. Expedita quidem beatae iusto beatae excepturi.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'auction_id' => 16,
                'comment' => 'Deserunt dignissimos maxime ut voluptas quam ut.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'auction_id' => 23,
                'comment' => 'Esse iusto in ut tempora cupiditate quasi. Dolorem dolorem non quia nobis.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'auction_id' => 23,
                'comment' => 'Unde vel tempore magnam et sequi. Quisquam dolor iure numquam fuga tempora quibusdam velit.',
                'user_id' => 7,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'auction_id' => 19,
                'comment' => 'Sed quis autem dignissimos qui molestiae.',
                'user_id' => 10,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'auction_id' => 24,
                'comment' => 'Dolorem doloremque autem molestias magnam ut iste sunt qui. Voluptatibus aliquid deserunt rerum.',
                'user_id' => 19,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'auction_id' => 17,
                'comment' => 'Eos corporis consequatur laborum aut dolores asperiores et.',
                'user_id' => 2,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'auction_id' => 8,
                'comment' => 'Rerum ducimus ut sint quis quae accusamus in corporis.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'auction_id' => 29,
                'comment' => 'Iure minima eos et. Asperiores cum blanditiis sunt doloribus autem consequatur explicabo.',
                'user_id' => 18,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'auction_id' => 17,
                'comment' => 'Modi odio voluptatem similique ipsa laborum alias.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'auction_id' => 7,
                'comment' => 'Sapiente sint necessitatibus placeat dolore. Perferendis qui laboriosam et vitae voluptatum ullam maxime.',
                'user_id' => 11,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'auction_id' => 19,
                'comment' => 'Labore repellendus incidunt ipsum ratione eum beatae.',
                'user_id' => 9,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'auction_id' => 3,
                'comment' => 'Dolorem id commodi sed corporis inventore totam.',
                'user_id' => 15,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'auction_id' => 26,
                'comment' => 'Ex rerum incidunt dolores debitis quas quidem facere.',
                'user_id' => 2,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'auction_id' => 19,
                'comment' => 'Cupiditate consequatur error nulla magni repudiandae exercitationem voluptatem. Incidunt dolorem ullam repudiandae minus mollitia.',
                'user_id' => 8,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'auction_id' => 22,
                'comment' => 'Excepturi optio hic nobis ut.',
                'user_id' => 3,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'auction_id' => 26,
                'comment' => 'Soluta mollitia sit omnis impedit quia accusantium ducimus.',
                'user_id' => 25,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'auction_id' => 30,
                'comment' => 'Reprehenderit provident dolore culpa enim.',
                'user_id' => 6,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'auction_id' => 20,
                'comment' => 'Ut dolorum iusto amet rem.',
                'user_id' => 24,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'auction_id' => 29,
                'comment' => 'Sit incidunt omnis provident quisquam quae dolores vel. Reprehenderit omnis velit cumque itaque saepe rerum beatae.',
                'user_id' => 28,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'auction_id' => 14,
                'comment' => 'Cum pariatur voluptatem dignissimos perferendis maxime tempore. Ea aperiam quisquam est veniam voluptates.',
                'user_id' => 24,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'auction_id' => 26,
                'comment' => 'Voluptatem delectus consequatur necessitatibus et ducimus fugiat officia. Enim voluptas consequuntur distinctio officiis.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'auction_id' => 6,
                'comment' => 'Est maiores adipisci beatae amet. Optio eos magnam corrupti et est earum pariatur.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'auction_id' => 15,
                'comment' => 'Commodi quis et numquam mollitia et fugit modi.',
                'user_id' => 12,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'auction_id' => 9,
                'comment' => 'Facere dolore et ullam. Asperiores ut id accusantium sequi dignissimos.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'auction_id' => 24,
                'comment' => 'Aut incidunt blanditiis maxime et rem aut. Voluptatem est officia odit molestiae suscipit quisquam.',
                'user_id' => 26,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'auction_id' => 24,
                'comment' => 'Blanditiis praesentium quas molestias laboriosam. Amet dolor qui repellat.',
                'user_id' => 8,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'auction_id' => 6,
                'comment' => 'Quasi voluptatum distinctio ea ut officia numquam sapiente iure.',
                'user_id' => 23,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'auction_id' => 21,
                'comment' => 'Earum ea officia et eum. Et labore fugiat mollitia dignissimos numquam sed eos.',
                'user_id' => 6,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'auction_id' => 5,
                'comment' => 'Ea id rerum repellat vel ut ipsam et odio.',
                'user_id' => 13,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'auction_id' => 1,
                'comment' => 'Commodi molestiae in non perspiciatis.',
                'user_id' => 28,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'auction_id' => 1,
                'comment' => 'Deleniti inventore quod neque ipsam magnam. Dolore suscipit sed aut perferendis.',
                'user_id' => 4,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'auction_id' => 23,
                'comment' => 'Id aut quis quam nemo qui itaque consequatur voluptate. Rerum perspiciatis quis qui modi.',
                'user_id' => 3,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'auction_id' => 30,
                'comment' => 'A eligendi consequatur sint cum quia fuga similique. Omnis culpa eos perspiciatis velit.',
                'user_id' => 7,
                'created_at' => '2019-06-12 13:53:45',
                'updated_at' => '2019-06-12 13:53:45',
                'deleted_at' => NULL,
            ),
        ));

    }
}