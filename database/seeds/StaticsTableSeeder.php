<?php

use Illuminate\Database\Seeder;

class StaticsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statics')->delete();
        
        \DB::table('statics')->insert(array (
            0 => 
            array (
                'id' => 1,
                'pop_up_image_url' => 'https://placeimg.com/300/420/animals?4',
            'pop_up_text' => '"Hai sobat JLF, Aplikasi ini masih tahap pengembangan (Beta Version) \\nMohon masukan demi pengembangan kami ke depannya"',
                'version' => 'v0.1.4',
                'change_log' => '-',
                'rek_ber1' => '123',
                'rek_ber2' => '123',
                'rek_ber3' => '13123',
                'email_cs' => 'qewq',
                'no_telp_cs' => '1313',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}