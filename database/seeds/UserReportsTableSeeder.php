<?php

use Illuminate\Database\Seeder;
use App\Models\UserReport;

class UserReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        UserReport::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        factory(App\Models\UserReport::class, 150)->create();
        
        $user_reports = [
            [
                'reason' => 'Hit and Run',
                'reported_user_id' => 3,
                'user_id' => 2
            ],
        ];

        foreach ($user_reports as $user_report) {
            UserReport::create($user_report);
        }
    }
}
