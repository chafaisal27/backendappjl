<?php

use Illuminate\Database\Seeder;
use App\Models\UserBlacklist;

class UserBlacklistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        UserBlacklist::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $user_blacklists = [
            [
                'reason' => 'Hit and Run',
                'admin_user_id' => 1,
                'user_id' => 3
            ],
            [
                'reason' => 'Hit and Run',
                'admin_user_id' => 1,
                'user_id' => 3
            ],
        ];

        foreach ($user_blacklists as $user_blacklist) {
            UserBlacklist::create($user_blacklist);
        }
    }
}
