<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('articles')->delete();
        
        \DB::table('articles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'link' => 'http://media.keepo.me/hi20161103132549.jpg',
                'description' => 'qweqw',
                'type' => 'champaign',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'image' => 'http://media.keepo.me/hi20161103132549.jpg',
                'thumbnail' => 'http://media.keepo.me/hi20161103132549.jpg',
            ),
            1 => 
            array (
                'id' => 2,
                'link' => 'https://www.liputan6.com/health/read/3082433/punya-kura-kura-di-rumah-ini-bahaya-yang-bisa-mengintai-anak?utm_expid=.9Z4i5ypGQeGiS7w9arwTvQ.0&utm_referrer=https%3A%2F%2Fwww.google.com%2F',
                'description' => 'Punya Kura-Kura di Rumah? Ini Bahaya yang Bisa Mengintai Anak',
                'type' => 'article',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 1,
                'admin_user_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            'image' => 'https://cdn0-production-images-kly.akamaized.net/mlWguH_D--qaFOedNOreIKdpV8s=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/1071039/original/041915500_1448872446-14824-Baby-Hermanns-Tortoise-white-background.jpg',
            'thumbnail' => 'https://cdn0-production-images-kly.akamaized.net/mlWguH_D--qaFOedNOreIKdpV8s=/640x360/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/1071039/original/041915500_1448872446-14824-Baby-Hermanns-Tortoise-white-background.jpg',
            ),
            2 => 
            array (
                'id' => 3,
                'link' => 'https://www.tribunnews.com/travel/2016/03/24/bertahan-hidup-184-tahun-kura-kura-jonathan-ini-akhirnya-untuk-pertama-kali-mandi',
                'description' => 'Bertahan Hidup 184 Tahun, Kura-kura Jonathan Ini Akhirnya Untuk Pertama Kali Mandi',
                'type' => 'article',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 1,
                'admin_user_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'image' => 'https://cdn2.tstatic.net/tribunnews/foto/bank/images/kura-kura-jonathan_20160324_053115.jpg',
                'thumbnail' => 'https://cdn2.tstatic.net/tribunnews/foto/bank/images/kura-kura-jonathan_20160324_053115.jpg',
            ),
            3 => 
            array (
                'id' => 4,
                'link' => 'https://www.tribunnews.com/sains/2018/04/12/kura-kura-berambut-hijau-yang-bernapas-melalui-alat-kelaminnya-terancam-punah',
                'description' => 'Kura-kura Berambut Hijau yang Bernapas Melalui Alat Kelaminnya Terancam Punah',
                'type' => 'article',
                'start_date' => '2019-07-25 01:00:00',
                'end_date' => '2019-07-25 01:00:00',
                'order' => 1,
                'admin_user_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'image' => 'https://cdn2.tstatic.net/tribunnews/foto/bank/images/kura-kura-saint-mary_20180412_122627.jpg',
                'thumbnail' => 'https://cdn2.tstatic.net/tribunnews/foto/bank/images/kura-kura-saint-mary_20180412_122627.jpg',
            ),
        ));
        
        
    }
}