<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'link' => 'https://placeimg.com/520/200/animals?4\'',
                'file_name' => 'qwe',
                'type' => 'qwe',
                'start_date' => '2019-07-10 00:00:00',
                'end_date' => '2019-07-26 00:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'link' => 'https://placeimg.com/420/200/animals?1',
                'file_name' => 'qwe',
                'type' => 'qwe',
                'start_date' => '2019-07-10 00:00:00',
                'end_date' => '2019-07-26 00:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'link' => 'https://placeimg.com/420/200/animals?2',
                'file_name' => 'qwe',
                'type' => 'qwe',
                'start_date' => '2019-07-10 00:00:00',
                'end_date' => '2019-07-26 00:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'link' => 'https://placeimg.com/420/200/animals?3',
                'file_name' => 'qwe',
                'type' => 'qwe',
                'start_date' => '2019-07-10 00:00:00',
                'end_date' => '2019-07-26 00:00:00',
                'order' => 1,
                'admin_user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}