<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'animal_id' => 2,
                'price' => 10000,
                'status' => 'active',
                'quantity' => 1,
                'inner_island_shipping' => 0,
                'owner_user_id' => 1,
                'slug' => 'wqeqwewq',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'type' => 'animal'
            ),
        ));
        
        
    }
}