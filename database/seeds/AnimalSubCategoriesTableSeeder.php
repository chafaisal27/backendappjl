<?php

use Illuminate\Database\Seeder;
use App\Models\AnimalSubCategory;

class AnimalSubCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // \DB::table('animal_sub_categories')->delete();
        AnimalSubCategory::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $categories = [
            [
                'name' => 'Kura-Kura',
                'sub' => ['Kura-kura Darat', 'Kura-kura Air']
            ],
            [
                'name' => 'Kadal',
                'sub' => ['Iguana', 'Gecko', 'Bearded Dragon', 'Kadal Lainnya']
            ],
            [
                'name' => 'Ular',
                'sub' => ['Ballpython', 'Retic/Boa', 'Colubird/Cornsnake', 'Venom', 'Ular Lainnya']
            ],
            [
                'name' => 'Ikan',
                'sub' => ['Koi', 'Cupang', 'Arwana', 'Aquascape', 'Ikan Lainnya']
            ],
            [
                'name' => 'Sugar Glider',
                'sub' => []
            ],
            [
                'name' => 'Anjing',
                'sub' => ['Anjing Besar', 'Anjing Kecil']
            ],
            [
                'name' => 'Kucing',
                'sub' => ['Bulu Panjang', 'Bulu Pendek']
            ],
            [
                'name' => 'Unggas',
                'sub' => ['Lovebird', 'Anis Merah/Kembang', 'Murai', 'Cucakrowo', 'Branjangan', 'Kenari', 'Jalak', 'Cendet/Plentet', 'Parrot', 'Ayam', 'Unggas Lainnya']
            ],
            [
                'name' => 'Amphibi',
                'sub' => ['Katak']
            ],
            [
                'name' => 'Tarantula',
                'sub' => []
            ],
        ];
        foreach ($categories as $key => $category) {
            foreach ($category['sub'] as $sub) {
                AnimalSubCategory::create([
                    'name' => $sub,
                    'image' => NULL,
                    'thumbnail' => NULL,
                    'slug' => str_slug($sub),
                    'animal_category_id' => ($key + 1),
                ]);
            }
        }
    }
}
