<?php

use Illuminate\Database\Seeder;
use App\Models\UserRating;

class UserRatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        UserRating::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        $user_ratings = [
            [ 
                'user_id' => 2,
                'rating' => 4,
                'comment' => 'Fast response, deskripsi doggy sesuai',
                'from_user_id' => 3
            ],
            [ 
                'user_id' => 2,
                'rating' => 5,
                'comment' => 'Fast response, deskripsi kura-kura sesuai',
                'from_user_id' => 4
            ],
        ];

        foreach ($user_ratings as $user_rating) {
            UserRating::create($user_rating);
        }
    }
}
