<?php

use Illuminate\Database\Seeder;

class ProductCommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_comments')->delete();
        
        \DB::table('product_comments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'product_id' => 1,
                'comment' => 'qweqweqwew',
                'user_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}