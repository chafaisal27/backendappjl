<?php

use Illuminate\Database\Seeder;
use App\Models\AnimalImage;
use App\Models\Animal;

class AnimalImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */


    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('animal_images')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        for ($i=1; $i <= 100; $i++) {
            $animal = Animal::with('animal_sub_category.animal_category')->find($i);
            AnimalImage::create([
                'animal_id' => $i,
                'image' => $animal->animal_sub_category->animal_category->image,
                'thumbnail' => $animal->animal_sub_category->animal_category->image,
            ]);
        }
    }
}
