<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('animal_id');
            $table->foreign('animal_id')->references('id')->on('animals');
            $table->string('type');
            $table->string('invoice_number');
            $table->string('quantity')->nullable();
            $table->string('expedition_name')->nullable();
            $table->text('guarantee')->nullable();

            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('delivery_price')->nullable();
            $table->unsignedBigInteger('service_price')->nullable();
            
            // $table->unsignedBigInteger('seller_point_received');
        
            $table->string('seller_bank_name')->nullable();
            $table->string('seller_bank_account_number')->nullable();
            $table->string('seller_bank_account_name')->nullable();
            
            $table->date('delivery_date')->nullable();
            $table->date('received_date_estimation')->nullable();

            $table->text('buyer_address')->nullable();

            // $table->unsignedBigInteger('buyer_point_received');

            $table->string('buyer_bank_name')->nullable();
            $table->string('buyer_bank_account_number')->nullable();
            $table->string('buyer_bank_account_name')->nullable();

            $table->unsignedBigInteger('seller_user_id');
            $table->foreign('seller_user_id')->references('id')->on('users');

            $table->unsignedBigInteger('buyer_user_id');
            $table->foreign('buyer_user_id')->references('id')->on('users');

            $table->unsignedBigInteger('admin_user_id');
            $table->foreign('admin_user_id')->references('id')->on('users');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
