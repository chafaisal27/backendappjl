<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('animal_id');
            $table->foreign('animal_id')->references('id')->on('animals');
            $table->unsignedBigInteger('open_bid');
            $table->unsignedBigInteger('multiply');
            $table->unsignedBigInteger('buy_it_now');
            $table->timestamp('expiry_date');
            
            // If owner cancelled the auction
            $table->string('cancellation_image')->nullable();
            $table->string('cancellation_reason')->nullable();
            $table->timestamp('cancellation_date')->nullable();

            // If a winner has been decided
            $table->string('payment_image')->nullable();
            $table->boolean('owner_confirmation')->nullable();
            $table->boolean('winner_confirmation')->nullable();
            $table->unsignedBigInteger('winner_bid_id')->nullable();
            
            $table->timestamp('winner_accepted_date')->nullable();
            
            $table->unsignedBigInteger('owner_user_id');
            $table->foreign('owner_user_id')->references('id')->on('users');
            
            $table->boolean('active')->default(1);

            $table->boolean('inner_island_shipping')->default(0);

            $table->string('slug');

            $table->integer('duration');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
