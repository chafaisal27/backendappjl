<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirebaseChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firebase_chats', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('firebase_chat_id')->unique();
            
            $table->integer('seller_unread_count')->default(0);
            $table->unsignedBigInteger('seller_user_id');
            $table->foreign('seller_user_id')->references('id')->on('users');

            $table->integer('buyer_unread_count')->default(0);
            $table->unsignedBigInteger('buyer_user_id');
            $table->foreign('buyer_user_id')->references('id')->on('users');

            $table->integer('admin_unread_count')->default(0);
            $table->unsignedBigInteger('admin_user_id');
            $table->foreign('admin_user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firebase_chats');
    }
}
