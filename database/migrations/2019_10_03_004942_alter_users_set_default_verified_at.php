<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class AlterUsersSetDefaultVerifiedAt extends Migration
{
    public function up()
    {
        // Update verified users before update 0.1.7 - Oct 2019 
        User::where("verification_status", "verified")->update(["verified_at" => \Carbon\Carbon::now()]);
    }
}
