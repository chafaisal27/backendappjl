<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStaticsAddHowToJoinHotAuctionImageUrlField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statics', function (Blueprint $table) {
            $table->text('how_to_join_hot_auction_image_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statics', function (Blueprint $table) {
            $table->dropColumn('how_to_join_hot_auction_image_url');
        });
    }
}
