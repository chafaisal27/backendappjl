<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('animal_sub_category_id');
            $table->foreign('animal_sub_category_id')->references('id')->on('animal_sub_categories');
            
            $table->string('name');

            // Gender [Male/Female]
            $table->enum('gender', ['M', 'F']);

            $table->text('description');

            $table->date('date_of_birth');

            $table->unsignedBigInteger('regency_id');
            $table->foreign('regency_id')->references('id')->on('regencies');

            $table->unsignedBigInteger('owner_user_id');
            $table->foreign('owner_user_id')->references('id')->on('users');
            
            $table->string('slug')->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
