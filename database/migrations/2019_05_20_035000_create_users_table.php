<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            
            // Gender [Male/Female]
            $table->enum('gender', ['M', 'F'])->nullable();

            $table->text('description')->nullable();
            $table->string('username')->unique();
            $table->string('password')->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->string('address')->nullable();
            $table->string('photo')->nullable();
            
            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')->references('id')->on('roles');

            $table->unsignedBigInteger('regency_id')->nullable();
            $table->foreign('regency_id')->references('id')->on('regencies');

            $table->boolean('blacklisted')->default(0);

            $table->string('firebase_token')->nullable();

            $table->softDeletes();

            $table->timestamps();
            $table->rememberToken();
            // $table->timestamp('email_verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
