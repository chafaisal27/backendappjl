<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTopSellerPoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_sellers_poin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->decimal('point', 8, 1);
            $table->unsignedBigInteger('animal_sub_category_id');
            $table->foreign('animal_sub_category_id')->references('id')->on('animal_sub_categories');
            $table->unsignedBigInteger('animal_category_id');
            $table->foreign('animal_category_id')->references('id')->on('animal_categories');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_sellers_poin');
    }
}
