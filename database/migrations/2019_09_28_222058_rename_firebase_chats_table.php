<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFirebaseChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('firebase_chats')) {
            Schema::rename('firebase_chats', 'auction_chats');
        }

        if (!Schema::hasColumn('auction_chats', 'auction_id')) {
            Schema::table('auction_chats', function (Blueprint $table) {
                $table->unsignedBigInteger('auction_id');
                $table->foreign('auction_id')->references('id')->on('auctions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firebase_chats', function (Blueprint $table) {
            $table->dropForeign('auction_id');
            $table->dropColumn('auction_id');
        });
    }
}
