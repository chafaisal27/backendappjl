<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('pop_up_image_url')->nullable();
            $table->text('pop_up_text')->nullable();
            $table->string('version')->nullable();
            $table->text('change_log')->nullable();
            $table->string('rek_ber1')->nullable();
            $table->string('rek_ber2')->nullable();
            $table->string('rek_ber3')->nullable();
            $table->string('email_cs')->nullable();
            $table->string('no_telp_cs')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statics');
    }
}
