<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTopSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('top_sellers', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
            $table->string('thumbnail')->nullable()->change();

            $table->unsignedBigInteger('user_id')->nullable()->change();

            $table->dropForeign(['animal_category_id']);
            $table->dropColumn('animal_category_id');

            $table->unsignedBigInteger('animal_sub_category_id');
            $table->foreign('animal_sub_category_id')->references('id')->on('animal_sub_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('top_sellers', function (Blueprint $table) {
            $table->string('image')->nullable(false)->change();
            $table->string('thumbnail')->nullable(false)->change();

            $table->unsignedBigInteger('user_id')->nullable(false)->change();
            
            $table->unsignedBigInteger('animal_category_id');
            $table->foreign('animal_category_id')->references('id')->on('animal_categories');
        });
    }
}
