<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class AlterFlagTypeVerifUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('verification_type')->after('verification_status')->nullable();
        });

        $tampUsers = User::get();
        foreach ($tampUsers as $value) {
            if ($value->verification_status == "verified") {
                User::where("id", $value->id)->update(["verification_type" => "KTP"]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verification-type');
        });
    }
}
